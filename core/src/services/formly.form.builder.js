"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var formly_config_1 = require("./formly.config");
var utils_1 = require("./../utils");
var utils_2 = require("../utils");
var formly_form_expression_1 = require("./formly.form.expression");
var FormlyFormBuilder = /** @class */ (function () {
    function FormlyFormBuilder(formlyConfig, formlyFormExpression) {
        this.formlyConfig = formlyConfig;
        this.formlyFormExpression = formlyFormExpression;
        this.formId = 0;
    }
    FormlyFormBuilder.prototype.buildForm = function (form, fields, model, options) {
        if (fields === void 0) { fields = []; }
        var fieldTransforms = (options && options.fieldTransform) || this.formlyConfig.extras.fieldTransform;
        if (!Array.isArray(fieldTransforms)) {
            fieldTransforms = [fieldTransforms];
        }
        fieldTransforms.forEach(function (fieldTransform) {
            if (fieldTransform) {
                fields = fieldTransform(fields, model, form, options);
                if (!fields) {
                    throw new Error('fieldTransform must return an array of fields');
                }
            }
        });
        this._buildForm(form, fields, model, options);
        this.formlyFormExpression.checkFields(form, fields, model, options);
    };
    FormlyFormBuilder.prototype._buildForm = function (form, fields, model, options) {
        if (fields === void 0) { fields = []; }
        this.formId++;
        this.registerFormControls(form, fields, model, options);
    };
    FormlyFormBuilder.prototype.registerFormControls = function (form, fields, model, options) {
        var _this = this;
        fields.forEach(function (field, index) {
            field.id = utils_1.getFieldId("formly_" + _this.formId, field, index);
            if (!utils_2.isUndefined(field.defaultValue) && utils_2.isUndefined(utils_1.getValueForKey(model, field.key))) {
                utils_1.assignModelValue(model, field.key, field.defaultValue);
            }
            _this.initFieldOptions(field);
            _this.initFieldExpression(field, model, options);
            _this.initFieldValidation(field);
            _this.initFieldAsyncValidation(field);
            if (field.key && field.type) {
                var paths_1 = utils_2.getKeyPath({ key: field.key });
                var rootForm_1 = form, rootModel_1 = model;
                paths_1.forEach(function (path, index) {
                    // FormGroup/FormArray only allow string value for path
                    var formPath = path.toString();
                    // is last item
                    if (index === paths_1.length - 1) {
                        _this.addFormControl(rootForm_1, field, rootModel_1, formPath);
                    }
                    else {
                        var nestedForm = rootForm_1.get(formPath);
                        if (!nestedForm) {
                            nestedForm = new forms_1.FormGroup({});
                            _this.addControl(rootForm_1, formPath, nestedForm);
                        }
                        if (!rootModel_1[path]) {
                            rootModel_1[path] = typeof path === 'string' ? {} : [];
                        }
                        rootForm_1 = nestedForm;
                        rootModel_1 = rootModel_1[path];
                    }
                });
            }
            if (field.fieldGroup) {
                if (!field.type) {
                    field.type = 'formly-group';
                }
                if (field.key) {
                    _this.addFormControl(form, field, (_a = {}, _a[field.key] = field.fieldArray ? [] : {}, _a), field.key);
                    model[field.key] = model[field.key] || (field.fieldArray ? [] : {});
                    _this._buildForm(field.formControl, field.fieldGroup, model[field.key], options);
                }
                else {
                    // if `hideExpression` is set in that case we have to deal
                    // with toggle FormControl for each field in fieldGroup separately
                    if (field.hideExpression) {
                        field.fieldGroup.forEach(function (f) {
                            var hideExpression = f.hideExpression || (function () { return false; });
                            if (typeof hideExpression === 'string') {
                                hideExpression = utils_1.evalStringExpression(hideExpression, ['model', 'formState']);
                            }
                            f.hideExpression = function (model, formState) { return field.hide || hideExpression(model, formState); };
                        });
                    }
                    _this._buildForm(form, field.fieldGroup, model, options);
                }
            }
            var _a;
        });
    };
    FormlyFormBuilder.prototype.initFieldExpression = function (field, model, options) {
        if (field.expressionProperties) {
            for (var key in field.expressionProperties) {
                if (typeof field.expressionProperties[key] === 'string' || utils_2.isFunction(field.expressionProperties[key])) {
                    // cache built expression
                    field.expressionProperties[key] = {
                        expression: utils_2.isFunction(field.expressionProperties[key]) ? field.expressionProperties[key] : utils_1.evalStringExpression(field.expressionProperties[key], ['model', 'formState']),
                        expressionValueSetter: utils_1.evalExpressionValueSetter(key, ['expressionValue', 'model', 'templateOptions', 'validation', 'field']),
                    };
                }
            }
        }
        if (field.hideExpression) {
            // delete hide value in order to force re-evalute it in FormlyFormExpression.
            delete field.hide;
            if (typeof field.hideExpression === 'string') {
                // cache built expression
                field.hideExpression = utils_1.evalStringExpression(field.hideExpression, ['model', 'formState']);
            }
        }
    };
    FormlyFormBuilder.prototype.initFieldOptions = function (field) {
        field.templateOptions = field.templateOptions || {};
        if (field.type) {
            this.formlyConfig.getMergedField(field);
            if (field.key) {
                field.templateOptions = Object.assign({
                    label: '',
                    placeholder: '',
                    focus: false,
                }, field.templateOptions);
            }
        }
    };
    FormlyFormBuilder.prototype.initFieldAsyncValidation = function (field) {
        var _this = this;
        var validators = [];
        if (field.asyncValidators) {
            var _loop_1 = function (validatorName) {
                if (validatorName !== 'validation') {
                    validators.push(function (control) {
                        var validator = field.asyncValidators[validatorName];
                        if (utils_1.isObject(validator)) {
                            validator = validator.expression;
                        }
                        return new Promise(function (resolve) {
                            return validator(control, field).then(function (result) {
                                resolve(result ? null : (_a = {}, _a[validatorName] = true, _a));
                                var _a;
                            });
                        });
                    });
                }
            };
            for (var validatorName in field.asyncValidators) {
                _loop_1(validatorName);
            }
        }
        if (field.asyncValidators && Array.isArray(field.asyncValidators.validation)) {
            field.asyncValidators.validation.forEach(function (validate) {
                if (typeof validate === 'string') {
                    validators.push(_this.formlyConfig.getValidator(validate).validation);
                }
                else {
                    validators.push(validate);
                }
            });
        }
        if (validators.length) {
            if (field.asyncValidators && !Array.isArray(field.asyncValidators.validation)) {
                field.asyncValidators.validation = forms_1.Validators.composeAsync([field.asyncValidators.validation].concat(validators));
            }
            else {
                field.asyncValidators = {
                    validation: forms_1.Validators.composeAsync(validators),
                };
            }
        }
    };
    FormlyFormBuilder.prototype.initFieldValidation = function (field) {
        var _this = this;
        var validators = [];
        utils_1.FORMLY_VALIDATORS
            .filter(function (opt) { return (field.templateOptions && field.templateOptions.hasOwnProperty(opt))
            || (field.expressionProperties && field.expressionProperties["templateOptions." + opt]); })
            .forEach(function (opt) {
            validators.push(function (control) {
                if (field.templateOptions[opt] === false) {
                    return null;
                }
                return _this.getValidation(opt, field.templateOptions[opt])(control);
            });
        });
        if (field.validators) {
            var _loop_2 = function (validatorName) {
                if (validatorName !== 'validation') {
                    validators.push(function (control) {
                        var validator = field.validators[validatorName];
                        if (utils_1.isObject(validator)) {
                            validator = validator.expression;
                        }
                        return validator(control, field) ? null : (_a = {}, _a[validatorName] = true, _a);
                        var _a;
                    });
                }
            };
            for (var validatorName in field.validators) {
                _loop_2(validatorName);
            }
        }
        if (field.validators && Array.isArray(field.validators.validation)) {
            field.validators.validation.forEach(function (validate) {
                if (typeof validate === 'string') {
                    validators.push(_this.formlyConfig.getValidator(validate).validation);
                }
                else {
                    validators.push(validate);
                }
            });
        }
        if (validators.length) {
            if (field.validators && !Array.isArray(field.validators.validation)) {
                field.validators.validation = forms_1.Validators.compose([field.validators.validation].concat(validators));
            }
            else {
                field.validators = {
                    validation: forms_1.Validators.compose(validators),
                };
            }
        }
    };
    FormlyFormBuilder.prototype.addFormControl = function (form, field, model, path) {
        var control;
        if (field.formControl instanceof forms_1.AbstractControl || form.get(path)) {
            control = field.formControl || form.get(path);
            if (!(utils_1.isNullOrUndefined(control.value) && utils_1.isNullOrUndefined(model[path]))
                && control.value !== model[path]
                && control instanceof forms_1.FormControl) {
                control.patchValue(model[path]);
            }
        }
        else if (field.component && field.component.createControl) {
            control = field.component.createControl(model[path], field);
        }
        else if (field.fieldGroup && field.key && field.key === path && !field.fieldArray) {
            control = new forms_1.FormGroup(model[path], field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
        }
        else if (field.fieldArray && field.key && field.key === path) {
            control = new forms_1.FormArray([], field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
        }
        else {
            control = new forms_1.FormControl(model[path], field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
        }
        if (field.templateOptions.disabled) {
            control.disable();
        }
        // Replace decorated property with a getter that returns the observable.
        // https://github.com/angular-redux/store/blob/master/src/decorators/select.ts#L79-L85
        if (delete field.templateOptions.disabled) {
            Object.defineProperty(field.templateOptions, 'disabled', {
                get: (function () { return !this.formControl.enabled; }).bind(field),
                set: (function (value) {
                    if (this.expressionProperties && this.expressionProperties.hasOwnProperty('templateOptions.disabled')) {
                        this.expressionProperties['templateOptions.disabled'].expressionValue = value;
                    }
                    value ? this.formControl.disable() : this.formControl.enable();
                }).bind(field),
                enumerable: true,
                configurable: true,
            });
        }
        this.addControl(form, path, control, field);
    };
    FormlyFormBuilder.prototype.addControl = function (form, key, formControl, field) {
        if (field) {
            field.formControl = formControl;
        }
        if (form instanceof forms_1.FormArray) {
            if (form.at(key) !== formControl) {
                form.setControl(key, formControl);
            }
        }
        else {
            if (form.get(key) !== formControl) {
                form.setControl(key, formControl);
            }
        }
    };
    FormlyFormBuilder.prototype.getValidation = function (opt, value) {
        switch (opt) {
            case 'required':
                return forms_1.Validators.required;
            case 'pattern':
                return forms_1.Validators.pattern(value);
            case 'minLength':
                return forms_1.Validators.minLength(value);
            case 'maxLength':
                return forms_1.Validators.maxLength(value);
            case 'min':
                return forms_1.Validators.min(value);
            case 'max':
                return forms_1.Validators.max(value);
        }
    };
    FormlyFormBuilder = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [formly_config_1.FormlyConfig,
            formly_form_expression_1.FormlyFormExpression])
    ], FormlyFormBuilder);
    return FormlyFormBuilder;
}());
exports.FormlyFormBuilder = FormlyFormBuilder;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5LmZvcm0uYnVpbGRlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvcm1seS5mb3JtLmJ1aWxkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0Msd0NBQWdHO0FBQ2hHLGlEQUErQztBQUMvQyxvQ0FBMks7QUFFM0ssa0NBQStEO0FBQy9ELG1FQUFnRTtBQUdoRTtJQUdFLDJCQUNVLFlBQTBCLEVBQzFCLG9CQUEwQztRQUQxQyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBSjVDLFdBQU0sR0FBRyxDQUFDLENBQUM7SUFLaEIsQ0FBQztJQUVKLHFDQUFTLEdBQVQsVUFBVSxJQUEyQixFQUFFLE1BQWdDLEVBQUUsS0FBVSxFQUFFLE9BQTBCO1FBQXhFLHVCQUFBLEVBQUEsV0FBZ0M7UUFDckUsSUFBSSxlQUFlLEdBQUcsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQztRQUNyRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLGVBQWUsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RDLENBQUM7UUFFRCxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsY0FBYztZQUNwQyxFQUFFLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNuQixNQUFNLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUN0RCxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ1osTUFBTSxJQUFJLEtBQUssQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO2dCQUNuRSxDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFFTyxzQ0FBVSxHQUFsQixVQUFtQixJQUEyQixFQUFFLE1BQWdDLEVBQUUsS0FBVSxFQUFFLE9BQTBCO1FBQXhFLHVCQUFBLEVBQUEsV0FBZ0M7UUFDOUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFTyxnREFBb0IsR0FBNUIsVUFBNkIsSUFBMkIsRUFBRSxNQUEyQixFQUFFLEtBQVUsRUFBRSxPQUEwQjtRQUE3SCxpQkErREM7UUE5REMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQUssRUFBRSxLQUFLO1lBQzFCLEtBQUssQ0FBQyxFQUFFLEdBQUcsa0JBQVUsQ0FBQyxZQUFVLEtBQUksQ0FBQyxNQUFRLEVBQUUsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBRTdELEVBQUUsQ0FBQyxDQUFDLENBQUMsbUJBQVcsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksbUJBQVcsQ0FBQyxzQkFBYyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RGLHdCQUFnQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN6RCxDQUFDO1lBQ0QsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdCLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQ2hELEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxLQUFJLENBQUMsd0JBQXdCLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFckMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsSUFBTSxPQUFLLEdBQUcsa0JBQVUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDN0MsSUFBSSxVQUFRLEdBQUcsSUFBSSxFQUFFLFdBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3ZDLE9BQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSztvQkFDeEIsdURBQXVEO29CQUN2RCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ2pDLGVBQWU7b0JBQ2YsRUFBRSxDQUFDLENBQUMsS0FBSyxLQUFLLE9BQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDL0IsS0FBSSxDQUFDLGNBQWMsQ0FBQyxVQUFRLEVBQUUsS0FBSyxFQUFFLFdBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDNUQsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDTixJQUFJLFVBQVUsR0FBRyxVQUFRLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBYyxDQUFDO3dCQUNyRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7NEJBQ2hCLFVBQVUsR0FBRyxJQUFJLGlCQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7NEJBQy9CLEtBQUksQ0FBQyxVQUFVLENBQUMsVUFBUSxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDbEQsQ0FBQzt3QkFDRCxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3JCLFdBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLElBQUksS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUN2RCxDQUFDO3dCQUVELFVBQVEsR0FBRyxVQUFVLENBQUM7d0JBQ3RCLFdBQVMsR0FBRyxXQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzlCLENBQUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JCLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2hCLEtBQUssQ0FBQyxJQUFJLEdBQUcsY0FBYyxDQUFDO2dCQUM5QixDQUFDO2dCQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNkLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLEtBQUssWUFBSSxHQUFDLEtBQUssQ0FBQyxHQUFHLElBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQUksS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN6RixLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO29CQUNwRSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxXQUF3QixFQUFFLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDL0YsQ0FBQztnQkFBQyxJQUFJLENBQUMsQ0FBQztvQkFDTiwwREFBMEQ7b0JBQzFELGtFQUFrRTtvQkFDbEUsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQzs0QkFDeEIsSUFBSSxjQUFjLEdBQVEsQ0FBQyxDQUFDLGNBQWMsSUFBSSxDQUFDLGNBQU0sT0FBQSxLQUFLLEVBQUwsQ0FBSyxDQUFDLENBQUM7NEJBQzVELEVBQUUsQ0FBQyxDQUFDLE9BQU8sY0FBYyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0NBQ3ZDLGNBQWMsR0FBRyw0QkFBb0IsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUMsQ0FBQzs0QkFDaEYsQ0FBQzs0QkFFRCxDQUFDLENBQUMsY0FBYyxHQUFHLFVBQUMsS0FBSyxFQUFFLFNBQVMsSUFBSyxPQUFBLEtBQUssQ0FBQyxJQUFJLElBQUksY0FBYyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsRUFBOUMsQ0FBOEMsQ0FBQzt3QkFDMUYsQ0FBQyxDQUFDLENBQUM7b0JBQ0wsQ0FBQztvQkFDRCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDMUQsQ0FBQztZQUNILENBQUM7O1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU8sK0NBQW1CLEdBQTNCLFVBQTRCLEtBQXdCLEVBQUUsS0FBVSxFQUFFLE9BQTBCO1FBQzFGLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUM7WUFDL0IsR0FBRyxDQUFDLENBQUMsSUFBTSxHQUFHLElBQUksS0FBSyxDQUFDLG9CQUEyQixDQUFDLENBQUMsQ0FBQztnQkFDcEQsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsb0JBQW9CLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxJQUFJLGtCQUFVLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2Ryx5QkFBeUI7b0JBQ3pCLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsR0FBRzt3QkFDaEMsVUFBVSxFQUFFLGtCQUFVLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsNEJBQW9CLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDO3dCQUN6SyxxQkFBcUIsRUFBRSxpQ0FBeUIsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO3FCQUM5SCxDQUFDO2dCQUNKLENBQUM7WUFDSCxDQUFDO1FBQ0gsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3pCLDZFQUE2RTtZQUM3RSxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUM7WUFDbEIsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsY0FBYyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzdDLHlCQUF5QjtnQkFDekIsS0FBSyxDQUFDLGNBQWMsR0FBRyw0QkFBb0IsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUMsT0FBTyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUM7WUFDNUYsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRU8sNENBQWdCLEdBQXhCLFVBQXlCLEtBQXdCO1FBQy9DLEtBQUssQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7UUFDcEQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDZixJQUFJLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN4QyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztnQkFDZCxLQUFLLENBQUMsZUFBZSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7b0JBQ3BDLEtBQUssRUFBRSxFQUFFO29CQUNULFdBQVcsRUFBRSxFQUFFO29CQUNmLEtBQUssRUFBRSxLQUFLO2lCQUNiLEVBQUUsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQzVCLENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVPLG9EQUF3QixHQUFoQyxVQUFpQyxLQUF3QjtRQUF6RCxpQkF1Q0M7UUF0Q0MsSUFBTSxVQUFVLEdBQVEsRUFBRSxDQUFDO1FBQzNCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDO29DQUNmLGFBQWE7Z0JBQ3RCLEVBQUUsQ0FBQyxDQUFDLGFBQWEsS0FBSyxZQUFZLENBQUMsQ0FBQyxDQUFDO29CQUNuQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQUMsT0FBb0I7d0JBQ25DLElBQUksU0FBUyxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQ3JELEVBQUUsQ0FBQyxDQUFDLGdCQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUN4QixTQUFTLEdBQUcsU0FBUyxDQUFDLFVBQVUsQ0FBQzt3QkFDbkMsQ0FBQzt3QkFFRCxNQUFNLENBQUMsSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPOzRCQUN6QixNQUFNLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFlO2dDQUNwRCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFFLEdBQUMsYUFBYSxJQUFHLElBQUksS0FBQyxDQUFDLENBQUM7OzRCQUNuRCxDQUFDLENBQUMsQ0FBQzt3QkFDTCxDQUFDLENBQUMsQ0FBQztvQkFDTCxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDO1lBQ0gsQ0FBQztZQWZELEdBQUcsQ0FBQyxDQUFDLElBQU0sYUFBYSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUM7d0JBQXZDLGFBQWE7YUFldkI7UUFDSCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdFLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQWE7Z0JBQ3JELEVBQUUsQ0FBQyxDQUFDLE9BQU8sUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZFLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDNUIsQ0FBQztZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ3RCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM5RSxLQUFLLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxrQkFBVSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsZUFBZSxDQUFDLFVBQVUsU0FBSyxVQUFVLEVBQUUsQ0FBQztZQUNoSCxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sS0FBSyxDQUFDLGVBQWUsR0FBRztvQkFDdEIsVUFBVSxFQUFFLGtCQUFVLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQztpQkFDaEQsQ0FBQztZQUNKLENBQUM7UUFDSCxDQUFDO0lBQ0gsQ0FBQztJQUVPLCtDQUFtQixHQUEzQixVQUE0QixLQUF3QjtRQUFwRCxpQkFrREM7UUFqREMsSUFBTSxVQUFVLEdBQVEsRUFBRSxDQUFDO1FBQzNCLHlCQUFpQjthQUNkLE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLENBQUMsS0FBSyxDQUFDLGVBQWUsSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztlQUM5RSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsSUFBSSxLQUFLLENBQUMsb0JBQW9CLENBQUMscUJBQW1CLEdBQUssQ0FBQyxDQUFDLEVBRDFFLENBQzBFLENBQ3hGO2FBQ0EsT0FBTyxDQUFDLFVBQUMsR0FBRztZQUNYLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFvQjtnQkFDbkMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNkLENBQUM7Z0JBRUQsTUFBTSxDQUFDLEtBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0RSxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUwsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7b0NBQ1YsYUFBYTtnQkFDdEIsRUFBRSxDQUFDLENBQUMsYUFBYSxLQUFLLFlBQVksQ0FBQyxDQUFDLENBQUM7b0JBQ25DLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBQyxPQUFvQjt3QkFDbkMsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDaEQsRUFBRSxDQUFDLENBQUMsZ0JBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3hCLFNBQVMsR0FBRyxTQUFTLENBQUMsVUFBVSxDQUFDO3dCQUNuQyxDQUFDO3dCQUVELE1BQU0sQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFFLEdBQUMsYUFBYSxJQUFHLElBQUksS0FBQyxDQUFDOztvQkFDcEUsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQztZQUNILENBQUM7WUFYRCxHQUFHLENBQUMsQ0FBQyxJQUFNLGFBQWEsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDO3dCQUFsQyxhQUFhO2FBV3ZCO1FBQ0gsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQyxRQUFhO2dCQUNoRCxFQUFFLENBQUMsQ0FBQyxPQUFPLFFBQVEsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUNqQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2RSxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNOLFVBQVUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVCLENBQUM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUN0QixFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDcEUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLEdBQUcsa0JBQVUsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLFNBQUssVUFBVSxFQUFFLENBQUM7WUFDakcsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLEtBQUssQ0FBQyxVQUFVLEdBQUc7b0JBQ2pCLFVBQVUsRUFBRSxrQkFBVSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7aUJBQzNDLENBQUM7WUFDSixDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFTywwQ0FBYyxHQUF0QixVQUF1QixJQUEyQixFQUFFLEtBQXdCLEVBQUUsS0FBVSxFQUFFLElBQVk7UUFDcEcsSUFBSSxPQUF3QixDQUFDO1FBQzdCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLFlBQVksdUJBQWUsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRSxPQUFPLEdBQUcsS0FBSyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLEVBQUUsQ0FBQyxDQUNELENBQUMsQ0FBQyx5QkFBaUIsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUkseUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7bUJBQ2xFLE9BQU8sQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQzttQkFDN0IsT0FBTyxZQUFZLG1CQUN4QixDQUFDLENBQUMsQ0FBQztnQkFDRCxPQUFPLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLENBQUM7UUFDSCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzVELE9BQU8sR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDOUQsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUNwRixPQUFPLEdBQUcsSUFBSSxpQkFBUyxDQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLEVBQ1gsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFDMUQsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FDckUsQ0FBQztRQUNKLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxLQUFLLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztZQUMvRCxPQUFPLEdBQUcsSUFBSSxpQkFBUyxDQUNyQixFQUFFLEVBQ0YsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFDMUQsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FDckUsQ0FBQztRQUNKLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNOLE9BQU8sR0FBRyxJQUFJLG1CQUFXLENBQ3ZCLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFDWCxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUMxRCxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUNyRSxDQUFDO1FBQ0osQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDcEIsQ0FBQztRQUVELHdFQUF3RTtRQUN4RSxzRkFBc0Y7UUFDdEYsRUFBRSxDQUFDLENBQUMsT0FBTyxLQUFLLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDMUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLFVBQVUsRUFBRTtnQkFDdkQsR0FBRyxFQUFFLENBQUMsY0FBYyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQ3BFLEdBQUcsRUFBRSxDQUFDLFVBQVUsS0FBYztvQkFDNUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3RHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7b0JBQ2hGLENBQUM7b0JBRUQsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNqRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUNkLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUM7UUFDTCxDQUFDO1FBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRU8sc0NBQVUsR0FBbEIsVUFBbUIsSUFBMkIsRUFBRSxHQUFvQixFQUFFLFdBQTRCLEVBQUUsS0FBeUI7UUFDM0gsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNWLEtBQUssQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1FBQ2xDLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLFlBQVksaUJBQVMsQ0FBQyxDQUFDLENBQUM7WUFDOUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBVSxHQUFHLENBQUMsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxJQUFJLENBQUMsVUFBVSxDQUFTLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUM1QyxDQUFDO1FBQ0gsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSxHQUFHLENBQUMsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFTLEdBQUcsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUM1QyxDQUFDO1FBQ0gsQ0FBQztJQUNILENBQUM7SUFFTyx5Q0FBYSxHQUFyQixVQUFzQixHQUFXLEVBQUUsS0FBVTtRQUMzQyxNQUFNLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ1osS0FBSyxVQUFVO2dCQUNiLE1BQU0sQ0FBQyxrQkFBVSxDQUFDLFFBQVEsQ0FBQztZQUM3QixLQUFLLFNBQVM7Z0JBQ1osTUFBTSxDQUFDLGtCQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ25DLEtBQUssV0FBVztnQkFDZCxNQUFNLENBQUMsa0JBQVUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckMsS0FBSyxXQUFXO2dCQUNkLE1BQU0sQ0FBQyxrQkFBVSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQyxLQUFLLEtBQUs7Z0JBQ1IsTUFBTSxDQUFDLGtCQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9CLEtBQUssS0FBSztnQkFDUixNQUFNLENBQUMsa0JBQVUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDakMsQ0FBQztJQUNILENBQUM7SUEzVFUsaUJBQWlCO1FBRDdCLGlCQUFVLEVBQUU7eUNBS2EsNEJBQVk7WUFDSiw2Q0FBb0I7T0FMekMsaUJBQWlCLENBNFQ3QjtJQUFELHdCQUFDO0NBQUEsQUE1VEQsSUE0VEM7QUE1VFksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUdyb3VwLCBGb3JtQXJyYXksIEZvcm1Db250cm9sLCBBYnN0cmFjdENvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3JtbHlDb25maWcgfSBmcm9tICcuL2Zvcm1seS5jb25maWcnO1xuaW1wb3J0IHsgRk9STUxZX1ZBTElEQVRPUlMsIGV2YWxTdHJpbmdFeHByZXNzaW9uLCBldmFsRXhwcmVzc2lvblZhbHVlU2V0dGVyLCBnZXRGaWVsZElkLCBhc3NpZ25Nb2RlbFZhbHVlLCBnZXRWYWx1ZUZvcktleSwgaXNPYmplY3QsIGlzTnVsbE9yVW5kZWZpbmVkIH0gZnJvbSAnLi8uLi91dGlscyc7XG5pbXBvcnQgeyBGb3JtbHlGaWVsZENvbmZpZywgRm9ybWx5Rm9ybU9wdGlvbnMgfSBmcm9tICcuLi9jb21wb25lbnRzL2Zvcm1seS5maWVsZC5jb25maWcnO1xuaW1wb3J0IHsgZ2V0S2V5UGF0aCwgaXNVbmRlZmluZWQsIGlzRnVuY3Rpb24gfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQgeyBGb3JtbHlGb3JtRXhwcmVzc2lvbiB9IGZyb20gJy4vZm9ybWx5LmZvcm0uZXhwcmVzc2lvbic7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBGb3JtbHlGb3JtQnVpbGRlciB7XG4gIHByaXZhdGUgZm9ybUlkID0gMDtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZvcm1seUNvbmZpZzogRm9ybWx5Q29uZmlnLFxuICAgIHByaXZhdGUgZm9ybWx5Rm9ybUV4cHJlc3Npb246IEZvcm1seUZvcm1FeHByZXNzaW9uLFxuICApIHt9XG5cbiAgYnVpbGRGb3JtKGZvcm06IEZvcm1Hcm91cCB8IEZvcm1BcnJheSwgZmllbGRzOiBGb3JtbHlGaWVsZENvbmZpZ1tdID0gW10sIG1vZGVsOiBhbnksIG9wdGlvbnM6IEZvcm1seUZvcm1PcHRpb25zKSB7XG4gICAgbGV0IGZpZWxkVHJhbnNmb3JtcyA9IChvcHRpb25zICYmIG9wdGlvbnMuZmllbGRUcmFuc2Zvcm0pIHx8IHRoaXMuZm9ybWx5Q29uZmlnLmV4dHJhcy5maWVsZFRyYW5zZm9ybTtcbiAgICBpZiAoIUFycmF5LmlzQXJyYXkoZmllbGRUcmFuc2Zvcm1zKSkge1xuICAgICAgZmllbGRUcmFuc2Zvcm1zID0gW2ZpZWxkVHJhbnNmb3Jtc107XG4gICAgfVxuXG4gICAgZmllbGRUcmFuc2Zvcm1zLmZvckVhY2goZmllbGRUcmFuc2Zvcm0gPT4ge1xuICAgICAgaWYgKGZpZWxkVHJhbnNmb3JtKSB7XG4gICAgICAgIGZpZWxkcyA9IGZpZWxkVHJhbnNmb3JtKGZpZWxkcywgbW9kZWwsIGZvcm0sIG9wdGlvbnMpO1xuICAgICAgICBpZiAoIWZpZWxkcykge1xuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignZmllbGRUcmFuc2Zvcm0gbXVzdCByZXR1cm4gYW4gYXJyYXkgb2YgZmllbGRzJyk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuX2J1aWxkRm9ybShmb3JtLCBmaWVsZHMsIG1vZGVsLCBvcHRpb25zKTtcbiAgICB0aGlzLmZvcm1seUZvcm1FeHByZXNzaW9uLmNoZWNrRmllbGRzKGZvcm0sIGZpZWxkcywgbW9kZWwsIG9wdGlvbnMpO1xuICB9XG5cbiAgcHJpdmF0ZSBfYnVpbGRGb3JtKGZvcm06IEZvcm1Hcm91cCB8IEZvcm1BcnJheSwgZmllbGRzOiBGb3JtbHlGaWVsZENvbmZpZ1tdID0gW10sIG1vZGVsOiBhbnksIG9wdGlvbnM6IEZvcm1seUZvcm1PcHRpb25zKSB7XG4gICAgdGhpcy5mb3JtSWQrKztcbiAgICB0aGlzLnJlZ2lzdGVyRm9ybUNvbnRyb2xzKGZvcm0sIGZpZWxkcywgbW9kZWwsIG9wdGlvbnMpO1xuICB9XG5cbiAgcHJpdmF0ZSByZWdpc3RlckZvcm1Db250cm9scyhmb3JtOiBGb3JtR3JvdXAgfCBGb3JtQXJyYXksIGZpZWxkczogRm9ybWx5RmllbGRDb25maWdbXSwgbW9kZWw6IGFueSwgb3B0aW9uczogRm9ybWx5Rm9ybU9wdGlvbnMpIHtcbiAgICBmaWVsZHMuZm9yRWFjaCgoZmllbGQsIGluZGV4KSA9PiB7XG4gICAgICBmaWVsZC5pZCA9IGdldEZpZWxkSWQoYGZvcm1seV8ke3RoaXMuZm9ybUlkfWAsIGZpZWxkLCBpbmRleCk7XG5cbiAgICAgIGlmICghaXNVbmRlZmluZWQoZmllbGQuZGVmYXVsdFZhbHVlKSAmJiBpc1VuZGVmaW5lZChnZXRWYWx1ZUZvcktleShtb2RlbCwgZmllbGQua2V5KSkpIHtcbiAgICAgICAgYXNzaWduTW9kZWxWYWx1ZShtb2RlbCwgZmllbGQua2V5LCBmaWVsZC5kZWZhdWx0VmFsdWUpO1xuICAgICAgfVxuICAgICAgdGhpcy5pbml0RmllbGRPcHRpb25zKGZpZWxkKTtcbiAgICAgIHRoaXMuaW5pdEZpZWxkRXhwcmVzc2lvbihmaWVsZCwgbW9kZWwsIG9wdGlvbnMpO1xuICAgICAgdGhpcy5pbml0RmllbGRWYWxpZGF0aW9uKGZpZWxkKTtcbiAgICAgIHRoaXMuaW5pdEZpZWxkQXN5bmNWYWxpZGF0aW9uKGZpZWxkKTtcblxuICAgICAgaWYgKGZpZWxkLmtleSAmJiBmaWVsZC50eXBlKSB7XG4gICAgICAgIGNvbnN0IHBhdGhzID0gZ2V0S2V5UGF0aCh7IGtleTogZmllbGQua2V5IH0pO1xuICAgICAgICBsZXQgcm9vdEZvcm0gPSBmb3JtLCByb290TW9kZWwgPSBtb2RlbDtcbiAgICAgICAgcGF0aHMuZm9yRWFjaCgocGF0aCwgaW5kZXgpID0+IHtcbiAgICAgICAgICAvLyBGb3JtR3JvdXAvRm9ybUFycmF5IG9ubHkgYWxsb3cgc3RyaW5nIHZhbHVlIGZvciBwYXRoXG4gICAgICAgICAgY29uc3QgZm9ybVBhdGggPSBwYXRoLnRvU3RyaW5nKCk7XG4gICAgICAgICAgLy8gaXMgbGFzdCBpdGVtXG4gICAgICAgICAgaWYgKGluZGV4ID09PSBwYXRocy5sZW5ndGggLSAxKSB7XG4gICAgICAgICAgICB0aGlzLmFkZEZvcm1Db250cm9sKHJvb3RGb3JtLCBmaWVsZCwgcm9vdE1vZGVsLCBmb3JtUGF0aCk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGxldCBuZXN0ZWRGb3JtID0gcm9vdEZvcm0uZ2V0KGZvcm1QYXRoKSBhcyBGb3JtR3JvdXA7XG4gICAgICAgICAgICBpZiAoIW5lc3RlZEZvcm0pIHtcbiAgICAgICAgICAgICAgbmVzdGVkRm9ybSA9IG5ldyBGb3JtR3JvdXAoe30pO1xuICAgICAgICAgICAgICB0aGlzLmFkZENvbnRyb2wocm9vdEZvcm0sIGZvcm1QYXRoLCBuZXN0ZWRGb3JtKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICghcm9vdE1vZGVsW3BhdGhdKSB7XG4gICAgICAgICAgICAgIHJvb3RNb2RlbFtwYXRoXSA9IHR5cGVvZiBwYXRoID09PSAnc3RyaW5nJyA/IHt9IDogW107XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJvb3RGb3JtID0gbmVzdGVkRm9ybTtcbiAgICAgICAgICAgIHJvb3RNb2RlbCA9IHJvb3RNb2RlbFtwYXRoXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICBpZiAoZmllbGQuZmllbGRHcm91cCkge1xuICAgICAgICBpZiAoIWZpZWxkLnR5cGUpIHtcbiAgICAgICAgICBmaWVsZC50eXBlID0gJ2Zvcm1seS1ncm91cCc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZmllbGQua2V5KSB7XG4gICAgICAgICAgdGhpcy5hZGRGb3JtQ29udHJvbChmb3JtLCBmaWVsZCwgeyBbZmllbGQua2V5XTogZmllbGQuZmllbGRBcnJheSA/IFtdIDoge30gfSwgZmllbGQua2V5KTtcbiAgICAgICAgICBtb2RlbFtmaWVsZC5rZXldID0gbW9kZWxbZmllbGQua2V5XSB8fCAoZmllbGQuZmllbGRBcnJheSA/IFtdIDoge30pO1xuICAgICAgICAgIHRoaXMuX2J1aWxkRm9ybShmaWVsZC5mb3JtQ29udHJvbCBhcyBGb3JtR3JvdXAsIGZpZWxkLmZpZWxkR3JvdXAsIG1vZGVsW2ZpZWxkLmtleV0sIG9wdGlvbnMpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIC8vIGlmIGBoaWRlRXhwcmVzc2lvbmAgaXMgc2V0IGluIHRoYXQgY2FzZSB3ZSBoYXZlIHRvIGRlYWxcbiAgICAgICAgICAvLyB3aXRoIHRvZ2dsZSBGb3JtQ29udHJvbCBmb3IgZWFjaCBmaWVsZCBpbiBmaWVsZEdyb3VwIHNlcGFyYXRlbHlcbiAgICAgICAgICBpZiAoZmllbGQuaGlkZUV4cHJlc3Npb24pIHtcbiAgICAgICAgICAgIGZpZWxkLmZpZWxkR3JvdXAuZm9yRWFjaChmID0+IHtcbiAgICAgICAgICAgICAgbGV0IGhpZGVFeHByZXNzaW9uOiBhbnkgPSBmLmhpZGVFeHByZXNzaW9uIHx8ICgoKSA9PiBmYWxzZSk7XG4gICAgICAgICAgICAgIGlmICh0eXBlb2YgaGlkZUV4cHJlc3Npb24gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgICAgaGlkZUV4cHJlc3Npb24gPSBldmFsU3RyaW5nRXhwcmVzc2lvbihoaWRlRXhwcmVzc2lvbiwgWydtb2RlbCcsICdmb3JtU3RhdGUnXSk7XG4gICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICBmLmhpZGVFeHByZXNzaW9uID0gKG1vZGVsLCBmb3JtU3RhdGUpID0+IGZpZWxkLmhpZGUgfHwgaGlkZUV4cHJlc3Npb24obW9kZWwsIGZvcm1TdGF0ZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5fYnVpbGRGb3JtKGZvcm0sIGZpZWxkLmZpZWxkR3JvdXAsIG1vZGVsLCBvcHRpb25zKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBpbml0RmllbGRFeHByZXNzaW9uKGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZywgbW9kZWw6IGFueSwgb3B0aW9uczogRm9ybWx5Rm9ybU9wdGlvbnMpIHtcbiAgICBpZiAoZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXMpIHtcbiAgICAgIGZvciAoY29uc3Qga2V5IGluIGZpZWxkLmV4cHJlc3Npb25Qcm9wZXJ0aWVzIGFzIGFueSkge1xuICAgICAgICBpZiAodHlwZW9mIGZpZWxkLmV4cHJlc3Npb25Qcm9wZXJ0aWVzW2tleV0gPT09ICdzdHJpbmcnIHx8IGlzRnVuY3Rpb24oZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXNba2V5XSkpIHtcbiAgICAgICAgICAvLyBjYWNoZSBidWlsdCBleHByZXNzaW9uXG4gICAgICAgICAgZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXNba2V5XSA9IHtcbiAgICAgICAgICAgIGV4cHJlc3Npb246IGlzRnVuY3Rpb24oZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXNba2V5XSkgPyBmaWVsZC5leHByZXNzaW9uUHJvcGVydGllc1trZXldIDogZXZhbFN0cmluZ0V4cHJlc3Npb24oZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXNba2V5XSwgWydtb2RlbCcsICdmb3JtU3RhdGUnXSksXG4gICAgICAgICAgICBleHByZXNzaW9uVmFsdWVTZXR0ZXI6IGV2YWxFeHByZXNzaW9uVmFsdWVTZXR0ZXIoa2V5LCBbJ2V4cHJlc3Npb25WYWx1ZScsICdtb2RlbCcsICd0ZW1wbGF0ZU9wdGlvbnMnLCAndmFsaWRhdGlvbicsICdmaWVsZCddKSxcbiAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGZpZWxkLmhpZGVFeHByZXNzaW9uKSB7XG4gICAgICAvLyBkZWxldGUgaGlkZSB2YWx1ZSBpbiBvcmRlciB0byBmb3JjZSByZS1ldmFsdXRlIGl0IGluIEZvcm1seUZvcm1FeHByZXNzaW9uLlxuICAgICAgZGVsZXRlIGZpZWxkLmhpZGU7XG4gICAgICBpZiAodHlwZW9mIGZpZWxkLmhpZGVFeHByZXNzaW9uID09PSAnc3RyaW5nJykge1xuICAgICAgICAvLyBjYWNoZSBidWlsdCBleHByZXNzaW9uXG4gICAgICAgIGZpZWxkLmhpZGVFeHByZXNzaW9uID0gZXZhbFN0cmluZ0V4cHJlc3Npb24oZmllbGQuaGlkZUV4cHJlc3Npb24sIFsnbW9kZWwnLCAnZm9ybVN0YXRlJ10pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgaW5pdEZpZWxkT3B0aW9ucyhmaWVsZDogRm9ybWx5RmllbGRDb25maWcpIHtcbiAgICBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMgPSBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMgfHwge307XG4gICAgaWYgKGZpZWxkLnR5cGUpIHtcbiAgICAgIHRoaXMuZm9ybWx5Q29uZmlnLmdldE1lcmdlZEZpZWxkKGZpZWxkKTtcbiAgICAgIGlmIChmaWVsZC5rZXkpIHtcbiAgICAgICAgZmllbGQudGVtcGxhdGVPcHRpb25zID0gT2JqZWN0LmFzc2lnbih7XG4gICAgICAgICAgbGFiZWw6ICcnLFxuICAgICAgICAgIHBsYWNlaG9sZGVyOiAnJyxcbiAgICAgICAgICBmb2N1czogZmFsc2UsXG4gICAgICAgIH0sIGZpZWxkLnRlbXBsYXRlT3B0aW9ucyk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBpbml0RmllbGRBc3luY1ZhbGlkYXRpb24oZmllbGQ6IEZvcm1seUZpZWxkQ29uZmlnKSB7XG4gICAgY29uc3QgdmFsaWRhdG9yczogYW55ID0gW107XG4gICAgaWYgKGZpZWxkLmFzeW5jVmFsaWRhdG9ycykge1xuICAgICAgZm9yIChjb25zdCB2YWxpZGF0b3JOYW1lIGluIGZpZWxkLmFzeW5jVmFsaWRhdG9ycykge1xuICAgICAgICBpZiAodmFsaWRhdG9yTmFtZSAhPT0gJ3ZhbGlkYXRpb24nKSB7XG4gICAgICAgICAgdmFsaWRhdG9ycy5wdXNoKChjb250cm9sOiBGb3JtQ29udHJvbCkgPT4ge1xuICAgICAgICAgICAgbGV0IHZhbGlkYXRvciA9IGZpZWxkLmFzeW5jVmFsaWRhdG9yc1t2YWxpZGF0b3JOYW1lXTtcbiAgICAgICAgICAgIGlmIChpc09iamVjdCh2YWxpZGF0b3IpKSB7XG4gICAgICAgICAgICAgIHZhbGlkYXRvciA9IHZhbGlkYXRvci5leHByZXNzaW9uO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcbiAgICAgICAgICAgICAgcmV0dXJuIHZhbGlkYXRvcihjb250cm9sLCBmaWVsZCkudGhlbigocmVzdWx0OiBib29sZWFuKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQgPyBudWxsIDoge1t2YWxpZGF0b3JOYW1lXTogdHJ1ZX0pO1xuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIGlmIChmaWVsZC5hc3luY1ZhbGlkYXRvcnMgJiYgQXJyYXkuaXNBcnJheShmaWVsZC5hc3luY1ZhbGlkYXRvcnMudmFsaWRhdGlvbikpIHtcbiAgICAgIGZpZWxkLmFzeW5jVmFsaWRhdG9ycy52YWxpZGF0aW9uLmZvckVhY2goKHZhbGlkYXRlOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWxpZGF0ZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICB2YWxpZGF0b3JzLnB1c2godGhpcy5mb3JtbHlDb25maWcuZ2V0VmFsaWRhdG9yKHZhbGlkYXRlKS52YWxpZGF0aW9uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWxpZGF0b3JzLnB1c2godmFsaWRhdGUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAodmFsaWRhdG9ycy5sZW5ndGgpIHtcbiAgICAgIGlmIChmaWVsZC5hc3luY1ZhbGlkYXRvcnMgJiYgIUFycmF5LmlzQXJyYXkoZmllbGQuYXN5bmNWYWxpZGF0b3JzLnZhbGlkYXRpb24pKSB7XG4gICAgICAgIGZpZWxkLmFzeW5jVmFsaWRhdG9ycy52YWxpZGF0aW9uID0gVmFsaWRhdG9ycy5jb21wb3NlQXN5bmMoW2ZpZWxkLmFzeW5jVmFsaWRhdG9ycy52YWxpZGF0aW9uLCAuLi52YWxpZGF0b3JzXSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBmaWVsZC5hc3luY1ZhbGlkYXRvcnMgPSB7XG4gICAgICAgICAgdmFsaWRhdGlvbjogVmFsaWRhdG9ycy5jb21wb3NlQXN5bmModmFsaWRhdG9ycyksXG4gICAgICAgIH07XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBpbml0RmllbGRWYWxpZGF0aW9uKGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZykge1xuICAgIGNvbnN0IHZhbGlkYXRvcnM6IGFueSA9IFtdO1xuICAgIEZPUk1MWV9WQUxJREFUT1JTXG4gICAgICAuZmlsdGVyKG9wdCA9PiAoZmllbGQudGVtcGxhdGVPcHRpb25zICYmIGZpZWxkLnRlbXBsYXRlT3B0aW9ucy5oYXNPd25Qcm9wZXJ0eShvcHQpKVxuICAgICAgICB8fCAoZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXMgJiYgZmllbGQuZXhwcmVzc2lvblByb3BlcnRpZXNbYHRlbXBsYXRlT3B0aW9ucy4ke29wdH1gXSksXG4gICAgICApXG4gICAgICAuZm9yRWFjaCgob3B0KSA9PiB7XG4gICAgICAgIHZhbGlkYXRvcnMucHVzaCgoY29udHJvbDogRm9ybUNvbnRyb2wpID0+IHtcbiAgICAgICAgICBpZiAoZmllbGQudGVtcGxhdGVPcHRpb25zW29wdF0gPT09IGZhbHNlKSB7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4gdGhpcy5nZXRWYWxpZGF0aW9uKG9wdCwgZmllbGQudGVtcGxhdGVPcHRpb25zW29wdF0pKGNvbnRyb2wpO1xuICAgICAgICB9KTtcbiAgICAgIH0pO1xuXG4gICAgaWYgKGZpZWxkLnZhbGlkYXRvcnMpIHtcbiAgICAgIGZvciAoY29uc3QgdmFsaWRhdG9yTmFtZSBpbiBmaWVsZC52YWxpZGF0b3JzKSB7XG4gICAgICAgIGlmICh2YWxpZGF0b3JOYW1lICE9PSAndmFsaWRhdGlvbicpIHtcbiAgICAgICAgICB2YWxpZGF0b3JzLnB1c2goKGNvbnRyb2w6IEZvcm1Db250cm9sKSA9PiB7XG4gICAgICAgICAgICBsZXQgdmFsaWRhdG9yID0gZmllbGQudmFsaWRhdG9yc1t2YWxpZGF0b3JOYW1lXTtcbiAgICAgICAgICAgIGlmIChpc09iamVjdCh2YWxpZGF0b3IpKSB7XG4gICAgICAgICAgICAgIHZhbGlkYXRvciA9IHZhbGlkYXRvci5leHByZXNzaW9uO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gdmFsaWRhdG9yKGNvbnRyb2wsIGZpZWxkKSA/IG51bGwgOiB7W3ZhbGlkYXRvck5hbWVdOiB0cnVlfTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChmaWVsZC52YWxpZGF0b3JzICYmIEFycmF5LmlzQXJyYXkoZmllbGQudmFsaWRhdG9ycy52YWxpZGF0aW9uKSkge1xuICAgICAgZmllbGQudmFsaWRhdG9ycy52YWxpZGF0aW9uLmZvckVhY2goKHZhbGlkYXRlOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKHR5cGVvZiB2YWxpZGF0ZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICB2YWxpZGF0b3JzLnB1c2godGhpcy5mb3JtbHlDb25maWcuZ2V0VmFsaWRhdG9yKHZhbGlkYXRlKS52YWxpZGF0aW9uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB2YWxpZGF0b3JzLnB1c2godmFsaWRhdGUpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICBpZiAodmFsaWRhdG9ycy5sZW5ndGgpIHtcbiAgICAgIGlmIChmaWVsZC52YWxpZGF0b3JzICYmICFBcnJheS5pc0FycmF5KGZpZWxkLnZhbGlkYXRvcnMudmFsaWRhdGlvbikpIHtcbiAgICAgICAgZmllbGQudmFsaWRhdG9ycy52YWxpZGF0aW9uID0gVmFsaWRhdG9ycy5jb21wb3NlKFtmaWVsZC52YWxpZGF0b3JzLnZhbGlkYXRpb24sIC4uLnZhbGlkYXRvcnNdKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZpZWxkLnZhbGlkYXRvcnMgPSB7XG4gICAgICAgICAgdmFsaWRhdGlvbjogVmFsaWRhdG9ycy5jb21wb3NlKHZhbGlkYXRvcnMpLFxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYWRkRm9ybUNvbnRyb2woZm9ybTogRm9ybUdyb3VwIHwgRm9ybUFycmF5LCBmaWVsZDogRm9ybWx5RmllbGRDb25maWcsIG1vZGVsOiBhbnksIHBhdGg6IHN0cmluZykge1xuICAgIGxldCBjb250cm9sOiBBYnN0cmFjdENvbnRyb2w7XG4gICAgaWYgKGZpZWxkLmZvcm1Db250cm9sIGluc3RhbmNlb2YgQWJzdHJhY3RDb250cm9sIHx8IGZvcm0uZ2V0KHBhdGgpKSB7XG4gICAgICBjb250cm9sID0gZmllbGQuZm9ybUNvbnRyb2wgfHwgZm9ybS5nZXQocGF0aCk7XG4gICAgICBpZiAoXG4gICAgICAgICEoaXNOdWxsT3JVbmRlZmluZWQoY29udHJvbC52YWx1ZSkgJiYgaXNOdWxsT3JVbmRlZmluZWQobW9kZWxbcGF0aF0pKVxuICAgICAgICAmJiBjb250cm9sLnZhbHVlICE9PSBtb2RlbFtwYXRoXVxuICAgICAgICAmJiBjb250cm9sIGluc3RhbmNlb2YgRm9ybUNvbnRyb2xcbiAgICAgICkge1xuICAgICAgICBjb250cm9sLnBhdGNoVmFsdWUobW9kZWxbcGF0aF0pO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoZmllbGQuY29tcG9uZW50ICYmIGZpZWxkLmNvbXBvbmVudC5jcmVhdGVDb250cm9sKSB7XG4gICAgICBjb250cm9sID0gZmllbGQuY29tcG9uZW50LmNyZWF0ZUNvbnRyb2wobW9kZWxbcGF0aF0sIGZpZWxkKTtcbiAgICB9IGVsc2UgaWYgKGZpZWxkLmZpZWxkR3JvdXAgJiYgZmllbGQua2V5ICYmIGZpZWxkLmtleSA9PT0gcGF0aCAmJiAhZmllbGQuZmllbGRBcnJheSkge1xuICAgICAgY29udHJvbCA9IG5ldyBGb3JtR3JvdXAoXG4gICAgICAgIG1vZGVsW3BhdGhdLFxuICAgICAgICBmaWVsZC52YWxpZGF0b3JzID8gZmllbGQudmFsaWRhdG9ycy52YWxpZGF0aW9uIDogdW5kZWZpbmVkLFxuICAgICAgICBmaWVsZC5hc3luY1ZhbGlkYXRvcnMgPyBmaWVsZC5hc3luY1ZhbGlkYXRvcnMudmFsaWRhdGlvbiA6IHVuZGVmaW5lZCxcbiAgICAgICk7XG4gICAgfSBlbHNlIGlmIChmaWVsZC5maWVsZEFycmF5ICYmIGZpZWxkLmtleSAmJiBmaWVsZC5rZXkgPT09IHBhdGgpIHtcbiAgICAgIGNvbnRyb2wgPSBuZXcgRm9ybUFycmF5KFxuICAgICAgICBbXSxcbiAgICAgICAgZmllbGQudmFsaWRhdG9ycyA/IGZpZWxkLnZhbGlkYXRvcnMudmFsaWRhdGlvbiA6IHVuZGVmaW5lZCxcbiAgICAgICAgZmllbGQuYXN5bmNWYWxpZGF0b3JzID8gZmllbGQuYXN5bmNWYWxpZGF0b3JzLnZhbGlkYXRpb24gOiB1bmRlZmluZWQsXG4gICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb250cm9sID0gbmV3IEZvcm1Db250cm9sKFxuICAgICAgICBtb2RlbFtwYXRoXSxcbiAgICAgICAgZmllbGQudmFsaWRhdG9ycyA/IGZpZWxkLnZhbGlkYXRvcnMudmFsaWRhdGlvbiA6IHVuZGVmaW5lZCxcbiAgICAgICAgZmllbGQuYXN5bmNWYWxpZGF0b3JzID8gZmllbGQuYXN5bmNWYWxpZGF0b3JzLnZhbGlkYXRpb24gOiB1bmRlZmluZWQsXG4gICAgICApO1xuICAgIH1cblxuICAgIGlmIChmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuZGlzYWJsZWQpIHtcbiAgICAgIGNvbnRyb2wuZGlzYWJsZSgpO1xuICAgIH1cblxuICAgIC8vIFJlcGxhY2UgZGVjb3JhdGVkIHByb3BlcnR5IHdpdGggYSBnZXR0ZXIgdGhhdCByZXR1cm5zIHRoZSBvYnNlcnZhYmxlLlxuICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyLXJlZHV4L3N0b3JlL2Jsb2IvbWFzdGVyL3NyYy9kZWNvcmF0b3JzL3NlbGVjdC50cyNMNzktTDg1XG4gICAgaWYgKGRlbGV0ZSBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuZGlzYWJsZWQpIHtcbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShmaWVsZC50ZW1wbGF0ZU9wdGlvbnMsICdkaXNhYmxlZCcsIHtcbiAgICAgICAgZ2V0OiAoZnVuY3Rpb24gKCkgeyByZXR1cm4gIXRoaXMuZm9ybUNvbnRyb2wuZW5hYmxlZDsgfSkuYmluZChmaWVsZCksXG4gICAgICAgIHNldDogKGZ1bmN0aW9uICh2YWx1ZTogYm9vbGVhbikge1xuICAgICAgICAgIGlmICh0aGlzLmV4cHJlc3Npb25Qcm9wZXJ0aWVzICYmIHRoaXMuZXhwcmVzc2lvblByb3BlcnRpZXMuaGFzT3duUHJvcGVydHkoJ3RlbXBsYXRlT3B0aW9ucy5kaXNhYmxlZCcpKSB7XG4gICAgICAgICAgICB0aGlzLmV4cHJlc3Npb25Qcm9wZXJ0aWVzWyd0ZW1wbGF0ZU9wdGlvbnMuZGlzYWJsZWQnXS5leHByZXNzaW9uVmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB2YWx1ZSA/IHRoaXMuZm9ybUNvbnRyb2wuZGlzYWJsZSgpIDogdGhpcy5mb3JtQ29udHJvbC5lbmFibGUoKTtcbiAgICAgICAgfSkuYmluZChmaWVsZCksXG4gICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIHRoaXMuYWRkQ29udHJvbChmb3JtLCBwYXRoLCBjb250cm9sLCBmaWVsZCk7XG4gIH1cblxuICBwcml2YXRlIGFkZENvbnRyb2woZm9ybTogRm9ybUdyb3VwIHwgRm9ybUFycmF5LCBrZXk6IHN0cmluZyB8IG51bWJlciwgZm9ybUNvbnRyb2w6IEFic3RyYWN0Q29udHJvbCwgZmllbGQ/OiBGb3JtbHlGaWVsZENvbmZpZykge1xuICAgIGlmIChmaWVsZCkge1xuICAgICAgZmllbGQuZm9ybUNvbnRyb2wgPSBmb3JtQ29udHJvbDtcbiAgICB9XG5cbiAgICBpZiAoZm9ybSBpbnN0YW5jZW9mIEZvcm1BcnJheSkge1xuICAgICAgaWYgKGZvcm0uYXQoPG51bWJlcj4ga2V5KSAhPT0gZm9ybUNvbnRyb2wpIHtcbiAgICAgICAgZm9ybS5zZXRDb250cm9sKDxudW1iZXI+a2V5LCBmb3JtQ29udHJvbCk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGlmIChmb3JtLmdldCg8c3RyaW5nPiBrZXkpICE9PSBmb3JtQ29udHJvbCkge1xuICAgICAgICBmb3JtLnNldENvbnRyb2woPHN0cmluZz5rZXksIGZvcm1Db250cm9sKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGdldFZhbGlkYXRpb24ob3B0OiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcbiAgICBzd2l0Y2ggKG9wdCkge1xuICAgICAgY2FzZSAncmVxdWlyZWQnOlxuICAgICAgICByZXR1cm4gVmFsaWRhdG9ycy5yZXF1aXJlZDtcbiAgICAgIGNhc2UgJ3BhdHRlcm4nOlxuICAgICAgICByZXR1cm4gVmFsaWRhdG9ycy5wYXR0ZXJuKHZhbHVlKTtcbiAgICAgIGNhc2UgJ21pbkxlbmd0aCc6XG4gICAgICAgIHJldHVybiBWYWxpZGF0b3JzLm1pbkxlbmd0aCh2YWx1ZSk7XG4gICAgICBjYXNlICdtYXhMZW5ndGgnOlxuICAgICAgICByZXR1cm4gVmFsaWRhdG9ycy5tYXhMZW5ndGgodmFsdWUpO1xuICAgICAgY2FzZSAnbWluJzpcbiAgICAgICAgcmV0dXJuIFZhbGlkYXRvcnMubWluKHZhbHVlKTtcbiAgICAgIGNhc2UgJ21heCc6XG4gICAgICAgIHJldHVybiBWYWxpZGF0b3JzLm1heCh2YWx1ZSk7XG4gICAgfVxuICB9XG59XG4iXX0=
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
function createGenericTestComponent(html, type) {
    testing_1.TestBed.overrideComponent(type, { set: { template: html } });
    var fixture = testing_1.TestBed.createComponent(type);
    fixture.detectChanges();
    return fixture;
}
exports.createGenericTestComponent = createGenericTestComponent;
// Source copied from https://github.com/cnunciato/ng2-mock-component
function MockComponent(options) {
    var metadata = {
        selector: options.selector,
        template: options.template || '',
        inputs: options.inputs,
        outputs: options.outputs || [],
        exportAs: options.exportAs || '',
    };
    var Mock = /** @class */ (function () {
        function Mock() {
        }
        return Mock;
    }());
    metadata.outputs.forEach(function (method) {
        Mock.prototype[method] = new core_1.EventEmitter();
    });
    return core_1.Component(metadata)(Mock);
}
exports.MockComponent = MockComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGVzdC11dGlscy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInRlc3QtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpREFBa0U7QUFDbEUsc0NBQXdEO0FBRXhELG9DQUE4QyxJQUFZLEVBQUUsSUFBK0I7SUFDekYsaUJBQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsRUFBQyxHQUFHLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ3pELElBQU0sT0FBTyxHQUFHLGlCQUFPLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN4QixNQUFNLENBQUMsT0FBOEIsQ0FBQztBQUN4QyxDQUFDO0FBTEQsZ0VBS0M7QUFFRCxxRUFBcUU7QUFDckUsdUJBQThCLE9BQWtCO0lBQzlDLElBQU0sUUFBUSxHQUFjO1FBQzFCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUTtRQUMxQixRQUFRLEVBQUUsT0FBTyxDQUFDLFFBQVEsSUFBSSxFQUFFO1FBQ2hDLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTTtRQUN0QixPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU8sSUFBSSxFQUFFO1FBQzlCLFFBQVEsRUFBRSxPQUFPLENBQUMsUUFBUSxJQUFJLEVBQUU7S0FDakMsQ0FBQztJQUVGO1FBQUE7UUFBWSxDQUFDO1FBQUQsV0FBQztJQUFELENBQUMsQUFBYixJQUFhO0lBRWIsUUFBUSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsVUFBQSxNQUFNO1FBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7SUFDbkQsQ0FBQyxDQUFDLENBQUM7SUFFSCxNQUFNLENBQUMsZ0JBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFXLENBQUMsQ0FBQztBQUMxQyxDQUFDO0FBaEJELHNDQWdCQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFRlc3RCZWQsIENvbXBvbmVudEZpeHR1cmUgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZXhwb3J0IGZ1bmN0aW9uIGNyZWF0ZUdlbmVyaWNUZXN0Q29tcG9uZW50PFQ+KGh0bWw6IHN0cmluZywgdHlwZToge25ldyAoLi4uYXJnczogYW55W10pOiBUfSk6IENvbXBvbmVudEZpeHR1cmU8VD4ge1xuICBUZXN0QmVkLm92ZXJyaWRlQ29tcG9uZW50KHR5cGUsIHtzZXQ6IHt0ZW1wbGF0ZTogaHRtbH19KTtcbiAgY29uc3QgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KHR5cGUpO1xuICBmaXh0dXJlLmRldGVjdENoYW5nZXMoKTtcbiAgcmV0dXJuIGZpeHR1cmUgYXMgQ29tcG9uZW50Rml4dHVyZTxUPjtcbn1cblxuLy8gU291cmNlIGNvcGllZCBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9jbnVuY2lhdG8vbmcyLW1vY2stY29tcG9uZW50XG5leHBvcnQgZnVuY3Rpb24gTW9ja0NvbXBvbmVudChvcHRpb25zOiBDb21wb25lbnQpOiBDb21wb25lbnQge1xuICBjb25zdCBtZXRhZGF0YTogQ29tcG9uZW50ID0ge1xuICAgIHNlbGVjdG9yOiBvcHRpb25zLnNlbGVjdG9yLFxuICAgIHRlbXBsYXRlOiBvcHRpb25zLnRlbXBsYXRlIHx8ICcnLFxuICAgIGlucHV0czogb3B0aW9ucy5pbnB1dHMsXG4gICAgb3V0cHV0czogb3B0aW9ucy5vdXRwdXRzIHx8IFtdLFxuICAgIGV4cG9ydEFzOiBvcHRpb25zLmV4cG9ydEFzIHx8ICcnLFxuICB9O1xuXG4gIGNsYXNzIE1vY2sge31cblxuICBtZXRhZGF0YS5vdXRwdXRzLmZvckVhY2gobWV0aG9kID0+IHtcbiAgICBNb2NrLnByb3RvdHlwZVttZXRob2RdID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XG4gIH0pO1xuXG4gIHJldHVybiBDb21wb25lbnQobWV0YWRhdGEpKE1vY2sgYXMgYW55KTtcbn1cblxuIl19
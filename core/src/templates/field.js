"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var Field = /** @class */ (function () {
    function Field() {
    }
    Object.defineProperty(Field.prototype, "key", {
        get: function () { return this.field.key; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "formControl", {
        get: function () { return this.field.formControl || this.form.get(this.key); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "to", {
        get: function () { return this.field.templateOptions; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "showError", {
        get: function () { return this.options.showError(this); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "id", {
        get: function () { return this.field.id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Field.prototype, "formState", {
        get: function () { return this.options.formState || {}; },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.FormGroup)
    ], Field.prototype, "form", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Field.prototype, "field", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Field.prototype, "model", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], Field.prototype, "options", void 0);
    return Field;
}());
exports.Field = Field;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmaWVsZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFzQztBQUN0Qyx3Q0FBNEQ7QUFHNUQ7SUFBQTtJQWlCQSxDQUFDO0lBWEMsc0JBQUksc0JBQUc7YUFBUCxjQUFZLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRXBDLHNCQUFJLDhCQUFXO2FBQWYsY0FBcUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRWhHLHNCQUFJLHFCQUFFO2FBQU4sY0FBa0MsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFdEUsc0JBQUksNEJBQVM7YUFBYixjQUEyQixNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUVqRSxzQkFBSSxxQkFBRTthQUFOLGNBQW1CLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRTFDLHNCQUFJLDRCQUFTO2FBQWIsY0FBa0IsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBZi9DO1FBQVIsWUFBSyxFQUFFO2tDQUFPLGlCQUFTO3VDQUFDO0lBQ2hCO1FBQVIsWUFBSyxFQUFFOzt3Q0FBMEI7SUFDekI7UUFBUixZQUFLLEVBQUU7O3dDQUFZO0lBQ1g7UUFBUixZQUFLLEVBQUU7OzBDQUE0QjtJQWF0QyxZQUFDO0NBQUEsQUFqQkQsSUFpQkM7QUFqQnFCLHNCQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCwgQWJzdHJhY3RDb250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgRm9ybWx5VGVtcGxhdGVPcHRpb25zLCBGb3JtbHlGaWVsZENvbmZpZywgRm9ybWx5Rm9ybU9wdGlvbnMgfSBmcm9tICcuLi9jb21wb25lbnRzL2Zvcm1seS5maWVsZC5jb25maWcnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRmllbGQge1xuICBASW5wdXQoKSBmb3JtOiBGb3JtR3JvdXA7XG4gIEBJbnB1dCgpIGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZztcbiAgQElucHV0KCkgbW9kZWw6IGFueTtcbiAgQElucHV0KCkgb3B0aW9uczogRm9ybWx5Rm9ybU9wdGlvbnM7XG5cbiAgZ2V0IGtleSgpIHsgcmV0dXJuIHRoaXMuZmllbGQua2V5OyB9XG5cbiAgZ2V0IGZvcm1Db250cm9sKCk6IEFic3RyYWN0Q29udHJvbCB7IHJldHVybiB0aGlzLmZpZWxkLmZvcm1Db250cm9sIHx8IHRoaXMuZm9ybS5nZXQodGhpcy5rZXkpOyB9XG5cbiAgZ2V0IHRvKCk6IEZvcm1seVRlbXBsYXRlT3B0aW9ucyB7IHJldHVybiB0aGlzLmZpZWxkLnRlbXBsYXRlT3B0aW9uczsgfVxuXG4gIGdldCBzaG93RXJyb3IoKTogYm9vbGVhbiB7IHJldHVybiB0aGlzLm9wdGlvbnMuc2hvd0Vycm9yKHRoaXMpOyB9XG5cbiAgZ2V0IGlkKCk6IHN0cmluZyB7IHJldHVybiB0aGlzLmZpZWxkLmlkOyB9XG5cbiAgZ2V0IGZvcm1TdGF0ZSgpIHsgcmV0dXJuIHRoaXMub3B0aW9ucy5mb3JtU3RhdGUgfHwge307IH1cbn1cbiJdfQ==
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("./field");
var FieldType = /** @class */ (function (_super) {
    __extends(FieldType, _super);
    function FieldType() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FieldType.prototype.ngOnInit = function () {
        this.lifeCycleHooks(this.lifecycle.onInit);
    };
    FieldType.prototype.ngOnChanges = function (changes) {
        this.lifeCycleHooks(this.lifecycle.onChanges);
    };
    FieldType.prototype.ngDoCheck = function () {
        this.lifeCycleHooks(this.lifecycle.doCheck);
    };
    FieldType.prototype.ngAfterContentInit = function () {
        this.lifeCycleHooks(this.lifecycle.afterContentInit);
    };
    FieldType.prototype.ngAfterContentChecked = function () {
        this.lifeCycleHooks(this.lifecycle.afterContentChecked);
    };
    FieldType.prototype.ngAfterViewInit = function () {
        this.lifeCycleHooks(this.lifecycle.afterViewInit);
    };
    FieldType.prototype.ngAfterViewChecked = function () {
        this.lifeCycleHooks(this.lifecycle.afterViewChecked);
    };
    FieldType.prototype.ngOnDestroy = function () {
        this.lifeCycleHooks(this.lifecycle.onDestroy);
    };
    Object.defineProperty(FieldType.prototype, "lifecycle", {
        get: function () {
            return this.field.lifecycle || {};
        },
        enumerable: true,
        configurable: true
    });
    FieldType.prototype.lifeCycleHooks = function (callback) {
        if (callback) {
            callback.bind(this)(this.form, this.field, this.model, this.options);
        }
    };
    return FieldType;
}(field_1.Field));
exports.FieldType = FieldType;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQudHlwZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpZWxkLnR5cGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxpQ0FBZ0M7QUFJaEM7SUFBd0MsNkJBQUs7SUFBN0M7O0lBMENBLENBQUM7SUF6Q0MsNEJBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM3QyxDQUFDO0lBRUQsK0JBQVcsR0FBWCxVQUFZLE9BQXNCO1FBQ2hDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsNkJBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsc0NBQWtCLEdBQWxCO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVELHlDQUFxQixHQUFyQjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRCxtQ0FBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRCxzQ0FBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsK0JBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsc0JBQVksZ0NBQVM7YUFBckI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBRU8sa0NBQWMsR0FBdEIsVUFBdUIsUUFBMkI7UUFDaEQsRUFBRSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUNiLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3ZFLENBQUM7SUFDSCxDQUFDO0lBQ0gsZ0JBQUM7QUFBRCxDQUFDLEFBMUNELENBQXdDLGFBQUssR0EwQzVDO0FBMUNxQiw4QkFBUyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZpZWxkIH0gZnJvbSAnLi9maWVsZCc7XG5pbXBvcnQgeyBPbkluaXQsIE9uQ2hhbmdlcywgRG9DaGVjaywgQWZ0ZXJDb250ZW50SW5pdCwgQWZ0ZXJDb250ZW50Q2hlY2tlZCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95LCBBZnRlclZpZXdDaGVja2VkLCBTaW1wbGVDaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtbHlMaWZlQ3ljbGVPcHRpb25zLCBGb3JtbHlMaWZlQ3ljbGVGbiB9IGZyb20gJy4vLi4vY29tcG9uZW50cy9mb3JtbHkuZmllbGQuY29uZmlnJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEZpZWxkVHlwZSBleHRlbmRzIEZpZWxkIGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMsIERvQ2hlY2ssIEFmdGVyQ29udGVudEluaXQsIEFmdGVyQ29udGVudENoZWNrZWQsIEFmdGVyVmlld0luaXQsIEFmdGVyVmlld0NoZWNrZWQsIE9uRGVzdHJveSB7XG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMubGlmZUN5Y2xlSG9va3ModGhpcy5saWZlY3ljbGUub25Jbml0KTtcbiAgfVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICB0aGlzLmxpZmVDeWNsZUhvb2tzKHRoaXMubGlmZWN5Y2xlLm9uQ2hhbmdlcyk7XG4gIH1cblxuICBuZ0RvQ2hlY2soKSB7XG4gICAgdGhpcy5saWZlQ3ljbGVIb29rcyh0aGlzLmxpZmVjeWNsZS5kb0NoZWNrKTtcbiAgfVxuXG4gIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcbiAgICB0aGlzLmxpZmVDeWNsZUhvb2tzKHRoaXMubGlmZWN5Y2xlLmFmdGVyQ29udGVudEluaXQpO1xuICB9XG5cbiAgbmdBZnRlckNvbnRlbnRDaGVja2VkKCkge1xuICAgIHRoaXMubGlmZUN5Y2xlSG9va3ModGhpcy5saWZlY3ljbGUuYWZ0ZXJDb250ZW50Q2hlY2tlZCk7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgdGhpcy5saWZlQ3ljbGVIb29rcyh0aGlzLmxpZmVjeWNsZS5hZnRlclZpZXdJbml0KTtcbiAgfVxuXG4gIG5nQWZ0ZXJWaWV3Q2hlY2tlZCgpIHtcbiAgICB0aGlzLmxpZmVDeWNsZUhvb2tzKHRoaXMubGlmZWN5Y2xlLmFmdGVyVmlld0NoZWNrZWQpO1xuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgdGhpcy5saWZlQ3ljbGVIb29rcyh0aGlzLmxpZmVjeWNsZS5vbkRlc3Ryb3kpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXQgbGlmZWN5Y2xlKCk6IEZvcm1seUxpZmVDeWNsZU9wdGlvbnMge1xuICAgIHJldHVybiB0aGlzLmZpZWxkLmxpZmVjeWNsZSB8fCB7fTtcbiAgfVxuXG4gIHByaXZhdGUgbGlmZUN5Y2xlSG9va3MoY2FsbGJhY2s6IEZvcm1seUxpZmVDeWNsZUZuKSB7XG4gICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICBjYWxsYmFjay5iaW5kKHRoaXMpKHRoaXMuZm9ybSwgdGhpcy5maWVsZCwgdGhpcy5tb2RlbCwgdGhpcy5vcHRpb25zKTtcbiAgICB9XG4gIH1cbn1cbiJdfQ==
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var field_1 = require("./field");
var FieldWrapper = /** @class */ (function (_super) {
    __extends(FieldWrapper, _super);
    function FieldWrapper() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return FieldWrapper;
}(field_1.Field));
exports.FieldWrapper = FieldWrapper;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGQud3JhcHBlci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZpZWxkLndyYXBwZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxpQ0FBZ0M7QUFFaEM7SUFBMkMsZ0NBQUs7SUFBaEQ7O0lBRUEsQ0FBQztJQUFELG1CQUFDO0FBQUQsQ0FBQyxBQUZELENBQTJDLGFBQUssR0FFL0M7QUFGcUIsb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4vZmllbGQnO1xuXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRmllbGRXcmFwcGVyIGV4dGVuZHMgRmllbGQge1xuICBmaWVsZENvbXBvbmVudDogVmlld0NvbnRhaW5lclJlZjtcbn1cbiJdfQ==
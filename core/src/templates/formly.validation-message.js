"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var formly_config_1 = require("../services/formly.config");
var FormlyValidationMessage = /** @class */ (function () {
    function FormlyValidationMessage(formlyConfig) {
        this.formlyConfig = formlyConfig;
    }
    Object.defineProperty(FormlyValidationMessage.prototype, "errorMessage", {
        get: function () {
            for (var error in this.fieldForm.errors) {
                if (this.fieldForm.errors.hasOwnProperty(error)) {
                    var message = this.formlyConfig.getValidatorMessage(error);
                    if (this.field.validation && this.field.validation.messages && this.field.validation.messages[error]) {
                        message = this.field.validation.messages[error];
                    }
                    if (this.field.validators && this.field.validators[error] && this.field.validators[error].message) {
                        message = this.field.validators[error].message;
                    }
                    if (this.field.asyncValidators && this.field.asyncValidators[error] && this.field.asyncValidators[error].message) {
                        message = this.field.asyncValidators[error].message;
                    }
                    if (typeof message === 'function') {
                        return message(this.fieldForm.errors[error], this.field);
                    }
                    return message;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.FormControl)
    ], FormlyValidationMessage.prototype, "fieldForm", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyValidationMessage.prototype, "field", void 0);
    FormlyValidationMessage = __decorate([
        core_1.Component({
            selector: 'formly-validation-message',
            template: "{{ errorMessage }}",
        }),
        __metadata("design:paramtypes", [formly_config_1.FormlyConfig])
    ], FormlyValidationMessage);
    return FormlyValidationMessage;
}());
exports.FormlyValidationMessage = FormlyValidationMessage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5LnZhbGlkYXRpb24tbWVzc2FnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZvcm1seS52YWxpZGF0aW9uLW1lc3NhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBaUQ7QUFDakQsd0NBQTZDO0FBQzdDLDJEQUF5RDtBQU96RDtJQUlFLGlDQUFvQixZQUEwQjtRQUExQixpQkFBWSxHQUFaLFlBQVksQ0FBYztJQUFHLENBQUM7SUFFbEQsc0JBQUksaURBQVk7YUFBaEI7WUFDRSxHQUFHLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQ3hDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2hELElBQUksT0FBTyxHQUFzQixJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUU5RSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDckcsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDbEQsQ0FBQztvQkFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUNsRyxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDO29CQUNqRCxDQUFDO29CQUVELEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ2pILE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUM7b0JBQ3RELENBQUM7b0JBRUQsRUFBRSxDQUFDLENBQUMsT0FBTyxPQUFPLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDbEMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzNELENBQUM7b0JBRUQsTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDakIsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDOzs7T0FBQTtJQTdCUTtRQUFSLFlBQUssRUFBRTtrQ0FBWSxtQkFBVzs4REFBQztJQUN2QjtRQUFSLFlBQUssRUFBRTs7MERBQTBCO0lBRnZCLHVCQUF1QjtRQUpuQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLDJCQUEyQjtZQUNyQyxRQUFRLEVBQUUsb0JBQW9CO1NBQy9CLENBQUM7eUNBS2tDLDRCQUFZO09BSm5DLHVCQUF1QixDQStCbkM7SUFBRCw4QkFBQztDQUFBLEFBL0JELElBK0JDO0FBL0JZLDBEQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgRm9ybWx5Q29uZmlnIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybWx5LmNvbmZpZyc7XG5pbXBvcnQgeyBGb3JtbHlGaWVsZENvbmZpZyB9IGZyb20gJy4uL2NvbXBvbmVudHMvZm9ybWx5LmZpZWxkLmNvbmZpZyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Zvcm1seS12YWxpZGF0aW9uLW1lc3NhZ2UnLFxuICB0ZW1wbGF0ZTogYHt7IGVycm9yTWVzc2FnZSB9fWAsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seVZhbGlkYXRpb25NZXNzYWdlIHtcbiAgQElucHV0KCkgZmllbGRGb3JtOiBGb3JtQ29udHJvbDtcbiAgQElucHV0KCkgZmllbGQ6IEZvcm1seUZpZWxkQ29uZmlnO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9ybWx5Q29uZmlnOiBGb3JtbHlDb25maWcpIHt9XG5cbiAgZ2V0IGVycm9yTWVzc2FnZSgpOiBzdHJpbmcge1xuICAgIGZvciAobGV0IGVycm9yIGluIHRoaXMuZmllbGRGb3JtLmVycm9ycykge1xuICAgICAgaWYgKHRoaXMuZmllbGRGb3JtLmVycm9ycy5oYXNPd25Qcm9wZXJ0eShlcnJvcikpIHtcbiAgICAgICAgbGV0IG1lc3NhZ2U6IHN0cmluZyB8IEZ1bmN0aW9uID0gdGhpcy5mb3JtbHlDb25maWcuZ2V0VmFsaWRhdG9yTWVzc2FnZShlcnJvcik7XG5cbiAgICAgICAgaWYgKHRoaXMuZmllbGQudmFsaWRhdGlvbiAmJiB0aGlzLmZpZWxkLnZhbGlkYXRpb24ubWVzc2FnZXMgJiYgdGhpcy5maWVsZC52YWxpZGF0aW9uLm1lc3NhZ2VzW2Vycm9yXSkge1xuICAgICAgICAgIG1lc3NhZ2UgPSB0aGlzLmZpZWxkLnZhbGlkYXRpb24ubWVzc2FnZXNbZXJyb3JdO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZmllbGQudmFsaWRhdG9ycyAmJiB0aGlzLmZpZWxkLnZhbGlkYXRvcnNbZXJyb3JdICYmIHRoaXMuZmllbGQudmFsaWRhdG9yc1tlcnJvcl0ubWVzc2FnZSkge1xuICAgICAgICAgIG1lc3NhZ2UgPSB0aGlzLmZpZWxkLnZhbGlkYXRvcnNbZXJyb3JdLm1lc3NhZ2U7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5maWVsZC5hc3luY1ZhbGlkYXRvcnMgJiYgdGhpcy5maWVsZC5hc3luY1ZhbGlkYXRvcnNbZXJyb3JdICYmIHRoaXMuZmllbGQuYXN5bmNWYWxpZGF0b3JzW2Vycm9yXS5tZXNzYWdlKSB7XG4gICAgICAgICAgbWVzc2FnZSA9IHRoaXMuZmllbGQuYXN5bmNWYWxpZGF0b3JzW2Vycm9yXS5tZXNzYWdlO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHR5cGVvZiBtZXNzYWdlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgcmV0dXJuIG1lc3NhZ2UodGhpcy5maWVsZEZvcm0uZXJyb3JzW2Vycm9yXSwgdGhpcy5maWVsZCk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gbWVzc2FnZTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==
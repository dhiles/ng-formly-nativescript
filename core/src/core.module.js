"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var formly_form_1 = require("./components/formly.form");
var formly_field_1 = require("./components/formly.field");
var formly_attributes_1 = require("./components/formly.attributes");
var formly_config_1 = require("./services/formly.config");
var formly_form_builder_1 = require("./services/formly.form.builder");
var formly_form_expression_1 = require("./services/formly.form.expression");
var formly_group_1 = require("./components/formly.group");
var formly_validation_message_1 = require("./templates/formly.validation-message");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var nativescript_angular_1 = require("nativescript-angular");
var FormlyModule = /** @class */ (function () {
    function FormlyModule() {
    }
    FormlyModule_1 = FormlyModule;
    FormlyModule.forRoot = function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: FormlyModule_1,
            providers: [
                formly_form_builder_1.FormlyFormBuilder,
                formly_form_expression_1.FormlyFormExpression,
                formly_config_1.FormlyConfig,
                { provide: formly_config_1.FORMLY_CONFIG_TOKEN, useValue: { types: [{ name: 'formly-group', component: formly_group_1.FormlyGroup }] }, multi: true },
                { provide: formly_config_1.FORMLY_CONFIG_TOKEN, useValue: config, multi: true },
                { provide: core_1.ANALYZE_FOR_ENTRY_COMPONENTS, useValue: config, multi: true },
            ],
        };
    };
    FormlyModule.forChild = function (config) {
        if (config === void 0) { config = {}; }
        return {
            ngModule: FormlyModule_1,
            providers: [
                { provide: formly_config_1.FORMLY_CONFIG_TOKEN, useValue: config, multi: true },
                { provide: core_1.ANALYZE_FOR_ENTRY_COMPONENTS, useValue: config, multi: true },
            ],
        };
    };
    FormlyModule = FormlyModule_1 = __decorate([
        core_1.NgModule({
            declarations: [formly_form_1.FormlyForm, formly_field_1.FormlyField, formly_attributes_1.FormlyAttributes, formly_group_1.FormlyGroup, formly_validation_message_1.FormlyValidationMessage],
            entryComponents: [formly_group_1.FormlyGroup],
            exports: [formly_form_1.FormlyForm, formly_field_1.FormlyField, formly_attributes_1.FormlyAttributes, formly_group_1.FormlyGroup, formly_validation_message_1.FormlyValidationMessage],
            imports: [
                common_1.CommonModule,
                forms_1.ReactiveFormsModule,
                nativescript_module_1.NativeScriptModule,
                nativescript_angular_1.NativeScriptFormsModule
            ],
        })
    ], FormlyModule);
    return FormlyModule;
    var FormlyModule_1;
}());
exports.FormlyModule = FormlyModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUE0RjtBQUM1RiwwQ0FBK0M7QUFDL0Msd0NBQXFEO0FBQ3JELHdEQUFzRDtBQUN0RCwwREFBd0Q7QUFDeEQsb0VBQWtFO0FBQ2xFLDBEQUEyRjtBQUMzRixzRUFBbUU7QUFDbkUsNEVBQXlFO0FBQ3pFLDBEQUF3RDtBQUN4RCxtRkFBZ0Y7QUFDaEYsZ0ZBQThFO0FBQzlFLDZEQUErRDtBQWEvRDtJQUFBO0lBd0JBLENBQUM7cUJBeEJZLFlBQVk7SUFDaEIsb0JBQU8sR0FBZCxVQUFlLE1BQXlCO1FBQXpCLHVCQUFBLEVBQUEsV0FBeUI7UUFDdEMsTUFBTSxDQUFDO1lBQ0wsUUFBUSxFQUFFLGNBQVk7WUFDdEIsU0FBUyxFQUFFO2dCQUNULHVDQUFpQjtnQkFDakIsNkNBQW9CO2dCQUNwQiw0QkFBWTtnQkFDWixFQUFFLE9BQU8sRUFBRSxtQ0FBbUIsRUFBRSxRQUFRLEVBQUUsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsU0FBUyxFQUFFLDBCQUFXLEVBQUUsQ0FBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRTtnQkFDdEgsRUFBRSxPQUFPLEVBQUUsbUNBQW1CLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFO2dCQUMvRCxFQUFFLE9BQU8sRUFBRSxtQ0FBNEIsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7YUFDekU7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUVNLHFCQUFRLEdBQWYsVUFBZ0IsTUFBeUI7UUFBekIsdUJBQUEsRUFBQSxXQUF5QjtRQUN2QyxNQUFNLENBQUM7WUFDTCxRQUFRLEVBQUUsY0FBWTtZQUN0QixTQUFTLEVBQUU7Z0JBQ1QsRUFBRSxPQUFPLEVBQUUsbUNBQW1CLEVBQUUsUUFBUSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFO2dCQUMvRCxFQUFFLE9BQU8sRUFBRSxtQ0FBNEIsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUU7YUFDekU7U0FDRixDQUFDO0lBQ0osQ0FBQztJQXZCVSxZQUFZO1FBWHhCLGVBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxDQUFDLHdCQUFVLEVBQUUsMEJBQVcsRUFBRSxvQ0FBZ0IsRUFBRSwwQkFBVyxFQUFFLG1EQUF1QixDQUFDO1lBQy9GLGVBQWUsRUFBRSxDQUFDLDBCQUFXLENBQUM7WUFDOUIsT0FBTyxFQUFFLENBQUMsd0JBQVUsRUFBRSwwQkFBVyxFQUFFLG9DQUFnQixFQUFFLDBCQUFXLEVBQUUsbURBQXVCLENBQUM7WUFDMUYsT0FBTyxFQUFFO2dCQUNQLHFCQUFZO2dCQUNaLDJCQUFtQjtnQkFDbkIsd0NBQWtCO2dCQUNsQiw4Q0FBdUI7YUFDeEI7U0FDRixDQUFDO09BQ1csWUFBWSxDQXdCeEI7SUFBRCxtQkFBQzs7Q0FBQSxBQXhCRCxJQXdCQztBQXhCWSxvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBBTkFMWVpFX0ZPUl9FTlRSWV9DT01QT05FTlRTIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IEZvcm1seUZvcm0gfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybWx5LmZvcm0nO1xuaW1wb3J0IHsgRm9ybWx5RmllbGQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybWx5LmZpZWxkJztcbmltcG9ydCB7IEZvcm1seUF0dHJpYnV0ZXMgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybWx5LmF0dHJpYnV0ZXMnO1xuaW1wb3J0IHsgRm9ybWx5Q29uZmlnLCBDb25maWdPcHRpb24sIEZPUk1MWV9DT05GSUdfVE9LRU4gfSBmcm9tICcuL3NlcnZpY2VzL2Zvcm1seS5jb25maWcnO1xuaW1wb3J0IHsgRm9ybWx5Rm9ybUJ1aWxkZXIgfSBmcm9tICcuL3NlcnZpY2VzL2Zvcm1seS5mb3JtLmJ1aWxkZXInO1xuaW1wb3J0IHsgRm9ybWx5Rm9ybUV4cHJlc3Npb24gfSBmcm9tICcuL3NlcnZpY2VzL2Zvcm1seS5mb3JtLmV4cHJlc3Npb24nO1xuaW1wb3J0IHsgRm9ybWx5R3JvdXAgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybWx5Lmdyb3VwJztcbmltcG9ydCB7IEZvcm1seVZhbGlkYXRpb25NZXNzYWdlIH0gZnJvbSAnLi90ZW1wbGF0ZXMvZm9ybWx5LnZhbGlkYXRpb24tbWVzc2FnZSc7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbmF0aXZlc2NyaXB0Lm1vZHVsZVwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXJcIjtcblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbRm9ybWx5Rm9ybSwgRm9ybWx5RmllbGQsIEZvcm1seUF0dHJpYnV0ZXMsIEZvcm1seUdyb3VwLCBGb3JtbHlWYWxpZGF0aW9uTWVzc2FnZV0sXG4gIGVudHJ5Q29tcG9uZW50czogW0Zvcm1seUdyb3VwXSxcbiAgZXhwb3J0czogW0Zvcm1seUZvcm0sIEZvcm1seUZpZWxkLCBGb3JtbHlBdHRyaWJ1dGVzLCBGb3JtbHlHcm91cCwgRm9ybWx5VmFsaWRhdGlvbk1lc3NhZ2VdLFxuICBpbXBvcnRzOiBbXG4gICAgQ29tbW9uTW9kdWxlLFxuICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seU1vZHVsZSB7XG4gIHN0YXRpYyBmb3JSb290KGNvbmZpZzogQ29uZmlnT3B0aW9uID0ge30pOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcbiAgICByZXR1cm4ge1xuICAgICAgbmdNb2R1bGU6IEZvcm1seU1vZHVsZSxcbiAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICBGb3JtbHlGb3JtQnVpbGRlcixcbiAgICAgICAgRm9ybWx5Rm9ybUV4cHJlc3Npb24sXG4gICAgICAgIEZvcm1seUNvbmZpZyxcbiAgICAgICAgeyBwcm92aWRlOiBGT1JNTFlfQ09ORklHX1RPS0VOLCB1c2VWYWx1ZTogeyB0eXBlczogW3sgbmFtZTogJ2Zvcm1seS1ncm91cCcsIGNvbXBvbmVudDogRm9ybWx5R3JvdXAgfV0gfSwgbXVsdGk6IHRydWUgfSxcbiAgICAgICAgeyBwcm92aWRlOiBGT1JNTFlfQ09ORklHX1RPS0VOLCB1c2VWYWx1ZTogY29uZmlnLCBtdWx0aTogdHJ1ZSB9LFxuICAgICAgICB7IHByb3ZpZGU6IEFOQUxZWkVfRk9SX0VOVFJZX0NPTVBPTkVOVFMsIHVzZVZhbHVlOiBjb25maWcsIG11bHRpOiB0cnVlIH0sXG4gICAgICBdLFxuICAgIH07XG4gIH1cblxuICBzdGF0aWMgZm9yQ2hpbGQoY29uZmlnOiBDb25maWdPcHRpb24gPSB7fSk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xuICAgIHJldHVybiB7XG4gICAgICBuZ01vZHVsZTogRm9ybWx5TW9kdWxlLFxuICAgICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIHsgcHJvdmlkZTogRk9STUxZX0NPTkZJR19UT0tFTiwgdXNlVmFsdWU6IGNvbmZpZywgbXVsdGk6IHRydWUgfSxcbiAgICAgICAgeyBwcm92aWRlOiBBTkFMWVpFX0ZPUl9FTlRSWV9DT01QT05FTlRTLCB1c2VWYWx1ZTogY29uZmlnLCBtdWx0aTogdHJ1ZSB9LFxuICAgICAgXSxcbiAgICB9O1xuICB9XG59XG4iXX0=
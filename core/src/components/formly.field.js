"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var formly_config_1 = require("../services/formly.config");
var FormlyField = /** @class */ (function () {
    function FormlyField(formlyConfig, componentFactoryResolver) {
        this.formlyConfig = formlyConfig;
        this.componentFactoryResolver = componentFactoryResolver;
        this.options = {};
        this.modelChange = new core_1.EventEmitter();
        this.componentRefs = [];
    }
    FormlyField.prototype.ngOnInit = function () {
        if (!this.field.template) {
            this.createFieldComponent();
        }
    };
    FormlyField.prototype.ngOnChanges = function (changes) {
        var _this = this;
        this.componentRefs.forEach(function (ref) {
            Object.assign(ref.instance, {
                model: _this.model,
                form: _this.form,
                field: _this.field,
                options: _this.options,
            });
        });
    };
    FormlyField.prototype.ngOnDestroy = function () {
        this.componentRefs.forEach(function (componentRef) { return componentRef.destroy(); });
        this.componentRefs = [];
    };
    FormlyField.prototype.createFieldComponent = function () {
        var _this = this;
        var type = this.formlyConfig.getType(this.field.type), wrappers = this.getFieldWrappers(type);
        var fieldComponent = this.fieldComponent;
        wrappers.forEach(function (wrapperName) {
            var wrapperRef = _this.createComponent(fieldComponent, _this.formlyConfig.getWrapper(wrapperName).component);
            fieldComponent = wrapperRef.instance.fieldComponent;
        });
        return this.createComponent(fieldComponent, type.component);
    };
    FormlyField.prototype.getFieldWrappers = function (type) {
        var _this = this;
        var templateManipulators = {
            preWrapper: [],
            postWrapper: [],
        };
        if (this.field.templateOptions) {
            this.mergeTemplateManipulators(templateManipulators, this.field.templateOptions.templateManipulators);
        }
        this.mergeTemplateManipulators(templateManipulators, this.formlyConfig.templateManipulators);
        var preWrappers = templateManipulators.preWrapper.map(function (m) { return m(_this.field); }).filter(function (type) { return type; }), postWrappers = templateManipulators.postWrapper.map(function (m) { return m(_this.field); }).filter(function (type) { return type; });
        if (!this.field.wrappers)
            this.field.wrappers = [];
        if (!type.wrappers)
            type.wrappers = [];
        return preWrappers.concat(this.field.wrappers, postWrappers);
    };
    FormlyField.prototype.mergeTemplateManipulators = function (source, target) {
        target = target || {};
        if (target.preWrapper) {
            source.preWrapper = source.preWrapper.concat(target.preWrapper);
        }
        if (target.postWrapper) {
            source.postWrapper = source.postWrapper.concat(target.postWrapper);
        }
        return source;
    };
    FormlyField.prototype.createComponent = function (fieldComponent, component) {
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
        var ref = fieldComponent.createComponent(componentFactory);
        Object.assign(ref.instance, {
            model: this.model,
            form: this.form,
            field: this.field,
            options: this.options,
        });
        this.componentRefs.push(ref);
        return ref;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "model", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.FormGroup)
    ], FormlyField.prototype, "form", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "field", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyField.prototype, "options", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], FormlyField.prototype, "modelChange", void 0);
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyField.prototype, "fieldComponent", void 0);
    FormlyField = __decorate([
        core_1.Component({
            selector: 'formly-field',
            template: "\n    <ng-template #fieldComponent></ng-template>\n    <StackLayout *ngIf=\"field.template && !field.fieldGroup\" [innerHtml]=\"field.template\"></StackLayout>\n  ",
            host: {
                '[style.display]': 'field.hide ? "none":""',
            },
        }),
        __metadata("design:paramtypes", [formly_config_1.FormlyConfig,
            core_1.ComponentFactoryResolver])
    ], FormlyField);
    return FormlyField;
}());
exports.FormlyField = FormlyField;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5LmZpZWxkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZm9ybWx5LmZpZWxkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBR3VCO0FBQ3ZCLHdDQUEyQztBQUMzQywyREFBMkY7QUFjM0Y7SUFVRSxxQkFDVSxZQUEwQixFQUMxQix3QkFBa0Q7UUFEbEQsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUEwQjtRQVJuRCxZQUFPLEdBQXNCLEVBQUUsQ0FBQztRQUMvQixnQkFBVyxHQUFzQixJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQUd0RCxrQkFBYSxHQUEwQixFQUFFLENBQUM7SUFLL0MsQ0FBQztJQUVKLDhCQUFRLEdBQVI7UUFDRSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUM5QixDQUFDO0lBQ0gsQ0FBQztJQUVELGlDQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkFTQztRQVJDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztZQUM1QixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLEtBQUssRUFBRSxLQUFJLENBQUMsS0FBSztnQkFDakIsSUFBSSxFQUFFLEtBQUksQ0FBQyxJQUFJO2dCQUNmLEtBQUssRUFBRSxLQUFJLENBQUMsS0FBSztnQkFDakIsT0FBTyxFQUFFLEtBQUksQ0FBQyxPQUFPO2FBQ3RCLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVksSUFBSSxPQUFBLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBdEIsQ0FBc0IsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFTywwQ0FBb0IsR0FBNUI7UUFBQSxpQkFXQztRQVZDLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQ3JELFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFekMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUN6QyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsV0FBVztZQUMxQixJQUFNLFVBQVUsR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLGNBQWMsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM3RyxjQUFjLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7UUFFSCxNQUFNLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFTyxzQ0FBZ0IsR0FBeEIsVUFBeUIsSUFBZ0I7UUFBekMsaUJBbUJDO1FBbEJDLElBQU0sb0JBQW9CLEdBQXlCO1lBQ2pELFVBQVUsRUFBRSxFQUFFO1lBQ2QsV0FBVyxFQUFFLEVBQUU7U0FDaEIsQ0FBQztRQUVGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMseUJBQXlCLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN4RyxDQUFDO1FBRUQsSUFBSSxDQUFDLHlCQUF5QixDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUU3RixJQUFJLFdBQVcsR0FBRyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBYixDQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLEVBQUosQ0FBSSxDQUFDLEVBQzVGLFlBQVksR0FBRyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsRUFBYixDQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLEVBQUosQ0FBSSxDQUFDLENBQUM7UUFFL0YsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7WUFBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUV2QyxNQUFNLENBQUssV0FBVyxRQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFLLFlBQVksRUFBRTtJQUNuRSxDQUFDO0lBRU8sK0NBQXlCLEdBQWpDLFVBQWtDLE1BQTRCLEVBQUUsTUFBNEI7UUFDMUYsTUFBTSxHQUFHLE1BQU0sSUFBSSxFQUFFLENBQUM7UUFDdEIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDdEIsTUFBTSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDbEUsQ0FBQztRQUNELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3JFLENBQUM7UUFFRCxNQUFNLENBQUMsTUFBTSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxxQ0FBZSxHQUF2QixVQUF3QixjQUFnQyxFQUFFLFNBQWM7UUFDdEUsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEYsSUFBSSxHQUFHLEdBQXdCLGNBQWMsQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUVoRixNQUFNLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUU7WUFDeEIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87U0FDeEIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFN0IsTUFBTSxDQUFDLEdBQUcsQ0FBQztJQUNiLENBQUM7SUFoR1E7UUFBUixZQUFLLEVBQUU7OzhDQUFZO0lBQ1g7UUFBUixZQUFLLEVBQUU7a0NBQU8saUJBQVM7NkNBQUM7SUFDaEI7UUFBUixZQUFLLEVBQUU7OzhDQUEwQjtJQUN6QjtRQUFSLFlBQUssRUFBRTs7Z0RBQWlDO0lBQy9CO1FBQVQsYUFBTSxFQUFFO2tDQUFjLG1CQUFZO29EQUEyQjtJQUNQO1FBQXRELGdCQUFTLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxJQUFJLEVBQUUsdUJBQWdCLEVBQUMsQ0FBQztrQ0FBaUIsdUJBQWdCO3VEQUFDO0lBTjdFLFdBQVc7UUFWdkIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLFFBQVEsRUFBRSxxS0FHVDtZQUNELElBQUksRUFBRTtnQkFDSixpQkFBaUIsRUFBRSx3QkFBd0I7YUFDNUM7U0FDRixDQUFDO3lDQVl3Qiw0QkFBWTtZQUNBLCtCQUF3QjtPQVpqRCxXQUFXLENBa0d2QjtJQUFELGtCQUFDO0NBQUEsQUFsR0QsSUFrR0M7QUFsR1ksa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBDb21wb25lbnQsIE9uSW5pdCwgT25DaGFuZ2VzLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIE9uRGVzdHJveSxcbiAgVmlld0NvbnRhaW5lclJlZiwgVmlld0NoaWxkLCBDb21wb25lbnRSZWYsIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgU2ltcGxlQ2hhbmdlcyxcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3JtbHlDb25maWcsIFR5cGVPcHRpb24sIFRlbXBsYXRlTWFuaXB1bGF0b3JzIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybWx5LmNvbmZpZyc7XG5pbXBvcnQgeyBGaWVsZCB9IGZyb20gJy4uL3RlbXBsYXRlcy9maWVsZCc7XG5pbXBvcnQgeyBGb3JtbHlGaWVsZENvbmZpZywgRm9ybWx5Rm9ybU9wdGlvbnMgfSBmcm9tICcuL2Zvcm1seS5maWVsZC5jb25maWcnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktZmllbGQnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxuZy10ZW1wbGF0ZSAjZmllbGRDb21wb25lbnQ+PC9uZy10ZW1wbGF0ZT5cbiAgICA8U3RhY2tMYXlvdXQgKm5nSWY9XCJmaWVsZC50ZW1wbGF0ZSAmJiAhZmllbGQuZmllbGRHcm91cFwiIFtpbm5lckh0bWxdPVwiZmllbGQudGVtcGxhdGVcIj48L1N0YWNrTGF5b3V0PlxuICBgLFxuICBob3N0OiB7XG4gICAgJ1tzdHlsZS5kaXNwbGF5XSc6ICdmaWVsZC5oaWRlID8gXCJub25lXCI6XCJcIicsXG4gIH0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUZpZWxkIGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSB7XG4gIEBJbnB1dCgpIG1vZGVsOiBhbnk7XG4gIEBJbnB1dCgpIGZvcm06IEZvcm1Hcm91cDtcbiAgQElucHV0KCkgZmllbGQ6IEZvcm1seUZpZWxkQ29uZmlnO1xuICBASW5wdXQoKSBvcHRpb25zOiBGb3JtbHlGb3JtT3B0aW9ucyA9IHt9O1xuICBAT3V0cHV0KCkgbW9kZWxDaGFuZ2U6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICBAVmlld0NoaWxkKCdmaWVsZENvbXBvbmVudCcsIHtyZWFkOiBWaWV3Q29udGFpbmVyUmVmfSkgZmllbGRDb21wb25lbnQ6IFZpZXdDb250YWluZXJSZWY7XG5cbiAgcHJpdmF0ZSBjb21wb25lbnRSZWZzOiBDb21wb25lbnRSZWY8RmllbGQ+W10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZvcm1seUNvbmZpZzogRm9ybWx5Q29uZmlnLFxuICAgIHByaXZhdGUgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICkge31cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZiAoIXRoaXMuZmllbGQudGVtcGxhdGUpIHtcbiAgICAgIHRoaXMuY3JlYXRlRmllbGRDb21wb25lbnQoKTtcbiAgICB9XG4gIH1cblxuICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgdGhpcy5jb21wb25lbnRSZWZzLmZvckVhY2gocmVmID0+IHtcbiAgICAgIE9iamVjdC5hc3NpZ24ocmVmLmluc3RhbmNlLCB7XG4gICAgICAgIG1vZGVsOiB0aGlzLm1vZGVsLFxuICAgICAgICBmb3JtOiB0aGlzLmZvcm0sXG4gICAgICAgIGZpZWxkOiB0aGlzLmZpZWxkLFxuICAgICAgICBvcHRpb25zOiB0aGlzLm9wdGlvbnMsXG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIHRoaXMuY29tcG9uZW50UmVmcy5mb3JFYWNoKGNvbXBvbmVudFJlZiA9PiBjb21wb25lbnRSZWYuZGVzdHJveSgpKTtcbiAgICB0aGlzLmNvbXBvbmVudFJlZnMgPSBbXTtcbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlRmllbGRDb21wb25lbnQoKTogQ29tcG9uZW50UmVmPEZpZWxkPiB7XG4gICAgY29uc3QgdHlwZSA9IHRoaXMuZm9ybWx5Q29uZmlnLmdldFR5cGUodGhpcy5maWVsZC50eXBlKSxcbiAgICAgIHdyYXBwZXJzID0gdGhpcy5nZXRGaWVsZFdyYXBwZXJzKHR5cGUpO1xuXG4gICAgbGV0IGZpZWxkQ29tcG9uZW50ID0gdGhpcy5maWVsZENvbXBvbmVudDtcbiAgICB3cmFwcGVycy5mb3JFYWNoKHdyYXBwZXJOYW1lID0+IHtcbiAgICAgIGNvbnN0IHdyYXBwZXJSZWYgPSB0aGlzLmNyZWF0ZUNvbXBvbmVudChmaWVsZENvbXBvbmVudCwgdGhpcy5mb3JtbHlDb25maWcuZ2V0V3JhcHBlcih3cmFwcGVyTmFtZSkuY29tcG9uZW50KTtcbiAgICAgIGZpZWxkQ29tcG9uZW50ID0gd3JhcHBlclJlZi5pbnN0YW5jZS5maWVsZENvbXBvbmVudDtcbiAgICB9KTtcblxuICAgIHJldHVybiB0aGlzLmNyZWF0ZUNvbXBvbmVudChmaWVsZENvbXBvbmVudCwgdHlwZS5jb21wb25lbnQpO1xuICB9XG5cbiAgcHJpdmF0ZSBnZXRGaWVsZFdyYXBwZXJzKHR5cGU6IFR5cGVPcHRpb24pIHtcbiAgICBjb25zdCB0ZW1wbGF0ZU1hbmlwdWxhdG9yczogVGVtcGxhdGVNYW5pcHVsYXRvcnMgPSB7XG4gICAgICBwcmVXcmFwcGVyOiBbXSxcbiAgICAgIHBvc3RXcmFwcGVyOiBbXSxcbiAgICB9O1xuXG4gICAgaWYgKHRoaXMuZmllbGQudGVtcGxhdGVPcHRpb25zKSB7XG4gICAgICB0aGlzLm1lcmdlVGVtcGxhdGVNYW5pcHVsYXRvcnModGVtcGxhdGVNYW5pcHVsYXRvcnMsIHRoaXMuZmllbGQudGVtcGxhdGVPcHRpb25zLnRlbXBsYXRlTWFuaXB1bGF0b3JzKTtcbiAgICB9XG5cbiAgICB0aGlzLm1lcmdlVGVtcGxhdGVNYW5pcHVsYXRvcnModGVtcGxhdGVNYW5pcHVsYXRvcnMsIHRoaXMuZm9ybWx5Q29uZmlnLnRlbXBsYXRlTWFuaXB1bGF0b3JzKTtcblxuICAgIGxldCBwcmVXcmFwcGVycyA9IHRlbXBsYXRlTWFuaXB1bGF0b3JzLnByZVdyYXBwZXIubWFwKG0gPT4gbSh0aGlzLmZpZWxkKSkuZmlsdGVyKHR5cGUgPT4gdHlwZSksXG4gICAgICBwb3N0V3JhcHBlcnMgPSB0ZW1wbGF0ZU1hbmlwdWxhdG9ycy5wb3N0V3JhcHBlci5tYXAobSA9PiBtKHRoaXMuZmllbGQpKS5maWx0ZXIodHlwZSA9PiB0eXBlKTtcblxuICAgIGlmICghdGhpcy5maWVsZC53cmFwcGVycykgdGhpcy5maWVsZC53cmFwcGVycyA9IFtdO1xuICAgIGlmICghdHlwZS53cmFwcGVycykgdHlwZS53cmFwcGVycyA9IFtdO1xuXG4gICAgcmV0dXJuIFsuLi5wcmVXcmFwcGVycywgLi4udGhpcy5maWVsZC53cmFwcGVycywgLi4ucG9zdFdyYXBwZXJzXTtcbiAgfVxuXG4gIHByaXZhdGUgbWVyZ2VUZW1wbGF0ZU1hbmlwdWxhdG9ycyhzb3VyY2U6IFRlbXBsYXRlTWFuaXB1bGF0b3JzLCB0YXJnZXQ6IFRlbXBsYXRlTWFuaXB1bGF0b3JzKSB7XG4gICAgdGFyZ2V0ID0gdGFyZ2V0IHx8IHt9O1xuICAgIGlmICh0YXJnZXQucHJlV3JhcHBlcikge1xuICAgICAgc291cmNlLnByZVdyYXBwZXIgPSBzb3VyY2UucHJlV3JhcHBlci5jb25jYXQodGFyZ2V0LnByZVdyYXBwZXIpO1xuICAgIH1cbiAgICBpZiAodGFyZ2V0LnBvc3RXcmFwcGVyKSB7XG4gICAgICBzb3VyY2UucG9zdFdyYXBwZXIgPSBzb3VyY2UucG9zdFdyYXBwZXIuY29uY2F0KHRhcmdldC5wb3N0V3JhcHBlcik7XG4gICAgfVxuXG4gICAgcmV0dXJuIHNvdXJjZTtcbiAgfVxuXG4gIHByaXZhdGUgY3JlYXRlQ29tcG9uZW50KGZpZWxkQ29tcG9uZW50OiBWaWV3Q29udGFpbmVyUmVmLCBjb21wb25lbnQ6IGFueSk6IENvbXBvbmVudFJlZjxhbnk+IHtcbiAgICBsZXQgY29tcG9uZW50RmFjdG9yeSA9IHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbXBvbmVudCk7XG4gICAgbGV0IHJlZiA9IDxDb21wb25lbnRSZWY8RmllbGQ+PmZpZWxkQ29tcG9uZW50LmNyZWF0ZUNvbXBvbmVudChjb21wb25lbnRGYWN0b3J5KTtcblxuICAgIE9iamVjdC5hc3NpZ24ocmVmLmluc3RhbmNlLCB7XG4gICAgICAgIG1vZGVsOiB0aGlzLm1vZGVsLFxuICAgICAgICBmb3JtOiB0aGlzLmZvcm0sXG4gICAgICAgIGZpZWxkOiB0aGlzLmZpZWxkLFxuICAgICAgICBvcHRpb25zOiB0aGlzLm9wdGlvbnMsXG4gICAgfSk7XG5cbiAgICB0aGlzLmNvbXBvbmVudFJlZnMucHVzaChyZWYpO1xuXG4gICAgcmV0dXJuIHJlZjtcbiAgfVxufVxuIl19
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var FormlyAttributes = /** @class */ (function () {
    function FormlyAttributes(renderer, elementRef) {
        this.renderer = renderer;
        this.elementRef = elementRef;
        this.attributes = ['id', 'name', 'placeholder', 'tabindex', 'step', 'readonly'];
        this.statements = ['change', 'keydown', 'keyup', 'keypress', 'click', 'focus', 'blur'];
    }
    FormlyAttributes.prototype.onFocus = function () {
        this.field.focus = true;
    };
    FormlyAttributes.prototype.onBlur = function () {
        this.field.focus = false;
    };
    FormlyAttributes.prototype.ngOnChanges = function (changes) {
        var _this = this;
        if (changes.field) {
            var fieldChanges_1 = changes.field;
            this.attributes
                .filter(function (attr) { return _this.canApplyRender(fieldChanges_1, attr); })
                .forEach(function (attr) { return _this.renderer.setAttribute(_this.elementRef.nativeElement, attr, _this.getPropValue(_this.field, attr)); });
            if (this.field.templateOptions && this.field.templateOptions.attributes) {
                var attributes_1 = this.field.templateOptions.attributes;
                Object.keys(attributes_1).forEach(function (name) { return _this.renderer.setAttribute(_this.elementRef.nativeElement, name, attributes_1[name]); });
            }
            this.statements
                .filter(function (statement) { return _this.canApplyRender(fieldChanges_1, statement); })
                .forEach(function (statement) { return _this.renderer.listen(_this.elementRef.nativeElement, statement, _this.getStatementValue(statement)); });
            if ((fieldChanges_1.previousValue || {}).focus !== (fieldChanges_1.currentValue || {}).focus && this.elementRef.nativeElement.focus) {
                this.elementRef.nativeElement[this.field.focus ? 'focus' : 'blur']();
            }
        }
    };
    FormlyAttributes.prototype.getPropValue = function (field, prop) {
        field = field || {};
        if (field.templateOptions && field.templateOptions[prop]) {
            return field.templateOptions[prop];
        }
        return field[prop] || '';
    };
    FormlyAttributes.prototype.getStatementValue = function (statement) {
        var _this = this;
        var fn = this.field.templateOptions[statement];
        return function () { return fn(_this.field, _this.formControl); };
    };
    FormlyAttributes.prototype.canApplyRender = function (fieldChange, prop) {
        var currentValue = this.getPropValue(this.field, prop), previousValue = this.getPropValue(fieldChange.previousValue, prop);
        if (previousValue !== currentValue) {
            if (this.statements.indexOf(prop) !== -1) {
                return typeof currentValue === 'function';
            }
            return true;
        }
        return false;
    };
    __decorate([
        core_1.Input('formlyAttributes'),
        __metadata("design:type", Object)
    ], FormlyAttributes.prototype, "field", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", forms_1.AbstractControl)
    ], FormlyAttributes.prototype, "formControl", void 0);
    __decorate([
        core_1.HostListener('focus'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FormlyAttributes.prototype, "onFocus", null);
    __decorate([
        core_1.HostListener('blur'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], FormlyAttributes.prototype, "onBlur", null);
    FormlyAttributes = __decorate([
        core_1.Directive({
            selector: '[formlyAttributes]',
        }),
        __metadata("design:paramtypes", [core_1.Renderer2,
            core_1.ElementRef])
    ], FormlyAttributes);
    return FormlyAttributes;
}());
exports.FormlyAttributes = FormlyAttributes;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5LmF0dHJpYnV0ZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmb3JtbHkuYXR0cmlidXRlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUE4SDtBQUM5SCx3Q0FBaUQ7QUFNakQ7SUFjRSwwQkFDVSxRQUFtQixFQUNuQixVQUFzQjtRQUR0QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLGVBQVUsR0FBVixVQUFVLENBQVk7UUFieEIsZUFBVSxHQUFHLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMzRSxlQUFVLEdBQUcsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztJQWF2RixDQUFDO0lBWG1CLGtDQUFPLEdBQVA7UUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFFcUIsaUNBQU0sR0FBTjtRQUNwQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDM0IsQ0FBQztJQU9ELHNDQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkEwQkM7UUF6QkMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDbEIsSUFBTSxjQUFZLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUNuQyxJQUFJLENBQUMsVUFBVTtpQkFDWixNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLGNBQVksRUFBRSxJQUFJLENBQUMsRUFBdkMsQ0FBdUMsQ0FBQztpQkFDdkQsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQ3pDLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQ3pFLEVBRmdCLENBRWhCLENBQUMsQ0FBQztZQUVMLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLElBQU0sWUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQztnQkFDekQsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FDaEUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLFlBQVUsQ0FBQyxJQUFJLENBQVcsQ0FDaEUsRUFGdUMsQ0FFdkMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQztZQUVELElBQUksQ0FBQyxVQUFVO2lCQUNaLE1BQU0sQ0FBQyxVQUFBLFNBQVMsSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBWSxFQUFFLFNBQVMsQ0FBQyxFQUE1QyxDQUE0QyxDQUFDO2lCQUNqRSxPQUFPLENBQUMsVUFBQSxTQUFTLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FDeEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FDNUUsRUFGcUIsQ0FFckIsQ0FBQyxDQUFDO1lBRUwsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFZLENBQUMsYUFBYSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEtBQUssS0FBSyxDQUFDLGNBQVksQ0FBQyxZQUFZLElBQUksRUFBRSxDQUFDLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2hJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDdkUsQ0FBQztRQUNILENBQUM7SUFDSCxDQUFDO0lBRU8sdUNBQVksR0FBcEIsVUFBcUIsS0FBd0IsRUFBRSxJQUFZO1FBQ3pELEtBQUssR0FBRyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3BCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLElBQUksS0FBSyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekQsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsQ0FBQztRQUVELE1BQU0sQ0FBTyxLQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2xDLENBQUM7SUFFTyw0Q0FBaUIsR0FBekIsVUFBMEIsU0FBaUI7UUFBM0MsaUJBSUM7UUFIQyxJQUFNLEVBQUUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUVqRCxNQUFNLENBQUMsY0FBTSxPQUFBLEVBQUUsQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBaEMsQ0FBZ0MsQ0FBQztJQUNoRCxDQUFDO0lBRU8seUNBQWMsR0FBdEIsVUFBdUIsV0FBeUIsRUFBRSxJQUFZO1FBQzVELElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsRUFDdEQsYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVyRSxFQUFFLENBQUMsQ0FBQyxhQUFhLEtBQUssWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNuQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3pDLE1BQU0sQ0FBQyxPQUFPLFlBQVksS0FBSyxVQUFVLENBQUM7WUFDNUMsQ0FBQztZQUVELE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDZCxDQUFDO1FBRUQsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNmLENBQUM7SUExRTBCO1FBQTFCLFlBQUssQ0FBQyxrQkFBa0IsQ0FBQzs7bURBQTBCO0lBQzNDO1FBQVIsWUFBSyxFQUFFO2tDQUFjLHVCQUFlO3lEQUFDO0lBSWY7UUFBdEIsbUJBQVksQ0FBQyxPQUFPLENBQUM7Ozs7bURBRXJCO0lBRXFCO1FBQXJCLG1CQUFZLENBQUMsTUFBTSxDQUFDOzs7O2tEQUVwQjtJQVpVLGdCQUFnQjtRQUg1QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtTQUMvQixDQUFDO3lDQWdCb0IsZ0JBQVM7WUFDUCxpQkFBVTtPQWhCckIsZ0JBQWdCLENBNEU1QjtJQUFELHVCQUFDO0NBQUEsQUE1RUQsSUE0RUM7QUE1RVksNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIEVsZW1lbnRSZWYsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIFNpbXBsZUNoYW5nZSwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3JtbHlGaWVsZENvbmZpZyB9IGZyb20gJy4vZm9ybWx5LmZpZWxkLmNvbmZpZyc7XG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tmb3JtbHlBdHRyaWJ1dGVzXScsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUF0dHJpYnV0ZXMgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICBASW5wdXQoJ2Zvcm1seUF0dHJpYnV0ZXMnKSBmaWVsZDogRm9ybWx5RmllbGRDb25maWc7XG4gIEBJbnB1dCgpIGZvcm1Db250cm9sOiBBYnN0cmFjdENvbnRyb2w7XG4gIHByaXZhdGUgYXR0cmlidXRlcyA9IFsnaWQnLCAnbmFtZScsICdwbGFjZWhvbGRlcicsICd0YWJpbmRleCcsICdzdGVwJywgJ3JlYWRvbmx5J107XG4gIHByaXZhdGUgc3RhdGVtZW50cyA9IFsnY2hhbmdlJywgJ2tleWRvd24nLCAna2V5dXAnLCAna2V5cHJlc3MnLCAnY2xpY2snLCAnZm9jdXMnLCAnYmx1ciddO1xuXG4gIEBIb3N0TGlzdGVuZXIoJ2ZvY3VzJykgb25Gb2N1cygpIHtcbiAgICB0aGlzLmZpZWxkLmZvY3VzID0gdHJ1ZTtcbiAgfVxuXG4gIEBIb3N0TGlzdGVuZXIoJ2JsdXInKSBvbkJsdXIoKSB7XG4gICAgdGhpcy5maWVsZC5mb2N1cyA9IGZhbHNlO1xuICB9XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxuICAgIHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZixcbiAgKSB7fVxuXG4gIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICBpZiAoY2hhbmdlcy5maWVsZCkge1xuICAgICAgY29uc3QgZmllbGRDaGFuZ2VzID0gY2hhbmdlcy5maWVsZDtcbiAgICAgIHRoaXMuYXR0cmlidXRlc1xuICAgICAgICAuZmlsdGVyKGF0dHIgPT4gdGhpcy5jYW5BcHBseVJlbmRlcihmaWVsZENoYW5nZXMsIGF0dHIpKVxuICAgICAgICAuZm9yRWFjaChhdHRyID0+IHRoaXMucmVuZGVyZXIuc2V0QXR0cmlidXRlKFxuICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBhdHRyLCB0aGlzLmdldFByb3BWYWx1ZSh0aGlzLmZpZWxkLCBhdHRyKSxcbiAgICAgICAgKSk7XG5cbiAgICAgIGlmICh0aGlzLmZpZWxkLnRlbXBsYXRlT3B0aW9ucyAmJiB0aGlzLmZpZWxkLnRlbXBsYXRlT3B0aW9ucy5hdHRyaWJ1dGVzKSB7XG4gICAgICAgIGNvbnN0IGF0dHJpYnV0ZXMgPSB0aGlzLmZpZWxkLnRlbXBsYXRlT3B0aW9ucy5hdHRyaWJ1dGVzO1xuICAgICAgICBPYmplY3Qua2V5cyhhdHRyaWJ1dGVzKS5mb3JFYWNoKG5hbWUgPT4gdGhpcy5yZW5kZXJlci5zZXRBdHRyaWJ1dGUoXG4gICAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIG5hbWUsIGF0dHJpYnV0ZXNbbmFtZV0gYXMgc3RyaW5nLFxuICAgICAgICApKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zdGF0ZW1lbnRzXG4gICAgICAgIC5maWx0ZXIoc3RhdGVtZW50ID0+IHRoaXMuY2FuQXBwbHlSZW5kZXIoZmllbGRDaGFuZ2VzLCBzdGF0ZW1lbnQpKVxuICAgICAgICAuZm9yRWFjaChzdGF0ZW1lbnQgPT4gdGhpcy5yZW5kZXJlci5saXN0ZW4oXG4gICAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIHN0YXRlbWVudCwgdGhpcy5nZXRTdGF0ZW1lbnRWYWx1ZShzdGF0ZW1lbnQpLFxuICAgICAgICApKTtcblxuICAgICAgaWYgKChmaWVsZENoYW5nZXMucHJldmlvdXNWYWx1ZSB8fCB7fSkuZm9jdXMgIT09IChmaWVsZENoYW5nZXMuY3VycmVudFZhbHVlIHx8IHt9KS5mb2N1cyAmJiB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5mb2N1cykge1xuICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudFt0aGlzLmZpZWxkLmZvY3VzID8gJ2ZvY3VzJyA6ICdibHVyJ10oKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBwcml2YXRlIGdldFByb3BWYWx1ZShmaWVsZDogRm9ybWx5RmllbGRDb25maWcsIHByb3A6IHN0cmluZykge1xuICAgIGZpZWxkID0gZmllbGQgfHwge307XG4gICAgaWYgKGZpZWxkLnRlbXBsYXRlT3B0aW9ucyAmJiBmaWVsZC50ZW1wbGF0ZU9wdGlvbnNbcHJvcF0pIHtcbiAgICAgIHJldHVybiBmaWVsZC50ZW1wbGF0ZU9wdGlvbnNbcHJvcF07XG4gICAgfVxuXG4gICAgcmV0dXJuICg8YW55PmZpZWxkKVtwcm9wXSB8fCAnJztcbiAgfVxuXG4gIHByaXZhdGUgZ2V0U3RhdGVtZW50VmFsdWUoc3RhdGVtZW50OiBzdHJpbmcpIHtcbiAgICBjb25zdCBmbiA9IHRoaXMuZmllbGQudGVtcGxhdGVPcHRpb25zW3N0YXRlbWVudF07XG5cbiAgICByZXR1cm4gKCkgPT4gZm4odGhpcy5maWVsZCwgdGhpcy5mb3JtQ29udHJvbCk7XG4gIH1cblxuICBwcml2YXRlIGNhbkFwcGx5UmVuZGVyKGZpZWxkQ2hhbmdlOiBTaW1wbGVDaGFuZ2UsIHByb3A6IHN0cmluZyk6IEJvb2xlYW4ge1xuICAgIGNvbnN0IGN1cnJlbnRWYWx1ZSA9IHRoaXMuZ2V0UHJvcFZhbHVlKHRoaXMuZmllbGQsIHByb3ApLFxuICAgICAgcHJldmlvdXNWYWx1ZSA9IHRoaXMuZ2V0UHJvcFZhbHVlKGZpZWxkQ2hhbmdlLnByZXZpb3VzVmFsdWUsIHByb3ApO1xuXG4gICAgaWYgKHByZXZpb3VzVmFsdWUgIT09IGN1cnJlbnRWYWx1ZSkge1xuICAgICAgaWYgKHRoaXMuc3RhdGVtZW50cy5pbmRleE9mKHByb3ApICE9PSAtMSkge1xuICAgICAgICByZXR1cm4gdHlwZW9mIGN1cnJlbnRWYWx1ZSA9PT0gJ2Z1bmN0aW9uJztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iXX0=
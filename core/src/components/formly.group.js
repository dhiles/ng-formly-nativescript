"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var field_type_1 = require("../templates/field.type");
var FormlyGroup = /** @class */ (function (_super) {
    __extends(FormlyGroup, _super);
    function FormlyGroup() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyGroup = __decorate([
        core_1.Component({
            selector: 'formly-group',
            template: "\n    <formly-form\n      [fields]=\"field.fieldGroup\"\n      [isRoot]=\"false\"\n      [model]=\"model\"\n      [form]=\"field.formControl || form\"\n      [options]=\"options\"\n      [ngClass]=\"field.fieldGroupClassName\">\n      <ng-content></ng-content>\n    </formly-form>\n  ",
        })
    ], FormlyGroup);
    return FormlyGroup;
}(field_type_1.FieldType));
exports.FormlyGroup = FormlyGroup;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5Lmdyb3VwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZm9ybWx5Lmdyb3VwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLHNEQUFvRDtBQWdCcEQ7SUFBaUMsK0JBQVM7SUFBMUM7O0lBQTRDLENBQUM7SUFBaEMsV0FBVztRQWR2QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLGNBQWM7WUFDeEIsUUFBUSxFQUFFLDhSQVVUO1NBQ0YsQ0FBQztPQUNXLFdBQVcsQ0FBcUI7SUFBRCxrQkFBQztDQUFBLEFBQTdDLENBQWlDLHNCQUFTLEdBQUc7QUFBaEMsa0NBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZpZWxkVHlwZSB9IGZyb20gJy4uL3RlbXBsYXRlcy9maWVsZC50eXBlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LWdyb3VwJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8Zm9ybWx5LWZvcm1cbiAgICAgIFtmaWVsZHNdPVwiZmllbGQuZmllbGRHcm91cFwiXG4gICAgICBbaXNSb290XT1cImZhbHNlXCJcbiAgICAgIFttb2RlbF09XCJtb2RlbFwiXG4gICAgICBbZm9ybV09XCJmaWVsZC5mb3JtQ29udHJvbCB8fCBmb3JtXCJcbiAgICAgIFtvcHRpb25zXT1cIm9wdGlvbnNcIlxuICAgICAgW25nQ2xhc3NdPVwiZmllbGQuZmllbGRHcm91cENsYXNzTmFtZVwiPlxuICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuICAgIDwvZm9ybWx5LWZvcm0+XG4gIGAsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUdyb3VwIGV4dGVuZHMgRmllbGRUeXBlIHt9XG4iXX0=
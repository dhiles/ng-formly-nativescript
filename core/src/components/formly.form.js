"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var formly_form_builder_1 = require("../services/formly.form.builder");
var formly_form_expression_1 = require("../services/formly.form.expression");
var formly_config_1 = require("../services/formly.config");
var utils_1 = require("../utils");
var Subject_1 = require("rxjs/Subject");
var debounceTime_1 = require("rxjs/operator/debounceTime");
var map_1 = require("rxjs/operator/map");
var FormlyForm = /** @class */ (function () {
    function FormlyForm(formlyBuilder, formlyExpression, formlyConfig, parentForm, parentFormGroup, parentFormlyForm) {
        this.formlyBuilder = formlyBuilder;
        this.formlyExpression = formlyExpression;
        this.formlyConfig = formlyConfig;
        this.parentForm = parentForm;
        this.parentFormGroup = parentFormGroup;
        this.parentFormlyForm = parentFormlyForm;
        this.model = {};
        this.form = new forms_1.FormGroup({});
        this.fields = [];
        this.modelChange = new core_1.EventEmitter();
        /** @internal */
        this.isRoot = true;
        this.modelChangeSubs = [];
    }
    FormlyForm.prototype.ngDoCheck = function () {
        this.checkExpressionChange();
    };
    FormlyForm.prototype.ngOnChanges = function (changes) {
        if (!this.fields || this.fields.length === 0 || !this.isRoot) {
            return;
        }
        if (changes.fields || changes.form) {
            this.model = this.model || {};
            this.form = this.form || (new forms_1.FormGroup({}));
            this.setOptions();
            this.clearModelSubscriptions();
            this.formlyBuilder.buildForm(this.form, this.fields, this.model, this.options);
            this.trackModelChanges(this.fields);
            this.updateInitialValue();
        }
        else if (changes.model) {
            this.patchModel(this.model);
        }
    };
    FormlyForm.prototype.ngOnDestroy = function () {
        this.clearModelSubscriptions();
    };
    FormlyForm.prototype.fieldModel = function (field, model) {
        if (model === void 0) { model = this.model; }
        if (field.key && (field.fieldGroup || field.fieldArray)) {
            return utils_1.getFieldModel(model, field, true);
        }
        return model;
    };
    FormlyForm.prototype.changeModel = function (event) {
        utils_1.assignModelValue(this.model, event.key, event.value);
        this.modelChange.emit(this.model);
        this.checkExpressionChange();
    };
    FormlyForm.prototype.setOptions = function () {
        var _this = this;
        this.options = this.options || {};
        this.options.formState = this.options.formState || {};
        if (!this.options.showError) {
            this.options.showError = this.formlyConfig.extras.showError;
        }
        if (!this.options.fieldChanges) {
            this.options.fieldChanges = new Subject_1.Subject();
        }
        if (!this.options.resetModel) {
            this.options.resetModel = this.resetModel.bind(this);
        }
        if (!this.options.parentForm) {
            this.options.parentForm = this.parentFormGroup || this.parentForm;
        }
        if (!this.options.updateInitialValue) {
            this.options.updateInitialValue = this.updateInitialValue.bind(this);
        }
        if (!this.options.resetTrackModelChanges) {
            this.options.resetTrackModelChanges = function () {
                _this.clearModelSubscriptions();
                _this.trackModelChanges(_this.fields);
            };
        }
    };
    FormlyForm.prototype.checkExpressionChange = function () {
        if (this.isRoot) {
            this.formlyExpression.checkFields(this.form, this.fields, this.model, this.options);
        }
    };
    FormlyForm.prototype.trackModelChanges = function (fields, rootKey) {
        var _this = this;
        if (rootKey === void 0) { rootKey = []; }
        fields.forEach(function (field) {
            if (field.key && field.type && !field.fieldGroup && !field.fieldArray) {
                var valueChanges_1 = field.formControl.valueChanges;
                var debounce = field.modelOptions && field.modelOptions.debounce && field.modelOptions.debounce.default;
                if (debounce > 0) {
                    valueChanges_1 = debounceTime_1.debounceTime.call(valueChanges_1, debounce);
                }
                if (field.parsers && field.parsers.length > 0) {
                    field.parsers.forEach(function (parserFn) {
                        valueChanges_1 = map_1.map.call(valueChanges_1, parserFn);
                    });
                }
                _this.modelChangeSubs.push(valueChanges_1
                    .subscribe(function (event) { return _this.changeModel({ key: rootKey.concat([field.key]).join('.'), value: event }); }));
            }
            if (field.fieldGroup && field.fieldGroup.length > 0) {
                _this.trackModelChanges(field.fieldGroup, field.key ? rootKey.concat([field.key]) : rootKey);
            }
        });
    };
    FormlyForm.prototype.clearModelSubscriptions = function () {
        this.modelChangeSubs.forEach(function (sub) { return sub.unsubscribe(); });
        this.modelChangeSubs = [];
    };
    FormlyForm.prototype.patchModel = function (model) {
        this.clearModelSubscriptions();
        this.resetFieldArray(this.fields, model, this.model);
        this.initializeFormValue(this.form);
        this.form.patchValue(model, { onlySelf: true });
        this.trackModelChanges(this.fields);
    };
    FormlyForm.prototype.resetModel = function (model) {
        model = utils_1.isNullOrUndefined(model) ? this.initialModel : model;
        this.resetFieldArray(this.fields, model, this.model);
        // we should call `NgForm::resetForm` to ensure changing `submitted` state after resetting form
        // but only when the current component is a root one.
        if (!this.parentFormlyForm && this.options.parentForm && this.options.parentForm.control === this.form) {
            this.options.parentForm.resetForm(model);
        }
        else {
            this.form.reset(model);
        }
    };
    FormlyForm.prototype.resetFieldArray = function (fields, newModel, modelToUpdate) {
        var _this = this;
        fields.forEach(function (field) {
            if ((field.fieldGroup && field.fieldGroup.length > 0) || field.fieldArray) {
                var newFieldModel_1 = _this.fieldModel(field, newModel), fieldModel_1 = _this.fieldModel(field, modelToUpdate);
                if (field.fieldArray) {
                    field.fieldGroup = field.fieldGroup || [];
                    field.fieldGroup.length = 0;
                    if (fieldModel_1 !== newFieldModel_1 && fieldModel_1) {
                        fieldModel_1.length = 0;
                    }
                    var formControl_1 = field.formControl;
                    while (formControl_1.length !== 0) {
                        formControl_1.removeAt(0);
                    }
                    newFieldModel_1.forEach(function (m, i) {
                        fieldModel_1[i] = m;
                        field.fieldGroup.push(__assign({}, utils_1.clone(field.fieldArray), { key: "" + i }));
                        _this.formlyBuilder.buildForm(formControl_1, [field.fieldGroup[i]], newFieldModel_1, _this.options);
                    });
                }
                else {
                    _this.resetFieldArray(field.fieldGroup, newFieldModel_1, fieldModel_1);
                }
            }
        });
    };
    FormlyForm.prototype.initializeFormValue = function (control) {
        var _this = this;
        if (control instanceof forms_1.FormControl) {
            control.setValue(null);
        }
        else if (control instanceof forms_1.FormGroup) {
            Object.keys(control.controls).forEach(function (k) { return _this.initializeFormValue(control.controls[k]); });
        }
        else if (control instanceof forms_1.FormArray) {
            control.controls.forEach(function (c) { return _this.initializeFormValue(c); });
        }
    };
    FormlyForm.prototype.updateInitialValue = function () {
        this.initialModel = utils_1.reverseDeepMerge({}, this.model);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "model", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "form", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], FormlyForm.prototype, "fields", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "options", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "modelChange", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], FormlyForm.prototype, "isRoot", void 0);
    FormlyForm = __decorate([
        core_1.Component({
            selector: 'formly-form',
            template: "\n    <formly-field *ngFor=\"let field of fields\"\n      [model]=\"fieldModel(field)\" [form]=\"form\"\n      [field]=\"field\"\n      [ngClass]=\"field.className\"\n      [options]=\"options\">\n    </formly-field>\n    <ng-content></ng-content>\n  ",
        }),
        __param(3, core_1.Optional()),
        __param(4, core_1.Optional()),
        __param(5, core_1.Optional()), __param(5, core_1.SkipSelf()),
        __metadata("design:paramtypes", [formly_form_builder_1.FormlyFormBuilder,
            formly_form_expression_1.FormlyFormExpression,
            formly_config_1.FormlyConfig,
            forms_1.NgForm,
            forms_1.FormGroupDirective,
            FormlyForm])
    ], FormlyForm);
    return FormlyForm;
}());
exports.FormlyForm = FormlyForm;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWx5LmZvcm0uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmb3JtbHkuZm9ybS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF5STtBQUN6SSx3Q0FBK0Y7QUFFL0YsdUVBQW9FO0FBQ3BFLDZFQUEwRTtBQUMxRSwyREFBeUQ7QUFDekQsa0NBQXVHO0FBQ3ZHLHdDQUF1QztBQUN2QywyREFBMEQ7QUFDMUQseUNBQXdDO0FBZXhDO0lBYUUsb0JBQ1UsYUFBZ0MsRUFDaEMsZ0JBQXNDLEVBQ3RDLFlBQTBCLEVBQ2QsVUFBa0IsRUFDbEIsZUFBbUMsRUFDdkIsZ0JBQTRCO1FBTHBELGtCQUFhLEdBQWIsYUFBYSxDQUFtQjtRQUNoQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQXNCO1FBQ3RDLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQ2QsZUFBVSxHQUFWLFVBQVUsQ0FBUTtRQUNsQixvQkFBZSxHQUFmLGVBQWUsQ0FBb0I7UUFDdkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFZO1FBbEJyRCxVQUFLLEdBQVEsRUFBRSxDQUFDO1FBQ2hCLFNBQUksR0FBMEIsSUFBSSxpQkFBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELFdBQU0sR0FBd0IsRUFBRSxDQUFDO1FBRWhDLGdCQUFXLEdBQUcsSUFBSSxtQkFBWSxFQUFPLENBQUM7UUFFaEQsZ0JBQWdCO1FBQ1AsV0FBTSxHQUFHLElBQUksQ0FBQztRQUdmLG9CQUFlLEdBQW1CLEVBQUUsQ0FBQztJQVMxQyxDQUFDO0lBRUosOEJBQVMsR0FBVDtRQUNFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCxnQ0FBVyxHQUFYLFVBQVksT0FBc0I7UUFDaEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzdELE1BQU0sQ0FBQztRQUNULENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxpQkFBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1lBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzVCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQztJQUNILENBQUM7SUFFRCxnQ0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVELCtCQUFVLEdBQVYsVUFBVyxLQUF3QixFQUFFLEtBQWtCO1FBQWxCLHNCQUFBLEVBQUEsUUFBUSxJQUFJLENBQUMsS0FBSztRQUNyRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hELE1BQU0sQ0FBQyxxQkFBYSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0MsQ0FBQztRQUNELE1BQU0sQ0FBQyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQsZ0NBQVcsR0FBWCxVQUFZLEtBQWtDO1FBQzVDLHdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCwrQkFBVSxHQUFWO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUM7UUFFbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQ3RELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUM5RCxDQUFDO1FBQ0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsSUFBSSxpQkFBTyxFQUEwQixDQUFDO1FBQ3BFLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RCxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3BFLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2RSxDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsQ0FBUSxJQUFJLENBQUMsT0FBUSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsT0FBUSxDQUFDLHNCQUFzQixHQUFHO2dCQUM1QyxLQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztnQkFDL0IsS0FBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QyxDQUFDLENBQUM7UUFDSixDQUFDO0lBQ0gsQ0FBQztJQUVPLDBDQUFxQixHQUE3QjtRQUNFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3RGLENBQUM7SUFDSCxDQUFDO0lBRU8sc0NBQWlCLEdBQXpCLFVBQTBCLE1BQTJCLEVBQUUsT0FBc0I7UUFBN0UsaUJBdUJDO1FBdkJzRCx3QkFBQSxFQUFBLFlBQXNCO1FBQzNFLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxLQUFLO1lBQ2xCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxjQUFZLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQ2xELElBQU0sUUFBUSxHQUFHLEtBQUssQ0FBQyxZQUFZLElBQUksS0FBSyxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO2dCQUMxRyxFQUFFLENBQUMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakIsY0FBWSxHQUFHLDJCQUFZLENBQUMsSUFBSSxDQUFDLGNBQVksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDM0QsQ0FBQztnQkFDRCxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQzlDLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsUUFBUTt3QkFDNUIsY0FBWSxHQUFHLFNBQUcsQ0FBQyxJQUFJLENBQUMsY0FBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUNsRCxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDO2dCQUVELEtBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLGNBQVk7cUJBQ25DLFNBQVMsQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsRUFBRSxHQUFHLEVBQU0sT0FBTyxTQUFFLEtBQUssQ0FBQyxHQUFHLEdBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUExRSxDQUEwRSxDQUFDLENBQ2hHLENBQUM7WUFDSixDQUFDO1lBRUQsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsSUFBSSxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNwRCxLQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBSyxPQUFPLFNBQUUsS0FBSyxDQUFDLEdBQUcsR0FBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUYsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLDRDQUF1QixHQUEvQjtRQUNFLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLFdBQVcsRUFBRSxFQUFqQixDQUFpQixDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUVPLCtCQUFVLEdBQWxCLFVBQW1CLEtBQVU7UUFDM0IsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFFTywrQkFBVSxHQUFsQixVQUFtQixLQUFXO1FBQzVCLEtBQUssR0FBRyx5QkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzdELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJELCtGQUErRjtRQUMvRixxREFBcUQ7UUFDckQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3ZHLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQyxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN6QixDQUFDO0lBQ0gsQ0FBQztJQUVPLG9DQUFlLEdBQXZCLFVBQXdCLE1BQTJCLEVBQUUsUUFBYSxFQUFFLGFBQWtCO1FBQXRGLGlCQTZCQztRQTVCQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSztZQUNsQixFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLElBQUksS0FBSyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFFLElBQU0sZUFBYSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLFFBQVEsQ0FBQyxFQUNwRCxZQUFVLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7Z0JBRXJELEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO29CQUNyQixLQUFLLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxVQUFVLElBQUksRUFBRSxDQUFDO29CQUMxQyxLQUFLLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBRTVCLEVBQUUsQ0FBQyxDQUFDLFlBQVUsS0FBSyxlQUFhLElBQUksWUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDL0MsWUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7b0JBQ3hCLENBQUM7b0JBRUQsSUFBTSxhQUFXLEdBQWMsS0FBSyxDQUFDLFdBQVcsQ0FBQztvQkFDakQsT0FBTyxhQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRSxDQUFDO3dCQUNoQyxhQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUMxQixDQUFDO29CQUVELGVBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFNLEVBQUUsQ0FBUzt3QkFDdEMsWUFBVSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDbEIsS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLGNBQU0sYUFBSyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBRSxHQUFHLEVBQUUsS0FBRyxDQUFHLElBQUcsQ0FBQzt3QkFDbkUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsYUFBVyxFQUFFLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLGVBQWEsRUFBRSxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ2hHLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUM7Z0JBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ04sS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLGVBQWEsRUFBRSxZQUFVLENBQUMsQ0FBQztnQkFDcEUsQ0FBQztZQUNILENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyx3Q0FBbUIsR0FBM0IsVUFBNEIsT0FBTztRQUFuQyxpQkFRQztRQVBDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sWUFBWSxtQkFBVyxDQUFDLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLENBQUM7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxZQUFZLGlCQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3hDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQTdDLENBQTZDLENBQUMsQ0FBQztRQUM1RixDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sWUFBWSxpQkFBUyxDQUFDLENBQUMsQ0FBQztZQUN4QyxPQUFPLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO1FBQzdELENBQUM7SUFDSCxDQUFDO0lBRU8sdUNBQWtCLEdBQTFCO1FBQ0UsSUFBSSxDQUFDLFlBQVksR0FBRyx3QkFBZ0IsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUEvTFE7UUFBUixZQUFLLEVBQUU7OzZDQUFpQjtJQUNoQjtRQUFSLFlBQUssRUFBRTs7NENBQWlEO0lBQ2hEO1FBQVIsWUFBSyxFQUFFOzs4Q0FBa0M7SUFDakM7UUFBUixZQUFLLEVBQUU7OytDQUE0QjtJQUMxQjtRQUFULGFBQU0sRUFBRTs7bURBQXVDO0lBR3ZDO1FBQVIsWUFBSyxFQUFFOzs4Q0FBZTtJQVJaLFVBQVU7UUFadEIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFFBQVEsRUFBRSw2UEFRVDtTQUNGLENBQUM7UUFrQkcsV0FBQSxlQUFRLEVBQUUsQ0FBQTtRQUNWLFdBQUEsZUFBUSxFQUFFLENBQUE7UUFDVixXQUFBLGVBQVEsRUFBRSxDQUFBLEVBQUUsV0FBQSxlQUFRLEVBQUUsQ0FBQTt5Q0FMQSx1Q0FBaUI7WUFDZCw2Q0FBb0I7WUFDeEIsNEJBQVk7WUFDRixjQUFNO1lBQ0QsMEJBQWtCO1lBQ0wsVUFBVTtPQW5CbkQsVUFBVSxDQWlNdEI7SUFBRCxpQkFBQztDQUFBLEFBak1ELElBaU1DO0FBak1ZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBEb0NoZWNrLCBPbkNoYW5nZXMsIElucHV0LCBTaW1wbGVDaGFuZ2VzLCBPcHRpb25hbCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIFNraXBTZWxmLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCwgRm9ybUFycmF5LCBOZ0Zvcm0sIEZvcm1Hcm91cERpcmVjdGl2ZSwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3JtbHlGaWVsZENvbmZpZywgRm9ybWx5Rm9ybU9wdGlvbnMsIEZvcm1seVZhbHVlQ2hhbmdlRXZlbnQgfSBmcm9tICcuL2Zvcm1seS5maWVsZC5jb25maWcnO1xuaW1wb3J0IHsgRm9ybWx5Rm9ybUJ1aWxkZXIgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtbHkuZm9ybS5idWlsZGVyJztcbmltcG9ydCB7IEZvcm1seUZvcm1FeHByZXNzaW9uIH0gZnJvbSAnLi4vc2VydmljZXMvZm9ybWx5LmZvcm0uZXhwcmVzc2lvbic7XG5pbXBvcnQgeyBGb3JtbHlDb25maWcgfSBmcm9tICcuLi9zZXJ2aWNlcy9mb3JtbHkuY29uZmlnJztcbmltcG9ydCB7IGFzc2lnbk1vZGVsVmFsdWUsIGlzTnVsbE9yVW5kZWZpbmVkLCByZXZlcnNlRGVlcE1lcmdlLCBnZXRGaWVsZE1vZGVsLCBjbG9uZSB9IGZyb20gJy4uL3V0aWxzJztcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzL1N1YmplY3QnO1xuaW1wb3J0IHsgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvci9kZWJvdW5jZVRpbWUnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvci9tYXAnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcy9TdWJzY3JpcHRpb24nO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktZm9ybScsXG4gIHRlbXBsYXRlOiBgXG4gICAgPGZvcm1seS1maWVsZCAqbmdGb3I9XCJsZXQgZmllbGQgb2YgZmllbGRzXCJcbiAgICAgIFttb2RlbF09XCJmaWVsZE1vZGVsKGZpZWxkKVwiIFtmb3JtXT1cImZvcm1cIlxuICAgICAgW2ZpZWxkXT1cImZpZWxkXCJcbiAgICAgIFtuZ0NsYXNzXT1cImZpZWxkLmNsYXNzTmFtZVwiXG4gICAgICBbb3B0aW9uc109XCJvcHRpb25zXCI+XG4gICAgPC9mb3JtbHktZmllbGQ+XG4gICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuICBgLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtbHlGb3JtIGltcGxlbWVudHMgRG9DaGVjaywgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xuICBASW5wdXQoKSBtb2RlbDogYW55ID0ge307XG4gIEBJbnB1dCgpIGZvcm06IEZvcm1Hcm91cCB8IEZvcm1BcnJheSA9IG5ldyBGb3JtR3JvdXAoe30pO1xuICBASW5wdXQoKSBmaWVsZHM6IEZvcm1seUZpZWxkQ29uZmlnW10gPSBbXTtcbiAgQElucHV0KCkgb3B0aW9uczogRm9ybWx5Rm9ybU9wdGlvbnM7XG4gIEBPdXRwdXQoKSBtb2RlbENoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuXG4gIC8qKiBAaW50ZXJuYWwgKi9cbiAgQElucHV0KCkgaXNSb290ID0gdHJ1ZTtcblxuICBwcml2YXRlIGluaXRpYWxNb2RlbDogYW55O1xuICBwcml2YXRlIG1vZGVsQ2hhbmdlU3ViczogU3Vic2NyaXB0aW9uW10gPSBbXTtcblxuICBjb25zdHJ1Y3RvcihcbiAgICBwcml2YXRlIGZvcm1seUJ1aWxkZXI6IEZvcm1seUZvcm1CdWlsZGVyLFxuICAgIHByaXZhdGUgZm9ybWx5RXhwcmVzc2lvbjogRm9ybWx5Rm9ybUV4cHJlc3Npb24sXG4gICAgcHJpdmF0ZSBmb3JtbHlDb25maWc6IEZvcm1seUNvbmZpZyxcbiAgICBAT3B0aW9uYWwoKSBwcml2YXRlIHBhcmVudEZvcm06IE5nRm9ybSxcbiAgICBAT3B0aW9uYWwoKSBwcml2YXRlIHBhcmVudEZvcm1Hcm91cDogRm9ybUdyb3VwRGlyZWN0aXZlLFxuICAgIEBPcHRpb25hbCgpIEBTa2lwU2VsZigpIHByaXZhdGUgcGFyZW50Rm9ybWx5Rm9ybTogRm9ybWx5Rm9ybSxcbiAgKSB7fVxuXG4gIG5nRG9DaGVjaygpIHtcbiAgICB0aGlzLmNoZWNrRXhwcmVzc2lvbkNoYW5nZSgpO1xuICB9XG5cbiAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgIGlmICghdGhpcy5maWVsZHMgfHwgdGhpcy5maWVsZHMubGVuZ3RoID09PSAwIHx8ICF0aGlzLmlzUm9vdCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChjaGFuZ2VzLmZpZWxkcyB8fCBjaGFuZ2VzLmZvcm0pIHtcbiAgICAgIHRoaXMubW9kZWwgPSB0aGlzLm1vZGVsIHx8IHt9O1xuICAgICAgdGhpcy5mb3JtID0gdGhpcy5mb3JtIHx8IChuZXcgRm9ybUdyb3VwKHt9KSk7XG4gICAgICB0aGlzLnNldE9wdGlvbnMoKTtcbiAgICAgIHRoaXMuY2xlYXJNb2RlbFN1YnNjcmlwdGlvbnMoKTtcbiAgICAgIHRoaXMuZm9ybWx5QnVpbGRlci5idWlsZEZvcm0odGhpcy5mb3JtLCB0aGlzLmZpZWxkcywgdGhpcy5tb2RlbCwgdGhpcy5vcHRpb25zKTtcbiAgICAgIHRoaXMudHJhY2tNb2RlbENoYW5nZXModGhpcy5maWVsZHMpO1xuICAgICAgdGhpcy51cGRhdGVJbml0aWFsVmFsdWUoKTtcbiAgICB9IGVsc2UgaWYgKGNoYW5nZXMubW9kZWwpIHtcbiAgICAgIHRoaXMucGF0Y2hNb2RlbCh0aGlzLm1vZGVsKTtcbiAgICB9XG4gIH1cblxuICBuZ09uRGVzdHJveSgpIHtcbiAgICB0aGlzLmNsZWFyTW9kZWxTdWJzY3JpcHRpb25zKCk7XG4gIH1cblxuICBmaWVsZE1vZGVsKGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZywgbW9kZWwgPSB0aGlzLm1vZGVsKSB7XG4gICAgaWYgKGZpZWxkLmtleSAmJiAoZmllbGQuZmllbGRHcm91cCB8fCBmaWVsZC5maWVsZEFycmF5KSkge1xuICAgICAgcmV0dXJuIGdldEZpZWxkTW9kZWwobW9kZWwsIGZpZWxkLCB0cnVlKTtcbiAgICB9XG4gICAgcmV0dXJuIG1vZGVsO1xuICB9XG5cbiAgY2hhbmdlTW9kZWwoZXZlbnQ6IHsga2V5OiBzdHJpbmcsIHZhbHVlOiBhbnkgfSkge1xuICAgIGFzc2lnbk1vZGVsVmFsdWUodGhpcy5tb2RlbCwgZXZlbnQua2V5LCBldmVudC52YWx1ZSk7XG4gICAgdGhpcy5tb2RlbENoYW5nZS5lbWl0KHRoaXMubW9kZWwpO1xuICAgIHRoaXMuY2hlY2tFeHByZXNzaW9uQ2hhbmdlKCk7XG4gIH1cblxuICBzZXRPcHRpb25zKCkge1xuICAgIHRoaXMub3B0aW9ucyA9IHRoaXMub3B0aW9ucyB8fCB7fTtcblxuICAgIHRoaXMub3B0aW9ucy5mb3JtU3RhdGUgPSB0aGlzLm9wdGlvbnMuZm9ybVN0YXRlIHx8IHt9O1xuICAgIGlmICghdGhpcy5vcHRpb25zLnNob3dFcnJvcikge1xuICAgICAgdGhpcy5vcHRpb25zLnNob3dFcnJvciA9IHRoaXMuZm9ybWx5Q29uZmlnLmV4dHJhcy5zaG93RXJyb3I7XG4gICAgfVxuICAgIGlmICghdGhpcy5vcHRpb25zLmZpZWxkQ2hhbmdlcykge1xuICAgICAgdGhpcy5vcHRpb25zLmZpZWxkQ2hhbmdlcyA9IG5ldyBTdWJqZWN0PEZvcm1seVZhbHVlQ2hhbmdlRXZlbnQ+KCk7XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLm9wdGlvbnMucmVzZXRNb2RlbCkge1xuICAgICAgdGhpcy5vcHRpb25zLnJlc2V0TW9kZWwgPSB0aGlzLnJlc2V0TW9kZWwuYmluZCh0aGlzKTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucy5wYXJlbnRGb3JtKSB7XG4gICAgICB0aGlzLm9wdGlvbnMucGFyZW50Rm9ybSA9IHRoaXMucGFyZW50Rm9ybUdyb3VwIHx8IHRoaXMucGFyZW50Rm9ybTtcbiAgICB9XG5cbiAgICBpZiAoIXRoaXMub3B0aW9ucy51cGRhdGVJbml0aWFsVmFsdWUpIHtcbiAgICAgIHRoaXMub3B0aW9ucy51cGRhdGVJbml0aWFsVmFsdWUgPSB0aGlzLnVwZGF0ZUluaXRpYWxWYWx1ZS5iaW5kKHRoaXMpO1xuICAgIH1cblxuICAgIGlmICghKDxhbnk+IHRoaXMub3B0aW9ucykucmVzZXRUcmFja01vZGVsQ2hhbmdlcykge1xuICAgICAgKDxhbnk+IHRoaXMub3B0aW9ucykucmVzZXRUcmFja01vZGVsQ2hhbmdlcyA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5jbGVhck1vZGVsU3Vic2NyaXB0aW9ucygpO1xuICAgICAgICB0aGlzLnRyYWNrTW9kZWxDaGFuZ2VzKHRoaXMuZmllbGRzKTtcbiAgICAgIH07XG4gICAgfVxuICB9XG5cbiAgcHJpdmF0ZSBjaGVja0V4cHJlc3Npb25DaGFuZ2UoKSB7XG4gICAgaWYgKHRoaXMuaXNSb290KSB7XG4gICAgICB0aGlzLmZvcm1seUV4cHJlc3Npb24uY2hlY2tGaWVsZHModGhpcy5mb3JtLCB0aGlzLmZpZWxkcywgdGhpcy5tb2RlbCwgdGhpcy5vcHRpb25zKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHRyYWNrTW9kZWxDaGFuZ2VzKGZpZWxkczogRm9ybWx5RmllbGRDb25maWdbXSwgcm9vdEtleTogc3RyaW5nW10gPSBbXSkge1xuICAgIGZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGlmIChmaWVsZC5rZXkgJiYgZmllbGQudHlwZSAmJiAhZmllbGQuZmllbGRHcm91cCAmJiAhZmllbGQuZmllbGRBcnJheSkge1xuICAgICAgICBsZXQgdmFsdWVDaGFuZ2VzID0gZmllbGQuZm9ybUNvbnRyb2wudmFsdWVDaGFuZ2VzO1xuICAgICAgICBjb25zdCBkZWJvdW5jZSA9IGZpZWxkLm1vZGVsT3B0aW9ucyAmJiBmaWVsZC5tb2RlbE9wdGlvbnMuZGVib3VuY2UgJiYgZmllbGQubW9kZWxPcHRpb25zLmRlYm91bmNlLmRlZmF1bHQ7XG4gICAgICAgIGlmIChkZWJvdW5jZSA+IDApIHtcbiAgICAgICAgICB2YWx1ZUNoYW5nZXMgPSBkZWJvdW5jZVRpbWUuY2FsbCh2YWx1ZUNoYW5nZXMsIGRlYm91bmNlKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZmllbGQucGFyc2VycyAmJiBmaWVsZC5wYXJzZXJzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICBmaWVsZC5wYXJzZXJzLmZvckVhY2gocGFyc2VyRm4gPT4ge1xuICAgICAgICAgICAgdmFsdWVDaGFuZ2VzID0gbWFwLmNhbGwodmFsdWVDaGFuZ2VzLCBwYXJzZXJGbik7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm1vZGVsQ2hhbmdlU3Vicy5wdXNoKHZhbHVlQ2hhbmdlc1xuICAgICAgICAgIC5zdWJzY3JpYmUoZXZlbnQgPT4gdGhpcy5jaGFuZ2VNb2RlbCh7IGtleTogWy4uLnJvb3RLZXksIGZpZWxkLmtleV0uam9pbignLicpLCB2YWx1ZTogZXZlbnQgfSkpLFxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICBpZiAoZmllbGQuZmllbGRHcm91cCAmJiBmaWVsZC5maWVsZEdyb3VwLmxlbmd0aCA+IDApIHtcbiAgICAgICAgdGhpcy50cmFja01vZGVsQ2hhbmdlcyhmaWVsZC5maWVsZEdyb3VwLCBmaWVsZC5rZXkgPyBbLi4ucm9vdEtleSwgZmllbGQua2V5XSA6IHJvb3RLZXkpO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBjbGVhck1vZGVsU3Vic2NyaXB0aW9ucygpIHtcbiAgICB0aGlzLm1vZGVsQ2hhbmdlU3Vicy5mb3JFYWNoKHN1YiA9PiBzdWIudW5zdWJzY3JpYmUoKSk7XG4gICAgdGhpcy5tb2RlbENoYW5nZVN1YnMgPSBbXTtcbiAgfVxuXG4gIHByaXZhdGUgcGF0Y2hNb2RlbChtb2RlbDogYW55KSB7XG4gICAgdGhpcy5jbGVhck1vZGVsU3Vic2NyaXB0aW9ucygpO1xuICAgIHRoaXMucmVzZXRGaWVsZEFycmF5KHRoaXMuZmllbGRzLCBtb2RlbCwgdGhpcy5tb2RlbCk7XG4gICAgdGhpcy5pbml0aWFsaXplRm9ybVZhbHVlKHRoaXMuZm9ybSk7XG4gICAgKDxGb3JtR3JvdXA+IHRoaXMuZm9ybSkucGF0Y2hWYWx1ZShtb2RlbCwgeyBvbmx5U2VsZjogdHJ1ZSB9KTtcbiAgICB0aGlzLnRyYWNrTW9kZWxDaGFuZ2VzKHRoaXMuZmllbGRzKTtcbiAgfVxuXG4gIHByaXZhdGUgcmVzZXRNb2RlbChtb2RlbD86IGFueSkge1xuICAgIG1vZGVsID0gaXNOdWxsT3JVbmRlZmluZWQobW9kZWwpID8gdGhpcy5pbml0aWFsTW9kZWwgOiBtb2RlbDtcbiAgICB0aGlzLnJlc2V0RmllbGRBcnJheSh0aGlzLmZpZWxkcywgbW9kZWwsIHRoaXMubW9kZWwpO1xuXG4gICAgLy8gd2Ugc2hvdWxkIGNhbGwgYE5nRm9ybTo6cmVzZXRGb3JtYCB0byBlbnN1cmUgY2hhbmdpbmcgYHN1Ym1pdHRlZGAgc3RhdGUgYWZ0ZXIgcmVzZXR0aW5nIGZvcm1cbiAgICAvLyBidXQgb25seSB3aGVuIHRoZSBjdXJyZW50IGNvbXBvbmVudCBpcyBhIHJvb3Qgb25lLlxuICAgIGlmICghdGhpcy5wYXJlbnRGb3JtbHlGb3JtICYmIHRoaXMub3B0aW9ucy5wYXJlbnRGb3JtICYmIHRoaXMub3B0aW9ucy5wYXJlbnRGb3JtLmNvbnRyb2wgPT09IHRoaXMuZm9ybSkge1xuICAgICAgdGhpcy5vcHRpb25zLnBhcmVudEZvcm0ucmVzZXRGb3JtKG1vZGVsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5mb3JtLnJlc2V0KG1vZGVsKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHJlc2V0RmllbGRBcnJheShmaWVsZHM6IEZvcm1seUZpZWxkQ29uZmlnW10sIG5ld01vZGVsOiBhbnksIG1vZGVsVG9VcGRhdGU6IGFueSkge1xuICAgIGZpZWxkcy5mb3JFYWNoKGZpZWxkID0+IHtcbiAgICAgIGlmICgoZmllbGQuZmllbGRHcm91cCAmJiBmaWVsZC5maWVsZEdyb3VwLmxlbmd0aCA+IDApIHx8IGZpZWxkLmZpZWxkQXJyYXkpIHtcbiAgICAgICAgY29uc3QgbmV3RmllbGRNb2RlbCA9IHRoaXMuZmllbGRNb2RlbChmaWVsZCwgbmV3TW9kZWwpLFxuICAgICAgICAgIGZpZWxkTW9kZWwgPSB0aGlzLmZpZWxkTW9kZWwoZmllbGQsIG1vZGVsVG9VcGRhdGUpO1xuXG4gICAgICAgIGlmIChmaWVsZC5maWVsZEFycmF5KSB7XG4gICAgICAgICAgZmllbGQuZmllbGRHcm91cCA9IGZpZWxkLmZpZWxkR3JvdXAgfHwgW107XG4gICAgICAgICAgZmllbGQuZmllbGRHcm91cC5sZW5ndGggPSAwO1xuXG4gICAgICAgICAgaWYgKGZpZWxkTW9kZWwgIT09IG5ld0ZpZWxkTW9kZWwgJiYgZmllbGRNb2RlbCkge1xuICAgICAgICAgICAgZmllbGRNb2RlbC5sZW5ndGggPSAwO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnN0IGZvcm1Db250cm9sID0gPEZvcm1BcnJheT5maWVsZC5mb3JtQ29udHJvbDtcbiAgICAgICAgICB3aGlsZSAoZm9ybUNvbnRyb2wubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgICAgICBmb3JtQ29udHJvbC5yZW1vdmVBdCgwKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBuZXdGaWVsZE1vZGVsLmZvckVhY2goKG06IGFueSwgaTogbnVtYmVyKSA9PiB7XG4gICAgICAgICAgICBmaWVsZE1vZGVsW2ldID0gbTtcbiAgICAgICAgICAgIGZpZWxkLmZpZWxkR3JvdXAucHVzaCh7IC4uLmNsb25lKGZpZWxkLmZpZWxkQXJyYXkpLCBrZXk6IGAke2l9YCB9KTtcbiAgICAgICAgICAgIHRoaXMuZm9ybWx5QnVpbGRlci5idWlsZEZvcm0oZm9ybUNvbnRyb2wsIFtmaWVsZC5maWVsZEdyb3VwW2ldXSwgbmV3RmllbGRNb2RlbCwgdGhpcy5vcHRpb25zKTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnJlc2V0RmllbGRBcnJheShmaWVsZC5maWVsZEdyb3VwLCBuZXdGaWVsZE1vZGVsLCBmaWVsZE1vZGVsKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBpbml0aWFsaXplRm9ybVZhbHVlKGNvbnRyb2wpIHtcbiAgICBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1Db250cm9sKSB7XG4gICAgICBjb250cm9sLnNldFZhbHVlKG51bGwpO1xuICAgIH0gZWxzZSBpZiAoY29udHJvbCBpbnN0YW5jZW9mIEZvcm1Hcm91cCkge1xuICAgICAgT2JqZWN0LmtleXMoY29udHJvbC5jb250cm9scykuZm9yRWFjaChrID0+IHRoaXMuaW5pdGlhbGl6ZUZvcm1WYWx1ZShjb250cm9sLmNvbnRyb2xzW2tdKSk7XG4gICAgfSBlbHNlIGlmIChjb250cm9sIGluc3RhbmNlb2YgRm9ybUFycmF5KSB7XG4gICAgICBjb250cm9sLmNvbnRyb2xzLmZvckVhY2goYyA9PiB0aGlzLmluaXRpYWxpemVGb3JtVmFsdWUoYykpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgdXBkYXRlSW5pdGlhbFZhbHVlKCkge1xuICAgIHRoaXMuaW5pdGlhbE1vZGVsID0gcmV2ZXJzZURlZXBNZXJnZSh7fSwgdGhpcy5tb2RlbCk7XG4gIH1cbn1cbiJdfQ==
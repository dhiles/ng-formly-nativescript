"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var core_2 = require("../../core");
var ui_bootstrap_config_1 = require("./ui-bootstrap.config");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var nativescript_angular_1 = require("nativescript-angular");
var FormlyBootstrapModule = /** @class */ (function () {
    function FormlyBootstrapModule() {
    }
    FormlyBootstrapModule = __decorate([
        core_1.NgModule({
            declarations: ui_bootstrap_config_1.FIELD_TYPE_COMPONENTS,
            imports: [
                common_1.CommonModule,
                forms_1.ReactiveFormsModule,
                core_2.FormlyModule.forRoot(ui_bootstrap_config_1.BOOTSTRAP_FORMLY_CONFIG),
                nativescript_module_1.NativeScriptModule,
                nativescript_angular_1.NativeScriptFormsModule
            ],
        })
    ], FormlyBootstrapModule);
    return FormlyBootstrapModule;
}());
exports.FormlyBootstrapModule = FormlyBootstrapModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYm9vdHN0cmFwLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVpLWJvb3RzdHJhcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFDekMsMENBQStDO0FBQy9DLHdDQUFxRDtBQUNyRCxtQ0FBMEM7QUFDMUMsNkRBQXVGO0FBQ3ZGLGdGQUE4RTtBQUM5RSw2REFBK0Q7QUFhL0Q7SUFBQTtJQUNBLENBQUM7SUFEWSxxQkFBcUI7UUFWakMsZUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFLDJDQUFxQjtZQUNuQyxPQUFPLEVBQUU7Z0JBQ1AscUJBQVk7Z0JBQ1osMkJBQW1CO2dCQUNuQixtQkFBWSxDQUFDLE9BQU8sQ0FBQyw2Q0FBdUIsQ0FBQztnQkFDN0Msd0NBQWtCO2dCQUNsQiw4Q0FBdUI7YUFDeEI7U0FDRixDQUFDO09BQ1cscUJBQXFCLENBQ2pDO0lBQUQsNEJBQUM7Q0FBQSxBQURELElBQ0M7QUFEWSxzREFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGb3JtbHlNb2R1bGUgfSBmcm9tICcuLi8uLi9jb3JlJztcbmltcG9ydCB7IEJPT1RTVFJBUF9GT1JNTFlfQ09ORklHLCBGSUVMRF9UWVBFX0NPTVBPTkVOVFMgfSBmcm9tICcuL3VpLWJvb3RzdHJhcC5jb25maWcnO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBGSUVMRF9UWVBFX0NPTVBPTkVOVFMsXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcbiAgICBGb3JtbHlNb2R1bGUuZm9yUm9vdChCT09UU1RSQVBfRk9STUxZX0NPTkZJRyksXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIE5hdGl2ZVNjcmlwdEZvcm1zTW9kdWxlXG4gIF0sXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUJvb3RzdHJhcE1vZHVsZSB7XG59XG4iXX0=
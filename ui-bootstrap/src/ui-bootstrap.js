"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./types/types"));
__export(require("./wrappers/wrappers"));
var ui_bootstrap_module_1 = require("./ui-bootstrap.module");
exports.FormlyBootstrapModule = ui_bootstrap_module_1.FormlyBootstrapModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYm9vdHN0cmFwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidWktYm9vdHN0cmFwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsbUNBQThCO0FBQzlCLHlDQUFvQztBQUNwQyw2REFBOEQ7QUFBckQsc0RBQUEscUJBQXFCLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tICcuL3R5cGVzL3R5cGVzJztcbmV4cG9ydCAqIGZyb20gJy4vd3JhcHBlcnMvd3JhcHBlcnMnO1xuZXhwb3J0IHsgRm9ybWx5Qm9vdHN0cmFwTW9kdWxlIH0gZnJvbSAnLi91aS1ib290c3RyYXAubW9kdWxlJztcbiJdfQ==
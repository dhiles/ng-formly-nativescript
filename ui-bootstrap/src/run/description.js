"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TemplateDescription = /** @class */ (function () {
    function TemplateDescription() {
    }
    TemplateDescription.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) { return 'description'; });
    };
    return TemplateDescription;
}());
exports.TemplateDescription = TemplateDescription;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVzY3JpcHRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkZXNjcmlwdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0lBQUE7SUFJQSxDQUFDO0lBSEMsaUNBQUcsR0FBSCxVQUFJLEVBQWdCO1FBQ2xCLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBd0IsSUFBSyxPQUFBLGFBQWEsRUFBYixDQUFhLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBQ0gsMEJBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQztBQUpZLGtEQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEZvcm1seUZpZWxkQ29uZmlnLCBGb3JtbHlDb25maWcgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuZXhwb3J0IGNsYXNzIFRlbXBsYXRlRGVzY3JpcHRpb24ge1xuICBydW4oZmM6IEZvcm1seUNvbmZpZykge1xuICAgIGZjLnRlbXBsYXRlTWFuaXB1bGF0b3JzLnBvc3RXcmFwcGVyLnB1c2goKGZpZWxkOiBGb3JtbHlGaWVsZENvbmZpZykgPT4gJ2Rlc2NyaXB0aW9uJyk7XG4gIH1cbn1cbiJdfQ==
import { FormlyFieldConfig, FormlyConfig } from '../../../core';

export class TemplateValidation {
  run(fc: FormlyConfig) {
    fc.templateManipulators.postWrapper.push((field: FormlyFieldConfig) => 'validation-message');
  }
}

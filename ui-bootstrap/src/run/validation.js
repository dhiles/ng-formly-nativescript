"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TemplateValidation = /** @class */ (function () {
    function TemplateValidation() {
    }
    TemplateValidation.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) { return 'validation-message'; });
    };
    return TemplateValidation;
}());
exports.TemplateValidation = TemplateValidation;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGlvbi5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInZhbGlkYXRpb24udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUFBO0lBSUEsQ0FBQztJQUhDLGdDQUFHLEdBQUgsVUFBSSxFQUFnQjtRQUNsQixFQUFFLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFDLEtBQXdCLElBQUssT0FBQSxvQkFBb0IsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO0lBQy9GLENBQUM7SUFDSCx5QkFBQztBQUFELENBQUMsQUFKRCxJQUlDO0FBSlksZ0RBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRm9ybWx5RmllbGRDb25maWcsIEZvcm1seUNvbmZpZyB9IGZyb20gJy4uLy4uLy4uL2NvcmUnO1xuXG5leHBvcnQgY2xhc3MgVGVtcGxhdGVWYWxpZGF0aW9uIHtcbiAgcnVuKGZjOiBGb3JtbHlDb25maWcpIHtcbiAgICBmYy50ZW1wbGF0ZU1hbmlwdWxhdG9ycy5wb3N0V3JhcHBlci5wdXNoKChmaWVsZDogRm9ybWx5RmllbGRDb25maWcpID0+ICd2YWxpZGF0aW9uLW1lc3NhZ2UnKTtcbiAgfVxufVxuIl19
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TemplateAddons = /** @class */ (function () {
    function TemplateAddons() {
    }
    TemplateAddons.prototype.run = function (fc) {
        fc.templateManipulators.postWrapper.push(function (field) {
            if (field && field.templateOptions && (field.templateOptions.addonLeft || field.templateOptions.addonRight)) {
                return 'addons';
            }
        });
    };
    return TemplateAddons;
}());
exports.TemplateAddons = TemplateAddons;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhZGRvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBO0lBQUE7SUFRQSxDQUFDO0lBUEMsNEJBQUcsR0FBSCxVQUFJLEVBQWdCO1FBQ2xCLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUMsS0FBSztZQUM3QyxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLGVBQWUsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUM1RyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ2xCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDSCxxQkFBQztBQUFELENBQUMsQUFSRCxJQVFDO0FBUlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBGb3JtbHlDb25maWcgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuZXhwb3J0IGNsYXNzIFRlbXBsYXRlQWRkb25zIHtcbiAgcnVuKGZjOiBGb3JtbHlDb25maWcpIHtcbiAgICBmYy50ZW1wbGF0ZU1hbmlwdWxhdG9ycy5wb3N0V3JhcHBlci5wdXNoKChmaWVsZCkgPT4ge1xuICAgICAgaWYgKGZpZWxkICYmIGZpZWxkLnRlbXBsYXRlT3B0aW9ucyAmJiAoZmllbGQudGVtcGxhdGVPcHRpb25zLmFkZG9uTGVmdCB8fCBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMuYWRkb25SaWdodCkpIHtcbiAgICAgICAgcmV0dXJuICdhZGRvbnMnO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG59XG4iXX0=
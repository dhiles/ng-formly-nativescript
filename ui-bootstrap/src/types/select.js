"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var Observable_1 = require("rxjs/Observable");
var of_1 = require("rxjs/observable/of");
var SelectOption = /** @class */ (function () {
    function SelectOption(label, value, children) {
        this.label = label;
        this.value = value;
        this.group = children;
    }
    return SelectOption;
}());
exports.SelectOption = SelectOption;
var FormlyFieldSelect = /** @class */ (function (_super) {
    __extends(FormlyFieldSelect, _super);
    function FormlyFieldSelect() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormlyFieldSelect.prototype, "labelProp", {
        get: function () { return this.to.labelProp || 'label'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormlyFieldSelect.prototype, "valueProp", {
        get: function () { return this.to.valueProp || 'value'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormlyFieldSelect.prototype, "groupProp", {
        get: function () { return this.to.groupProp || 'group'; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormlyFieldSelect.prototype, "selectOptions", {
        get: function () {
            var _this = this;
            if (!(this.to.options instanceof Observable_1.Observable)) {
                var options_1 = [], groups_1 = {};
                this.to.options.map(function (option) {
                    if (!option[_this.groupProp]) {
                        options_1.push(option);
                    }
                    else {
                        if (groups_1[option[_this.groupProp]]) {
                            groups_1[option[_this.groupProp]].push(option);
                        }
                        else {
                            groups_1[option[_this.groupProp]] = [option];
                            options_1.push({
                                label: option[_this.groupProp],
                                group: groups_1[option[_this.groupProp]],
                            });
                        }
                    }
                });
                return of_1.of(options_1);
            }
            else {
                // return observable directly
                return this.to.options;
            }
        },
        enumerable: true,
        configurable: true
    });
    FormlyFieldSelect = __decorate([
        core_1.Component({
            selector: 'formly-field-select',
            template: "\n    <StackLayout></StackLayout>\n  ",
        })
    ], FormlyFieldSelect);
    return FormlyFieldSelect;
}(core_2.FieldType));
exports.FormlyFieldSelect = FormlyFieldSelect;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VsZWN0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic2VsZWN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTBDO0FBQzFDLHNDQUEwQztBQUMxQyw4Q0FBNkM7QUFDN0MseUNBQXdDO0FBRXhDO0lBT0Usc0JBQVksS0FBYSxFQUFFLEtBQWMsRUFBRSxRQUF5QjtRQUNsRSxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztJQUN4QixDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLEFBWkQsSUFZQztBQVpZLG9DQUFZO0FBb0J6QjtJQUF1QyxxQ0FBUztJQUFoRDs7SUFnQ0EsQ0FBQztJQS9CQyxzQkFBSSx3Q0FBUzthQUFiLGNBQTBCLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUNoRSxzQkFBSSx3Q0FBUzthQUFiLGNBQTBCLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUNoRSxzQkFBSSx3Q0FBUzthQUFiLGNBQTBCLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUVoRSxzQkFBSSw0Q0FBYTthQUFqQjtZQUFBLGlCQTBCQztZQXpCQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLFlBQVksdUJBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0MsSUFBTSxTQUFPLEdBQW1CLEVBQUUsRUFDaEMsUUFBTSxHQUFzQyxFQUFFLENBQUM7Z0JBRWpELElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFDLE1BQW9CO29CQUN2QyxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixTQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUN2QixDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNOLEVBQUUsQ0FBQyxDQUFDLFFBQU0sQ0FBQyxNQUFNLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNuQyxRQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDOUMsQ0FBQzt3QkFBQyxJQUFJLENBQUMsQ0FBQzs0QkFDTixRQUFNLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQzFDLFNBQU8sQ0FBQyxJQUFJLENBQUM7Z0NBQ1gsS0FBSyxFQUFFLE1BQU0sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDO2dDQUM3QixLQUFLLEVBQUUsUUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NkJBQ3RDLENBQUMsQ0FBQzt3QkFDTCxDQUFDO29CQUNILENBQUM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsTUFBTSxDQUFDLE9BQUUsQ0FBQyxTQUFPLENBQUMsQ0FBQztZQUNyQixDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sNkJBQTZCO2dCQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUM7WUFDekIsQ0FBQztRQUNILENBQUM7OztPQUFBO0lBL0JVLGlCQUFpQjtRQU43QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHFCQUFxQjtZQUMvQixRQUFRLEVBQUUsdUNBRVQ7U0FDRixDQUFDO09BQ1csaUJBQWlCLENBZ0M3QjtJQUFELHdCQUFDO0NBQUEsQUFoQ0QsQ0FBdUMsZ0JBQVMsR0FnQy9DO0FBaENZLDhDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRmllbGRUeXBlIH0gZnJvbSAnLi4vLi4vLi4vY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcbmltcG9ydCB7IG9mIH0gZnJvbSAncnhqcy9vYnNlcnZhYmxlL29mJztcblxuZXhwb3J0IGNsYXNzIFNlbGVjdE9wdGlvbiB7XG4gIGxhYmVsOiBzdHJpbmc7XG4gIHZhbHVlPzogc3RyaW5nO1xuICBncm91cD86IFNlbGVjdE9wdGlvbltdO1xuICBkaXNhYmxlZD86IGJvb2xlYW47XG4gIFtrZXk6IHN0cmluZ106IGFueTtcblxuICBjb25zdHJ1Y3RvcihsYWJlbDogc3RyaW5nLCB2YWx1ZT86IHN0cmluZywgY2hpbGRyZW4/OiBTZWxlY3RPcHRpb25bXSkge1xuICAgIHRoaXMubGFiZWwgPSBsYWJlbDtcbiAgICB0aGlzLnZhbHVlID0gdmFsdWU7XG4gICAgdGhpcy5ncm91cCA9IGNoaWxkcmVuO1xuICB9XG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2Zvcm1seS1maWVsZC1zZWxlY3QnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxTdGFja0xheW91dD48L1N0YWNrTGF5b3V0PlxuICBgLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtbHlGaWVsZFNlbGVjdCBleHRlbmRzIEZpZWxkVHlwZSB7XG4gIGdldCBsYWJlbFByb3AoKTogc3RyaW5nIHsgcmV0dXJuIHRoaXMudG8ubGFiZWxQcm9wIHx8ICdsYWJlbCc7IH1cbiAgZ2V0IHZhbHVlUHJvcCgpOiBzdHJpbmcgeyByZXR1cm4gdGhpcy50by52YWx1ZVByb3AgfHwgJ3ZhbHVlJzsgfVxuICBnZXQgZ3JvdXBQcm9wKCk6IHN0cmluZyB7IHJldHVybiB0aGlzLnRvLmdyb3VwUHJvcCB8fCAnZ3JvdXAnOyB9XG5cbiAgZ2V0IHNlbGVjdE9wdGlvbnMoKTogT2JzZXJ2YWJsZTxhbnlbXT4ge1xuICAgIGlmICghKHRoaXMudG8ub3B0aW9ucyBpbnN0YW5jZW9mIE9ic2VydmFibGUpKSB7XG4gICAgICBjb25zdCBvcHRpb25zOiBTZWxlY3RPcHRpb25bXSA9IFtdLFxuICAgICAgICBncm91cHM6IHsgW2tleTogc3RyaW5nXTogU2VsZWN0T3B0aW9uW10gfSA9IHt9O1xuXG4gICAgICB0aGlzLnRvLm9wdGlvbnMubWFwKChvcHRpb246IFNlbGVjdE9wdGlvbikgPT4ge1xuICAgICAgICBpZiAoIW9wdGlvblt0aGlzLmdyb3VwUHJvcF0pIHtcbiAgICAgICAgICBvcHRpb25zLnB1c2gob3B0aW9uKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAoZ3JvdXBzW29wdGlvblt0aGlzLmdyb3VwUHJvcF1dKSB7XG4gICAgICAgICAgICBncm91cHNbb3B0aW9uW3RoaXMuZ3JvdXBQcm9wXV0ucHVzaChvcHRpb24pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBncm91cHNbb3B0aW9uW3RoaXMuZ3JvdXBQcm9wXV0gPSBbb3B0aW9uXTtcbiAgICAgICAgICAgIG9wdGlvbnMucHVzaCh7XG4gICAgICAgICAgICAgIGxhYmVsOiBvcHRpb25bdGhpcy5ncm91cFByb3BdLFxuICAgICAgICAgICAgICBncm91cDogZ3JvdXBzW29wdGlvblt0aGlzLmdyb3VwUHJvcF1dLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcblxuICAgICAgcmV0dXJuIG9mKG9wdGlvbnMpO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyByZXR1cm4gb2JzZXJ2YWJsZSBkaXJlY3RseVxuICAgICAgcmV0dXJuIHRoaXMudG8ub3B0aW9ucztcbiAgICB9XG4gIH1cbn1cbiJdfQ==
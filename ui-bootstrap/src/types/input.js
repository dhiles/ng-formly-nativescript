"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyFieldInput = /** @class */ (function (_super) {
    __extends(FormlyFieldInput, _super);
    function FormlyFieldInput() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormlyFieldInput.prototype, "type", {
        get: function () {
            return this.to.type || 'text';
        },
        enumerable: true,
        configurable: true
    });
    FormlyFieldInput = __decorate([
        core_1.Component({
            selector: 'formly-field-input',
            template: "\n  <TextField *ngIf=\"type !== 'number' else numberTmp\" [type]=\"type\" [formControl]=\"formControl\" class=\"input form-control\" [formlyAttributes]=\"field\" [class.is-invalid]=\"showError\"></TextField>\n  <ng-template #numberTmp>\n    <TextField type=\"number\" [formControl]=\"formControl\" class=\"input form-control\" [formlyAttributes]=\"field\" [class.is-invalid]=\"showError\"></TextField>\n  </ng-template>\n",
            host: {
                // temporary fix until removing bootstrap 3 support.
                '[class.d-inline-flex]': 'to.addonLeft || to.addonRight',
                '[class.custom-file]': 'to.addonLeft || to.addonRight',
            },
        })
    ], FormlyFieldInput);
    return FormlyFieldInput;
}(core_2.FieldType));
exports.FormlyFieldInput = FormlyFieldInput;
/*

  <TextField *ngIf="to.type != 'search'" class="input form-control" [formlyAttributes]="field" [ngClass]="{'form-control-danger': valid, right: wrapper=='label'}" [secure]="to.type == 'password'" ngDefaultControl (propertyChange)="onPropertyChanged($event)" row="0" col="1"></TextField>

  <SearchBar *ngIf="to.type == 'search'" [name]="to.key" class="input form-control" [formlyAttributes]="field" [ngClass]="{'form-control-danger': valid}"></SearchBar>
*/
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5wdXQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbnB1dC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUMxQyxzQ0FBMEM7QUFnQjFDO0lBQXNDLG9DQUFTO0lBQS9DOztJQUlBLENBQUM7SUFIQyxzQkFBSSxrQ0FBSTthQUFSO1lBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQUhVLGdCQUFnQjtRQWQ1QixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixRQUFRLEVBQUUsdWFBS1g7WUFDQyxJQUFJLEVBQUU7Z0JBQ0osb0RBQW9EO2dCQUNwRCx1QkFBdUIsRUFBRSwrQkFBK0I7Z0JBQ3hELHFCQUFxQixFQUFFLCtCQUErQjthQUN2RDtTQUNGLENBQUM7T0FDVyxnQkFBZ0IsQ0FJNUI7SUFBRCx1QkFBQztDQUFBLEFBSkQsQ0FBc0MsZ0JBQVMsR0FJOUM7QUFKWSw0Q0FBZ0I7QUFNN0I7Ozs7O0VBS0UiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZpZWxkVHlwZSB9IGZyb20gJy4uLy4uLy4uL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktZmllbGQtaW5wdXQnLFxuICB0ZW1wbGF0ZTogYFxuICA8VGV4dEZpZWxkICpuZ0lmPVwidHlwZSAhPT0gJ251bWJlcicgZWxzZSBudW1iZXJUbXBcIiBbdHlwZV09XCJ0eXBlXCIgW2Zvcm1Db250cm9sXT1cImZvcm1Db250cm9sXCIgY2xhc3M9XCJpbnB1dCBmb3JtLWNvbnRyb2xcIiBbZm9ybWx5QXR0cmlidXRlc109XCJmaWVsZFwiIFtjbGFzcy5pcy1pbnZhbGlkXT1cInNob3dFcnJvclwiPjwvVGV4dEZpZWxkPlxuICA8bmctdGVtcGxhdGUgI251bWJlclRtcD5cbiAgICA8VGV4dEZpZWxkIHR5cGU9XCJudW1iZXJcIiBbZm9ybUNvbnRyb2xdPVwiZm9ybUNvbnRyb2xcIiBjbGFzcz1cImlucHV0IGZvcm0tY29udHJvbFwiIFtmb3JtbHlBdHRyaWJ1dGVzXT1cImZpZWxkXCIgW2NsYXNzLmlzLWludmFsaWRdPVwic2hvd0Vycm9yXCI+PC9UZXh0RmllbGQ+XG4gIDwvbmctdGVtcGxhdGU+XG5gLFxuICBob3N0OiB7XG4gICAgLy8gdGVtcG9yYXJ5IGZpeCB1bnRpbCByZW1vdmluZyBib290c3RyYXAgMyBzdXBwb3J0LlxuICAgICdbY2xhc3MuZC1pbmxpbmUtZmxleF0nOiAndG8uYWRkb25MZWZ0IHx8IHRvLmFkZG9uUmlnaHQnLFxuICAgICdbY2xhc3MuY3VzdG9tLWZpbGVdJzogJ3RvLmFkZG9uTGVmdCB8fCB0by5hZGRvblJpZ2h0JyxcbiAgfSxcbn0pXG5leHBvcnQgY2xhc3MgRm9ybWx5RmllbGRJbnB1dCBleHRlbmRzIEZpZWxkVHlwZSB7XG4gIGdldCB0eXBlKCkge1xuICAgIHJldHVybiB0aGlzLnRvLnR5cGUgfHwgJ3RleHQnO1xuICB9XG59XG5cbi8qXG5cbiAgPFRleHRGaWVsZCAqbmdJZj1cInRvLnR5cGUgIT0gJ3NlYXJjaCdcIiBjbGFzcz1cImlucHV0IGZvcm0tY29udHJvbFwiIFtmb3JtbHlBdHRyaWJ1dGVzXT1cImZpZWxkXCIgW25nQ2xhc3NdPVwieydmb3JtLWNvbnRyb2wtZGFuZ2VyJzogdmFsaWQsIHJpZ2h0OiB3cmFwcGVyPT0nbGFiZWwnfVwiIFtzZWN1cmVdPVwidG8udHlwZSA9PSAncGFzc3dvcmQnXCIgbmdEZWZhdWx0Q29udHJvbCAocHJvcGVydHlDaGFuZ2UpPVwib25Qcm9wZXJ0eUNoYW5nZWQoJGV2ZW50KVwiIHJvdz1cIjBcIiBjb2w9XCIxXCI+PC9UZXh0RmllbGQ+XG5cbiAgPFNlYXJjaEJhciAqbmdJZj1cInRvLnR5cGUgPT0gJ3NlYXJjaCdcIiBbbmFtZV09XCJ0by5rZXlcIiBjbGFzcz1cImlucHV0IGZvcm0tY29udHJvbFwiIFtmb3JtbHlBdHRyaWJ1dGVzXT1cImZpZWxkXCIgW25nQ2xhc3NdPVwieydmb3JtLWNvbnRyb2wtZGFuZ2VyJzogdmFsaWR9XCI+PC9TZWFyY2hCYXI+XG4qL1xuIl19
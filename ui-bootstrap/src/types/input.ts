import { Component } from '@angular/core';
import { FieldType } from '../../../core';

@Component({
  selector: 'formly-field-input',
  template: `
  <TextField *ngIf="type !== 'number' else numberTmp" [type]="type" [formControl]="formControl" class="input form-control" [formlyAttributes]="field" [class.is-invalid]="showError"></TextField>
  <ng-template #numberTmp>
    <TextField type="number" [formControl]="formControl" class="input form-control" [formlyAttributes]="field" [class.is-invalid]="showError"></TextField>
  </ng-template>
`,
  host: {
    // temporary fix until removing bootstrap 3 support.
    '[class.d-inline-flex]': 'to.addonLeft || to.addonRight',
    '[class.custom-file]': 'to.addonLeft || to.addonRight',
  },
})
export class FormlyFieldInput extends FieldType {
  get type() {
    return this.to.type || 'text';
  }
}

/*

  <TextField *ngIf="to.type != 'search'" class="input form-control" [formlyAttributes]="field" [ngClass]="{'form-control-danger': valid, right: wrapper=='label'}" [secure]="to.type == 'password'" ngDefaultControl (propertyChange)="onPropertyChanged($event)" row="0" col="1"></TextField>

  <SearchBar *ngIf="to.type == 'search'" [name]="to.key" class="input form-control" [formlyAttributes]="field" [ngClass]="{'form-control-danger': valid}"></SearchBar>
*/

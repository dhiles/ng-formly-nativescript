import { Component } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { FieldType, FormlyFieldConfig } from '../../../core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'formly-field-multicheckbox',
  template: `
  <StackLayout *ngFor="let option of to.options" class="checkbox custom-control custom-checkbox" orientation="horizontal">
    <Switch [id]="id" [name]="id" [checked]="option.value=='on'?true:false" [formControl]="formControl.get(option.key)" [formlyAttributes]="field" class="custom-control-input"></Switch>
    <Label [text]="option.value"></Label>
  </StackLayout>
`,
})
export class FormlyFieldMultiCheckbox extends FieldType {
  static createControl(model: any, field: FormlyFieldConfig): AbstractControl {
    if (!(field.templateOptions.options instanceof Observable)) {
      let controlGroupConfig = field.templateOptions.options.reduce((previous, option) => {
        previous[option.key] = new FormControl(model ? model[option.key] : undefined);
        return previous;
      }, {});

      return new FormGroup(
        controlGroupConfig,
        field.validators ? field.validators.validation : undefined,
        field.asyncValidators ? field.asyncValidators.validation : undefined,
      );
    } else {
      throw new Error(`[Formly Error] You cannot pass an Observable to a multicheckbox yet.`);
    }
  }
}

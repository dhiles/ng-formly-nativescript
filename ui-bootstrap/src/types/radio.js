"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyFieldRadio = /** @class */ (function (_super) {
    __extends(FormlyFieldRadio, _super);
    function FormlyFieldRadio() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldRadio = __decorate([
        core_1.Component({
            selector: 'formly-field-radio',
            template: "\n    <StackLayout [formGroup]=\"form\">\n      <StackLayout *ngFor=\"let option of to.options\" class=\"radio custom-control custom-radio\" orientation=\"horizontal\">\n        <Switch [checked]=\"option.key=='on'?true:false\" [formControl]=\"formControl\"\n        [formlyAttributes]=\"field\" class=\"custom-control-input\" ngDefaultControl></Switch>\n        <Label [text]=\"option.value\"></Label>\n      </StackLayout>\n    </StackLayout>\n",
        })
    ], FormlyFieldRadio);
    return FormlyFieldRadio;
}(core_2.FieldType));
exports.FormlyFieldRadio = FormlyFieldRadio;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyYWRpby50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUMxQyxzQ0FBMEM7QUFjMUM7SUFBc0Msb0NBQVM7SUFBL0M7O0lBQWlELENBQUM7SUFBckMsZ0JBQWdCO1FBWjVCLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLFFBQVEsRUFBRSxnY0FRWDtTQUNBLENBQUM7T0FDVyxnQkFBZ0IsQ0FBcUI7SUFBRCx1QkFBQztDQUFBLEFBQWxELENBQXNDLGdCQUFTLEdBQUc7QUFBckMsNENBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFR5cGUgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LWZpZWxkLXJhZGlvJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8U3RhY2tMYXlvdXQgW2Zvcm1Hcm91cF09XCJmb3JtXCI+XG4gICAgICA8U3RhY2tMYXlvdXQgKm5nRm9yPVwibGV0IG9wdGlvbiBvZiB0by5vcHRpb25zXCIgY2xhc3M9XCJyYWRpbyBjdXN0b20tY29udHJvbCBjdXN0b20tcmFkaW9cIiBvcmllbnRhdGlvbj1cImhvcml6b250YWxcIj5cbiAgICAgICAgPFN3aXRjaCBbY2hlY2tlZF09XCJvcHRpb24ua2V5PT0nb24nP3RydWU6ZmFsc2VcIiBbZm9ybUNvbnRyb2xdPVwiZm9ybUNvbnRyb2xcIlxuICAgICAgICBbZm9ybWx5QXR0cmlidXRlc109XCJmaWVsZFwiIGNsYXNzPVwiY3VzdG9tLWNvbnRyb2wtaW5wdXRcIiBuZ0RlZmF1bHRDb250cm9sPjwvU3dpdGNoPlxuICAgICAgICA8TGFiZWwgW3RleHRdPVwib3B0aW9uLnZhbHVlXCI+PC9MYWJlbD5cbiAgICAgIDwvU3RhY2tMYXlvdXQ+XG4gICAgPC9TdGFja0xheW91dD5cbmAsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUZpZWxkUmFkaW8gZXh0ZW5kcyBGaWVsZFR5cGUge31cbiJdfQ==
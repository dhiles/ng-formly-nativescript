"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var core_2 = require("../../../core");
var Observable_1 = require("rxjs/Observable");
var FormlyFieldMultiCheckbox = /** @class */ (function (_super) {
    __extends(FormlyFieldMultiCheckbox, _super);
    function FormlyFieldMultiCheckbox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldMultiCheckbox.createControl = function (model, field) {
        if (!(field.templateOptions.options instanceof Observable_1.Observable)) {
            var controlGroupConfig = field.templateOptions.options.reduce(function (previous, option) {
                previous[option.key] = new forms_1.FormControl(model ? model[option.key] : undefined);
                return previous;
            }, {});
            return new forms_1.FormGroup(controlGroupConfig, field.validators ? field.validators.validation : undefined, field.asyncValidators ? field.asyncValidators.validation : undefined);
        }
        else {
            throw new Error("[Formly Error] You cannot pass an Observable to a multicheckbox yet.");
        }
    };
    FormlyFieldMultiCheckbox = __decorate([
        core_1.Component({
            selector: 'formly-field-multicheckbox',
            template: "\n  <StackLayout *ngFor=\"let option of to.options\" class=\"checkbox custom-control custom-checkbox\" orientation=\"horizontal\">\n    <Switch [id]=\"id\" [name]=\"id\" [checked]=\"option.value=='on'?true:false\" [formControl]=\"formControl.get(option.key)\" [formlyAttributes]=\"field\" class=\"custom-control-input\"></Switch>\n    <Label [text]=\"option.value\"></Label>\n  </StackLayout>\n",
        })
    ], FormlyFieldMultiCheckbox);
    return FormlyFieldMultiCheckbox;
}(core_2.FieldType));
exports.FormlyFieldMultiCheckbox = FormlyFieldMultiCheckbox;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGljaGVja2JveC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIm11bHRpY2hlY2tib3gudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMEM7QUFDMUMsd0NBQXlFO0FBQ3pFLHNDQUE2RDtBQUM3RCw4Q0FBNkM7QUFXN0M7SUFBOEMsNENBQVM7SUFBdkQ7O0lBaUJBLENBQUM7SUFoQlEsc0NBQWEsR0FBcEIsVUFBcUIsS0FBVSxFQUFFLEtBQXdCO1FBQ3ZELEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsZUFBZSxDQUFDLE9BQU8sWUFBWSx1QkFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNELElBQUksa0JBQWtCLEdBQUcsS0FBSyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUMsUUFBUSxFQUFFLE1BQU07Z0JBQzdFLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxtQkFBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzlFLE1BQU0sQ0FBQyxRQUFRLENBQUM7WUFDbEIsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBRVAsTUFBTSxDQUFDLElBQUksaUJBQVMsQ0FDbEIsa0JBQWtCLEVBQ2xCLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQzFELEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQ3JFLENBQUM7UUFDSixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLElBQUksS0FBSyxDQUFDLHNFQUFzRSxDQUFDLENBQUM7UUFDMUYsQ0FBQztJQUNILENBQUM7SUFoQlUsd0JBQXdCO1FBVHBDLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsNEJBQTRCO1lBQ3RDLFFBQVEsRUFBRSw0WUFLWDtTQUNBLENBQUM7T0FDVyx3QkFBd0IsQ0FpQnBDO0lBQUQsK0JBQUM7Q0FBQSxBQWpCRCxDQUE4QyxnQkFBUyxHQWlCdEQ7QUFqQlksNERBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAsIEZvcm1Db250cm9sLCBBYnN0cmFjdENvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBGaWVsZFR5cGUsIEZvcm1seUZpZWxkQ29uZmlnIH0gZnJvbSAnLi4vLi4vLi4vY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9PYnNlcnZhYmxlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LWZpZWxkLW11bHRpY2hlY2tib3gnLFxuICB0ZW1wbGF0ZTogYFxuICA8U3RhY2tMYXlvdXQgKm5nRm9yPVwibGV0IG9wdGlvbiBvZiB0by5vcHRpb25zXCIgY2xhc3M9XCJjaGVja2JveCBjdXN0b20tY29udHJvbCBjdXN0b20tY2hlY2tib3hcIiBvcmllbnRhdGlvbj1cImhvcml6b250YWxcIj5cbiAgICA8U3dpdGNoIFtpZF09XCJpZFwiIFtuYW1lXT1cImlkXCIgW2NoZWNrZWRdPVwib3B0aW9uLnZhbHVlPT0nb24nP3RydWU6ZmFsc2VcIiBbZm9ybUNvbnRyb2xdPVwiZm9ybUNvbnRyb2wuZ2V0KG9wdGlvbi5rZXkpXCIgW2Zvcm1seUF0dHJpYnV0ZXNdPVwiZmllbGRcIiBjbGFzcz1cImN1c3RvbS1jb250cm9sLWlucHV0XCI+PC9Td2l0Y2g+XG4gICAgPExhYmVsIFt0ZXh0XT1cIm9wdGlvbi52YWx1ZVwiPjwvTGFiZWw+XG4gIDwvU3RhY2tMYXlvdXQ+XG5gLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtbHlGaWVsZE11bHRpQ2hlY2tib3ggZXh0ZW5kcyBGaWVsZFR5cGUge1xuICBzdGF0aWMgY3JlYXRlQ29udHJvbChtb2RlbDogYW55LCBmaWVsZDogRm9ybWx5RmllbGRDb25maWcpOiBBYnN0cmFjdENvbnRyb2wge1xuICAgIGlmICghKGZpZWxkLnRlbXBsYXRlT3B0aW9ucy5vcHRpb25zIGluc3RhbmNlb2YgT2JzZXJ2YWJsZSkpIHtcbiAgICAgIGxldCBjb250cm9sR3JvdXBDb25maWcgPSBmaWVsZC50ZW1wbGF0ZU9wdGlvbnMub3B0aW9ucy5yZWR1Y2UoKHByZXZpb3VzLCBvcHRpb24pID0+IHtcbiAgICAgICAgcHJldmlvdXNbb3B0aW9uLmtleV0gPSBuZXcgRm9ybUNvbnRyb2wobW9kZWwgPyBtb2RlbFtvcHRpb24ua2V5XSA6IHVuZGVmaW5lZCk7XG4gICAgICAgIHJldHVybiBwcmV2aW91cztcbiAgICAgIH0sIHt9KTtcblxuICAgICAgcmV0dXJuIG5ldyBGb3JtR3JvdXAoXG4gICAgICAgIGNvbnRyb2xHcm91cENvbmZpZyxcbiAgICAgICAgZmllbGQudmFsaWRhdG9ycyA/IGZpZWxkLnZhbGlkYXRvcnMudmFsaWRhdGlvbiA6IHVuZGVmaW5lZCxcbiAgICAgICAgZmllbGQuYXN5bmNWYWxpZGF0b3JzID8gZmllbGQuYXN5bmNWYWxpZGF0b3JzLnZhbGlkYXRpb24gOiB1bmRlZmluZWQsXG4gICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoYFtGb3JtbHkgRXJyb3JdIFlvdSBjYW5ub3QgcGFzcyBhbiBPYnNlcnZhYmxlIHRvIGEgbXVsdGljaGVja2JveCB5ZXQuYCk7XG4gICAgfVxuICB9XG59XG4iXX0=
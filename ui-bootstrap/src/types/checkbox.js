"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyFieldCheckbox = /** @class */ (function (_super) {
    __extends(FormlyFieldCheckbox, _super);
    function FormlyFieldCheckbox() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldCheckbox = __decorate([
        core_1.Component({
            selector: 'formly-field-checkbox',
            template: "\n    <StackLayout class=\"custom-control custom-checkbox\" orientation=\"horizontal\">\n      <Switch *ngIf=\"!to.hidden\" [isEnabled]=\"!to.disabled\"\n        [formlyAttributes]=\"field\" class=\"switch custom-control-input\"\n        ngDefaultControl (propertyChange)=\"onPropertyChanged($event)\"></Switch>\n      <Label [text]=\"to.label\" class=\"label footnote\" verticalAlignment=\"center\"></Label>\n    </StackLayout>\n  "
        })
    ], FormlyFieldCheckbox);
    return FormlyFieldCheckbox;
}(core_2.FieldType));
exports.FormlyFieldCheckbox = FormlyFieldCheckbox;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2tib3guanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJjaGVja2JveC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUMxQyxzQ0FBMEM7QUFhMUM7SUFBeUMsdUNBQVM7SUFBbEQ7O0lBQW9ELENBQUM7SUFBeEMsbUJBQW1CO1FBWC9CLGdCQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsdUJBQXVCO1lBQ2pDLFFBQVEsRUFBRSxrYkFPVDtTQUNGLENBQUM7T0FDVyxtQkFBbUIsQ0FBcUI7SUFBRCwwQkFBQztDQUFBLEFBQXJELENBQXlDLGdCQUFTLEdBQUc7QUFBeEMsa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFR5cGUgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LWZpZWxkLWNoZWNrYm94JyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJjdXN0b20tY29udHJvbCBjdXN0b20tY2hlY2tib3hcIiBvcmllbnRhdGlvbj1cImhvcml6b250YWxcIj5cbiAgICAgIDxTd2l0Y2ggKm5nSWY9XCIhdG8uaGlkZGVuXCIgW2lzRW5hYmxlZF09XCIhdG8uZGlzYWJsZWRcIlxuICAgICAgICBbZm9ybWx5QXR0cmlidXRlc109XCJmaWVsZFwiIGNsYXNzPVwic3dpdGNoIGN1c3RvbS1jb250cm9sLWlucHV0XCJcbiAgICAgICAgbmdEZWZhdWx0Q29udHJvbCAocHJvcGVydHlDaGFuZ2UpPVwib25Qcm9wZXJ0eUNoYW5nZWQoJGV2ZW50KVwiPjwvU3dpdGNoPlxuICAgICAgPExhYmVsIFt0ZXh0XT1cInRvLmxhYmVsXCIgY2xhc3M9XCJsYWJlbCBmb290bm90ZVwiIHZlcnRpY2FsQWxpZ25tZW50PVwiY2VudGVyXCI+PC9MYWJlbD5cbiAgICA8L1N0YWNrTGF5b3V0PlxuICBgXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUZpZWxkQ2hlY2tib3ggZXh0ZW5kcyBGaWVsZFR5cGUge31cbiJdfQ==
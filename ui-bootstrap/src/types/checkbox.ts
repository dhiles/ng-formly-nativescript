import { Component } from '@angular/core';
import { FieldType } from '../../../core';

@Component({
  selector: 'formly-field-checkbox',
  template: `
    <StackLayout class="custom-control custom-checkbox" orientation="horizontal">
      <Switch *ngIf="!to.hidden" [isEnabled]="!to.disabled"
        [formlyAttributes]="field" class="switch custom-control-input"
        ngDefaultControl (propertyChange)="onPropertyChanged($event)"></Switch>
      <Label [text]="to.label" class="label footnote" verticalAlignment="center"></Label>
    </StackLayout>
  `
})
export class FormlyFieldCheckbox extends FieldType {}

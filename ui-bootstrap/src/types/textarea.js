"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyFieldTextArea = /** @class */ (function (_super) {
    __extends(FormlyFieldTextArea, _super);
    function FormlyFieldTextArea() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyFieldTextArea = __decorate([
        core_1.Component({
            selector: 'formly-field-textarea',
            template: "\n    <TextView [id]=\"id\" [formControl]=\"formControl\" class=\"input form-control\"\n      [formlyAttributes]=\"field\" ngDefaultControl>\n    </TextView>\n  ",
        })
    ], FormlyFieldTextArea);
    return FormlyFieldTextArea;
}(core_2.FieldType));
exports.FormlyFieldTextArea = FormlyFieldTextArea;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dGFyZWEuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ0ZXh0YXJlYS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEwQztBQUMxQyxzQ0FBMEM7QUFVMUM7SUFBeUMsdUNBQVM7SUFBbEQ7O0lBQ0EsQ0FBQztJQURZLG1CQUFtQjtRQVIvQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLHVCQUF1QjtZQUNqQyxRQUFRLEVBQUUsbUtBSVQ7U0FDRixDQUFDO09BQ1csbUJBQW1CLENBQy9CO0lBQUQsMEJBQUM7Q0FBQSxBQURELENBQXlDLGdCQUFTLEdBQ2pEO0FBRFksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFR5cGUgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LWZpZWxkLXRleHRhcmVhJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8VGV4dFZpZXcgW2lkXT1cImlkXCIgW2Zvcm1Db250cm9sXT1cImZvcm1Db250cm9sXCIgY2xhc3M9XCJpbnB1dCBmb3JtLWNvbnRyb2xcIlxuICAgICAgW2Zvcm1seUF0dHJpYnV0ZXNdPVwiZmllbGRcIiBuZ0RlZmF1bHRDb250cm9sPlxuICAgIDwvVGV4dFZpZXc+XG4gIGAsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seUZpZWxkVGV4dEFyZWEgZXh0ZW5kcyBGaWVsZFR5cGUge1xufVxuIl19
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyWrapperFieldset = /** @class */ (function (_super) {
    __extends(FormlyWrapperFieldset, _super);
    function FormlyWrapperFieldset() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyWrapperFieldset.prototype, "fieldComponent", void 0);
    FormlyWrapperFieldset = __decorate([
        core_1.Component({
            selector: 'formly-wrapper-fieldset',
            template: "\n    <StackLayout [ngClass]=\"{'has-danger': valid}\">\n      <template #fieldComponent></template>\n    </StackLayout>\n  ",
        })
    ], FormlyWrapperFieldset);
    return FormlyWrapperFieldset;
}(core_2.FieldWrapper));
exports.FormlyWrapperFieldset = FormlyWrapperFieldset;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmllbGRzZXQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJmaWVsZHNldC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1RTtBQUN2RSxzQ0FBNkM7QUFVN0M7SUFBMkMseUNBQVk7SUFBdkQ7O0lBRUEsQ0FBQztJQUR3RDtRQUF0RCxnQkFBUyxDQUFDLGdCQUFnQixFQUFFLEVBQUMsSUFBSSxFQUFFLHVCQUFnQixFQUFDLENBQUM7a0NBQWlCLHVCQUFnQjtpRUFBQztJQUQ3RSxxQkFBcUI7UUFSakMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx5QkFBeUI7WUFDbkMsUUFBUSxFQUFFLDhIQUlUO1NBQ0YsQ0FBQztPQUNXLHFCQUFxQixDQUVqQztJQUFELDRCQUFDO0NBQUEsQUFGRCxDQUEyQyxtQkFBWSxHQUV0RDtBQUZZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFdyYXBwZXIgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LXdyYXBwZXItZmllbGRzZXQnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxTdGFja0xheW91dCBbbmdDbGFzc109XCJ7J2hhcy1kYW5nZXInOiB2YWxpZH1cIj5cbiAgICAgIDx0ZW1wbGF0ZSAjZmllbGRDb21wb25lbnQ+PC90ZW1wbGF0ZT5cbiAgICA8L1N0YWNrTGF5b3V0PlxuICBgLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtbHlXcmFwcGVyRmllbGRzZXQgZXh0ZW5kcyBGaWVsZFdyYXBwZXIge1xuICBAVmlld0NoaWxkKCdmaWVsZENvbXBvbmVudCcsIHtyZWFkOiBWaWV3Q29udGFpbmVyUmVmfSkgZmllbGRDb21wb25lbnQ6IFZpZXdDb250YWluZXJSZWY7XG59XG4iXX0=
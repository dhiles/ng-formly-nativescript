"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyWrapperLabel = /** @class */ (function (_super) {
    __extends(FormlyWrapperLabel, _super);
    function FormlyWrapperLabel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyWrapperLabel.prototype, "fieldComponent", void 0);
    FormlyWrapperLabel = __decorate([
        core_1.Component({
            selector: 'formly-wrapper-label',
            template: "\n    <StackLayout class=\"input-field\">\n      <label [attr.for]=\"id\" class=\"form-control-label control-label\" *ngIf=\"to.label\">\n        {{ to.label }}\n        <ng-container *ngIf=\"to.required && to.hideRequiredMarker !== true\">*</ng-container>\n      </label>\n      <ng-template #fieldComponent></ng-template>\n    </StackLayout>\n  ",
        })
    ], FormlyWrapperLabel);
    return FormlyWrapperLabel;
}(core_2.FieldWrapper));
exports.FormlyWrapperLabel = FormlyWrapperLabel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFiZWwuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsYWJlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1RTtBQUN2RSxzQ0FBNkM7QUFjN0M7SUFBd0Msc0NBQVk7SUFBcEQ7O0lBRUEsQ0FBQztJQUR3RDtRQUF0RCxnQkFBUyxDQUFDLGdCQUFnQixFQUFFLEVBQUMsSUFBSSxFQUFFLHVCQUFnQixFQUFDLENBQUM7a0NBQWlCLHVCQUFnQjs4REFBQztJQUQ3RSxrQkFBa0I7UUFaOUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxzQkFBc0I7WUFDaEMsUUFBUSxFQUFFLDZWQVFUO1NBQ0YsQ0FBQztPQUNXLGtCQUFrQixDQUU5QjtJQUFELHlCQUFDO0NBQUEsQUFGRCxDQUF3QyxtQkFBWSxHQUVuRDtBQUZZLGdEQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFdyYXBwZXIgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LXdyYXBwZXItbGFiZWwnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxTdGFja0xheW91dCBjbGFzcz1cImlucHV0LWZpZWxkXCI+XG4gICAgICA8bGFiZWwgW2F0dHIuZm9yXT1cImlkXCIgY2xhc3M9XCJmb3JtLWNvbnRyb2wtbGFiZWwgY29udHJvbC1sYWJlbFwiICpuZ0lmPVwidG8ubGFiZWxcIj5cbiAgICAgICAge3sgdG8ubGFiZWwgfX1cbiAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdJZj1cInRvLnJlcXVpcmVkICYmIHRvLmhpZGVSZXF1aXJlZE1hcmtlciAhPT0gdHJ1ZVwiPio8L25nLWNvbnRhaW5lcj5cbiAgICAgIDwvbGFiZWw+XG4gICAgICA8bmctdGVtcGxhdGUgI2ZpZWxkQ29tcG9uZW50PjwvbmctdGVtcGxhdGU+XG4gICAgPC9TdGFja0xheW91dD5cbiAgYCxcbn0pXG5leHBvcnQgY2xhc3MgRm9ybWx5V3JhcHBlckxhYmVsIGV4dGVuZHMgRmllbGRXcmFwcGVyIHtcbiAgQFZpZXdDaGlsZCgnZmllbGRDb21wb25lbnQnLCB7cmVhZDogVmlld0NvbnRhaW5lclJlZn0pIGZpZWxkQ29tcG9uZW50OiBWaWV3Q29udGFpbmVyUmVmO1xufVxuIl19
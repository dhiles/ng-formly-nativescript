import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '../../../core';

@Component({
  selector: 'formly-wrapper-label',
  template: `
    <StackLayout class="input-field">
      <label [attr.for]="id" class="form-control-label control-label" *ngIf="to.label">
        {{ to.label }}
        <ng-container *ngIf="to.required && to.hideRequiredMarker !== true">*</ng-container>
      </label>
      <ng-template #fieldComponent></ng-template>
    </StackLayout>
  `,
})
export class FormlyWrapperLabel extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
}

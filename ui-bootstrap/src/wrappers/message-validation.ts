import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '../../../core';

@Component({
  selector: 'formly-wrapper-validation-messages',
  template: `
    <StackLayout class="input-field">
      <Label class="label font-weight-bold m-b-5 form-control-label" [text]="to.label"></Label>
      <template #fieldComponent></template>
      <StackLayout class="hr-light" *ngIf="to.divider"></StackLayout>
    </StackLayout>
  `,
})
export class FormlyWrapperValidationMessages extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;

  get validationId() {
    return this.field.id + '-message';
  }
}

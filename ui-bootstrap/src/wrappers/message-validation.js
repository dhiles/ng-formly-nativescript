"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyWrapperValidationMessages = /** @class */ (function (_super) {
    __extends(FormlyWrapperValidationMessages, _super);
    function FormlyWrapperValidationMessages() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(FormlyWrapperValidationMessages.prototype, "validationId", {
        get: function () {
            return this.field.id + '-message';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyWrapperValidationMessages.prototype, "fieldComponent", void 0);
    FormlyWrapperValidationMessages = __decorate([
        core_1.Component({
            selector: 'formly-wrapper-validation-messages',
            template: "\n    <StackLayout class=\"input-field\">\n      <Label class=\"label font-weight-bold m-b-5 form-control-label\" [text]=\"to.label\"></Label>\n      <template #fieldComponent></template>\n      <StackLayout class=\"hr-light\" *ngIf=\"to.divider\"></StackLayout>\n    </StackLayout>\n  ",
        })
    ], FormlyWrapperValidationMessages);
    return FormlyWrapperValidationMessages;
}(core_2.FieldWrapper));
exports.FormlyWrapperValidationMessages = FormlyWrapperValidationMessages;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS12YWxpZGF0aW9uLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibWVzc2FnZS12YWxpZGF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXVFO0FBQ3ZFLHNDQUE2QztBQVk3QztJQUFxRCxtREFBWTtJQUFqRTs7SUFNQSxDQUFDO0lBSEMsc0JBQUkseURBQVk7YUFBaEI7WUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBSnNEO1FBQXRELGdCQUFTLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxJQUFJLEVBQUUsdUJBQWdCLEVBQUMsQ0FBQztrQ0FBaUIsdUJBQWdCOzJFQUFDO0lBRDdFLCtCQUErQjtRQVYzQyxnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLG9DQUFvQztZQUM5QyxRQUFRLEVBQUUsZ1NBTVQ7U0FDRixDQUFDO09BQ1csK0JBQStCLENBTTNDO0lBQUQsc0NBQUM7Q0FBQSxBQU5ELENBQXFELG1CQUFZLEdBTWhFO0FBTlksMEVBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZpZWxkV3JhcHBlciB9IGZyb20gJy4uLy4uLy4uL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktd3JhcHBlci12YWxpZGF0aW9uLW1lc3NhZ2VzJyxcbiAgdGVtcGxhdGU6IGBcbiAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJpbnB1dC1maWVsZFwiPlxuICAgICAgPExhYmVsIGNsYXNzPVwibGFiZWwgZm9udC13ZWlnaHQtYm9sZCBtLWItNSBmb3JtLWNvbnRyb2wtbGFiZWxcIiBbdGV4dF09XCJ0by5sYWJlbFwiPjwvTGFiZWw+XG4gICAgICA8dGVtcGxhdGUgI2ZpZWxkQ29tcG9uZW50PjwvdGVtcGxhdGU+XG4gICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJoci1saWdodFwiICpuZ0lmPVwidG8uZGl2aWRlclwiPjwvU3RhY2tMYXlvdXQ+XG4gICAgPC9TdGFja0xheW91dD5cbiAgYCxcbn0pXG5leHBvcnQgY2xhc3MgRm9ybWx5V3JhcHBlclZhbGlkYXRpb25NZXNzYWdlcyBleHRlbmRzIEZpZWxkV3JhcHBlciB7XG4gIEBWaWV3Q2hpbGQoJ2ZpZWxkQ29tcG9uZW50Jywge3JlYWQ6IFZpZXdDb250YWluZXJSZWZ9KSBmaWVsZENvbXBvbmVudDogVmlld0NvbnRhaW5lclJlZjtcblxuICBnZXQgdmFsaWRhdGlvbklkKCkge1xuICAgIHJldHVybiB0aGlzLmZpZWxkLmlkICsgJy1tZXNzYWdlJztcbiAgfVxufVxuIl19
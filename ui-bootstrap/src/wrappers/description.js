"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyWrapperDescription = /** @class */ (function (_super) {
    __extends(FormlyWrapperDescription, _super);
    function FormlyWrapperDescription() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyWrapperDescription.prototype, "fieldComponent", void 0);
    FormlyWrapperDescription = __decorate([
        core_1.Component({
            selector: 'formly-wrapper-description',
            template: "\n  <template #fieldComponent></template>\n    <StackLayout>\n      <Label class=\"label m-y-5 t-12 text-muted\" [text]=\"to.description\"></Label>\n    </StackLayout>\n  ",
        })
    ], FormlyWrapperDescription);
    return FormlyWrapperDescription;
}(core_2.FieldWrapper));
exports.FormlyWrapperDescription = FormlyWrapperDescription;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVzY3JpcHRpb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJkZXNjcmlwdGlvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUF1RTtBQUN2RSxzQ0FBNkM7QUFXN0M7SUFBOEMsNENBQVk7SUFBMUQ7O0lBRUEsQ0FBQztJQUR3RDtRQUF0RCxnQkFBUyxDQUFDLGdCQUFnQixFQUFFLEVBQUMsSUFBSSxFQUFFLHVCQUFnQixFQUFDLENBQUM7a0NBQWlCLHVCQUFnQjtvRUFBQztJQUQ3RSx3QkFBd0I7UUFUcEMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSw0QkFBNEI7WUFDdEMsUUFBUSxFQUFFLDZLQUtUO1NBQ0YsQ0FBQztPQUNXLHdCQUF3QixDQUVwQztJQUFELCtCQUFDO0NBQUEsQUFGRCxDQUE4QyxtQkFBWSxHQUV6RDtBQUZZLDREQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBWaWV3Q29udGFpbmVyUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGaWVsZFdyYXBwZXIgfSBmcm9tICcuLi8uLi8uLi9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZm9ybWx5LXdyYXBwZXItZGVzY3JpcHRpb24nLFxuICB0ZW1wbGF0ZTogYFxuICA8dGVtcGxhdGUgI2ZpZWxkQ29tcG9uZW50PjwvdGVtcGxhdGU+XG4gICAgPFN0YWNrTGF5b3V0PlxuICAgICAgPExhYmVsIGNsYXNzPVwibGFiZWwgbS15LTUgdC0xMiB0ZXh0LW11dGVkXCIgW3RleHRdPVwidG8uZGVzY3JpcHRpb25cIj48L0xhYmVsPlxuICAgIDwvU3RhY2tMYXlvdXQ+XG4gIGAsXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1seVdyYXBwZXJEZXNjcmlwdGlvbiBleHRlbmRzIEZpZWxkV3JhcHBlciB7XG4gIEBWaWV3Q2hpbGQoJ2ZpZWxkQ29tcG9uZW50Jywge3JlYWQ6IFZpZXdDb250YWluZXJSZWZ9KSBmaWVsZENvbXBvbmVudDogVmlld0NvbnRhaW5lclJlZjtcbn1cbiJdfQ==
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("../../../core");
var FormlyWrapperAddons = /** @class */ (function (_super) {
    __extends(FormlyWrapperAddons, _super);
    function FormlyWrapperAddons() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FormlyWrapperAddons.prototype.addonRightClick = function ($event) {
        if (this.to.addonRight.onClick) {
            this.to.addonRight.onClick(this.to, this, $event);
        }
    };
    FormlyWrapperAddons.prototype.addonLeftClick = function ($event) {
        if (this.to.addonLeft.onClick) {
            this.to.addonLeft.onClick(this.to, this, $event);
        }
    };
    __decorate([
        core_1.ViewChild('fieldComponent', { read: core_1.ViewContainerRef }),
        __metadata("design:type", core_1.ViewContainerRef)
    ], FormlyWrapperAddons.prototype, "fieldComponent", void 0);
    FormlyWrapperAddons = __decorate([
        core_1.Component({
            selector: 'formly-wrapper-addons',
            template: "\n    <StackLayout class=\"input-group\">\n      <StackLayout class=\"input-group-addon\"\n          *ngIf=\"to.addonLeft\"\n          (tap)=\"addonLeftClick($event)\">\n          <Label *ngIf=\"to.addonLeft.iconClass\" [ngClass]=\"to.addonLeft.iconClass\" [text]=\"to.addonLeft.icon\"></Label>\n          <Label *ngIf=\"to.addonLeft.text\" [text]=\"to.addonLeft.text\"></Label>\n      </StackLayout>\n      <template #fieldComponent></template>\n      <StackLayout class=\"input-group-addon\"\n          *ngIf=\"to.addonRight\"\n          (tap)=\"addonRightClick($event)\">\n          <Label *ngIf=\"to.addonRight.iconClass\" [ngClass]=\"to.addonRight.iconClass\" [text]=\"to.addonLeft.icon\"></Label>\n          <Label *ngIf=\"to.addonRight.text\" [text]=\"to.addonRight.text\"></Label>\n      </StackLayout>\n    </StackLayout>\n",
        })
    ], FormlyWrapperAddons);
    return FormlyWrapperAddons;
}(core_2.FieldWrapper));
exports.FormlyWrapperAddons = FormlyWrapperAddons;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWRkb25zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYWRkb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXVFO0FBQ3ZFLHNDQUE2QztBQXNCN0M7SUFBeUMsdUNBQVk7SUFBckQ7O0lBY0EsQ0FBQztJQVhDLDZDQUFlLEdBQWYsVUFBZ0IsTUFBVztRQUN6QixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNwRCxDQUFDO0lBQ0gsQ0FBQztJQUVELDRDQUFjLEdBQWQsVUFBZSxNQUFXO1FBQ3hCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ25ELENBQUM7SUFDSCxDQUFDO0lBWnNEO1FBQXRELGdCQUFTLENBQUMsZ0JBQWdCLEVBQUUsRUFBQyxJQUFJLEVBQUUsdUJBQWdCLEVBQUMsQ0FBQztrQ0FBaUIsdUJBQWdCOytEQUFDO0lBRDdFLG1CQUFtQjtRQXBCL0IsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSx1QkFBdUI7WUFDakMsUUFBUSxFQUFFLGswQkFnQlg7U0FDQSxDQUFDO09BQ1csbUJBQW1CLENBYy9CO0lBQUQsMEJBQUM7Q0FBQSxBQWRELENBQXlDLG1CQUFZLEdBY3BEO0FBZFksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIFZpZXdDb250YWluZXJSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZpZWxkV3JhcHBlciB9IGZyb20gJy4uLy4uLy4uL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdmb3JtbHktd3JhcHBlci1hZGRvbnMnLFxuICB0ZW1wbGF0ZTogYFxuICAgIDxTdGFja0xheW91dCBjbGFzcz1cImlucHV0LWdyb3VwXCI+XG4gICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XCJpbnB1dC1ncm91cC1hZGRvblwiXG4gICAgICAgICAgKm5nSWY9XCJ0by5hZGRvbkxlZnRcIlxuICAgICAgICAgICh0YXApPVwiYWRkb25MZWZ0Q2xpY2soJGV2ZW50KVwiPlxuICAgICAgICAgIDxMYWJlbCAqbmdJZj1cInRvLmFkZG9uTGVmdC5pY29uQ2xhc3NcIiBbbmdDbGFzc109XCJ0by5hZGRvbkxlZnQuaWNvbkNsYXNzXCIgW3RleHRdPVwidG8uYWRkb25MZWZ0Lmljb25cIj48L0xhYmVsPlxuICAgICAgICAgIDxMYWJlbCAqbmdJZj1cInRvLmFkZG9uTGVmdC50ZXh0XCIgW3RleHRdPVwidG8uYWRkb25MZWZ0LnRleHRcIj48L0xhYmVsPlxuICAgICAgPC9TdGFja0xheW91dD5cbiAgICAgIDx0ZW1wbGF0ZSAjZmllbGRDb21wb25lbnQ+PC90ZW1wbGF0ZT5cbiAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cImlucHV0LWdyb3VwLWFkZG9uXCJcbiAgICAgICAgICAqbmdJZj1cInRvLmFkZG9uUmlnaHRcIlxuICAgICAgICAgICh0YXApPVwiYWRkb25SaWdodENsaWNrKCRldmVudClcIj5cbiAgICAgICAgICA8TGFiZWwgKm5nSWY9XCJ0by5hZGRvblJpZ2h0Lmljb25DbGFzc1wiIFtuZ0NsYXNzXT1cInRvLmFkZG9uUmlnaHQuaWNvbkNsYXNzXCIgW3RleHRdPVwidG8uYWRkb25MZWZ0Lmljb25cIj48L0xhYmVsPlxuICAgICAgICAgIDxMYWJlbCAqbmdJZj1cInRvLmFkZG9uUmlnaHQudGV4dFwiIFt0ZXh0XT1cInRvLmFkZG9uUmlnaHQudGV4dFwiPjwvTGFiZWw+XG4gICAgICA8L1N0YWNrTGF5b3V0PlxuICAgIDwvU3RhY2tMYXlvdXQ+XG5gLFxufSlcbmV4cG9ydCBjbGFzcyBGb3JtbHlXcmFwcGVyQWRkb25zIGV4dGVuZHMgRmllbGRXcmFwcGVyIHtcbiAgQFZpZXdDaGlsZCgnZmllbGRDb21wb25lbnQnLCB7cmVhZDogVmlld0NvbnRhaW5lclJlZn0pIGZpZWxkQ29tcG9uZW50OiBWaWV3Q29udGFpbmVyUmVmO1xuXG4gIGFkZG9uUmlnaHRDbGljaygkZXZlbnQ6IGFueSkge1xuICAgIGlmICh0aGlzLnRvLmFkZG9uUmlnaHQub25DbGljaykge1xuICAgICAgdGhpcy50by5hZGRvblJpZ2h0Lm9uQ2xpY2sodGhpcy50bywgdGhpcywgJGV2ZW50KTtcbiAgICB9XG4gIH1cblxuICBhZGRvbkxlZnRDbGljaygkZXZlbnQ6IGFueSkge1xuICAgIGlmICh0aGlzLnRvLmFkZG9uTGVmdC5vbkNsaWNrKSB7XG4gICAgICB0aGlzLnRvLmFkZG9uTGVmdC5vbkNsaWNrKHRoaXMudG8sIHRoaXMsICRldmVudCk7XG4gICAgfVxuICB9XG59XG4iXX0=
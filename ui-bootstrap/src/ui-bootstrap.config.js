"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var addons_1 = require("./wrappers/addons");
var description_1 = require("./run/description");
var validation_1 = require("./run/validation");
var addon_1 = require("./run/addon");
var types_1 = require("./types/types");
var wrappers_1 = require("./wrappers/wrappers");
exports.FIELD_TYPE_COMPONENTS = [
    // types
    types_1.FormlyFieldInput,
    types_1.FormlyFieldCheckbox,
    types_1.FormlyFieldRadio,
    types_1.FormlyFieldSelect,
    types_1.FormlyFieldTextArea,
    types_1.FormlyFieldMultiCheckbox,
    // wrappers
    wrappers_1.FormlyWrapperLabel,
    wrappers_1.FormlyWrapperDescription,
    wrappers_1.FormlyWrapperValidationMessages,
    wrappers_1.FormlyWrapperFieldset,
    addons_1.FormlyWrapperAddons,
];
exports.BOOTSTRAP_FORMLY_CONFIG = {
    types: [
        {
            name: 'input',
            component: types_1.FormlyFieldInput,
            wrappers: ['fieldset', 'label'],
        },
        {
            name: 'checkbox',
            component: types_1.FormlyFieldCheckbox,
            wrappers: ['fieldset'],
            defaultOptions: {
                templateOptions: {
                    indeterminate: true,
                },
            },
        },
        {
            name: 'radio',
            component: types_1.FormlyFieldRadio,
            wrappers: ['fieldset', 'label'],
            defaultOptions: {
                templateOptions: {
                    options: [],
                },
            },
        },
        {
            name: 'select',
            component: types_1.FormlyFieldSelect,
            wrappers: ['fieldset', 'label'],
            defaultOptions: {
                templateOptions: {
                    options: [],
                },
            },
        },
        {
            name: 'textarea',
            component: types_1.FormlyFieldTextArea,
            wrappers: ['fieldset', 'label'],
            defaultOptions: {
                templateOptions: {
                    cols: 1,
                    rows: 1,
                },
            },
        },
        {
            name: 'multicheckbox',
            component: types_1.FormlyFieldMultiCheckbox,
            wrappers: ['fieldset', 'label'],
            defaultOptions: {
                templateOptions: {
                    options: [],
                },
            },
        },
    ],
    wrappers: [
        { name: 'label', component: wrappers_1.FormlyWrapperLabel },
        { name: 'description', component: wrappers_1.FormlyWrapperDescription },
        { name: 'validation-message', component: wrappers_1.FormlyWrapperValidationMessages },
        { name: 'fieldset', component: wrappers_1.FormlyWrapperFieldset },
        { name: 'addons', component: addons_1.FormlyWrapperAddons },
    ],
    manipulators: [
        { class: description_1.TemplateDescription, method: 'run' },
        { class: validation_1.TemplateValidation, method: 'run' },
        { class: addon_1.TemplateAddons, method: 'run' },
    ],
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidWktYm9vdHN0cmFwLmNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInVpLWJvb3RzdHJhcC5jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSw0Q0FBd0Q7QUFDeEQsaURBQXdEO0FBQ3hELCtDQUFzRDtBQUN0RCxxQ0FBNkM7QUFDN0MsdUNBT3VCO0FBQ3ZCLGdEQUs2QjtBQUVoQixRQUFBLHFCQUFxQixHQUFHO0lBQ25DLFFBQVE7SUFDUix3QkFBZ0I7SUFDaEIsMkJBQW1CO0lBQ25CLHdCQUFnQjtJQUNoQix5QkFBaUI7SUFDakIsMkJBQW1CO0lBQ25CLGdDQUF3QjtJQUV4QixXQUFXO0lBQ1gsNkJBQWtCO0lBQ2xCLG1DQUF3QjtJQUN4QiwwQ0FBK0I7SUFDL0IsZ0NBQXFCO0lBQ3JCLDRCQUFtQjtDQUNwQixDQUFDO0FBRVcsUUFBQSx1QkFBdUIsR0FBaUI7SUFDbkQsS0FBSyxFQUFFO1FBQ0w7WUFDRSxJQUFJLEVBQUUsT0FBTztZQUNiLFNBQVMsRUFBRSx3QkFBZ0I7WUFDM0IsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQztTQUNoQztRQUNEO1lBQ0UsSUFBSSxFQUFFLFVBQVU7WUFDaEIsU0FBUyxFQUFFLDJCQUFtQjtZQUM5QixRQUFRLEVBQUUsQ0FBQyxVQUFVLENBQUM7WUFDdEIsY0FBYyxFQUFFO2dCQUNkLGVBQWUsRUFBRTtvQkFDZixhQUFhLEVBQUUsSUFBSTtpQkFDcEI7YUFDRjtTQUNGO1FBQ0Q7WUFDRSxJQUFJLEVBQUUsT0FBTztZQUNiLFNBQVMsRUFBRSx3QkFBZ0I7WUFDM0IsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQztZQUMvQixjQUFjLEVBQUU7Z0JBQ2QsZUFBZSxFQUFFO29CQUNmLE9BQU8sRUFBRSxFQUFFO2lCQUNaO2FBQ0Y7U0FDRjtRQUNEO1lBQ0UsSUFBSSxFQUFFLFFBQVE7WUFDZCxTQUFTLEVBQUUseUJBQWlCO1lBQzVCLFFBQVEsRUFBRSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUM7WUFDL0IsY0FBYyxFQUFFO2dCQUNkLGVBQWUsRUFBRTtvQkFDZixPQUFPLEVBQUUsRUFBRTtpQkFDWjthQUNGO1NBQ0Y7UUFDRDtZQUNFLElBQUksRUFBRSxVQUFVO1lBQ2hCLFNBQVMsRUFBRSwyQkFBbUI7WUFDOUIsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQztZQUMvQixjQUFjLEVBQUU7Z0JBQ2QsZUFBZSxFQUFFO29CQUNmLElBQUksRUFBRSxDQUFDO29CQUNQLElBQUksRUFBRSxDQUFDO2lCQUNSO2FBQ0Y7U0FDRjtRQUNEO1lBQ0UsSUFBSSxFQUFFLGVBQWU7WUFDckIsU0FBUyxFQUFFLGdDQUF3QjtZQUNuQyxRQUFRLEVBQUUsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDO1lBQy9CLGNBQWMsRUFBRTtnQkFDZCxlQUFlLEVBQUU7b0JBQ2YsT0FBTyxFQUFFLEVBQUU7aUJBQ1o7YUFDRjtTQUNGO0tBQ0Y7SUFDRCxRQUFRLEVBQUU7UUFDUixFQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLDZCQUFrQixFQUFDO1FBQzlDLEVBQUMsSUFBSSxFQUFFLGFBQWEsRUFBRSxTQUFTLEVBQUUsbUNBQXdCLEVBQUM7UUFDMUQsRUFBQyxJQUFJLEVBQUUsb0JBQW9CLEVBQUUsU0FBUyxFQUFFLDBDQUErQixFQUFDO1FBQ3hFLEVBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsZ0NBQXFCLEVBQUM7UUFDcEQsRUFBQyxJQUFJLEVBQUUsUUFBUSxFQUFFLFNBQVMsRUFBRSw0QkFBbUIsRUFBQztLQUNqRDtJQUNELFlBQVksRUFBRTtRQUNaLEVBQUMsS0FBSyxFQUFFLGlDQUFtQixFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUM7UUFDM0MsRUFBQyxLQUFLLEVBQUUsK0JBQWtCLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBQztRQUMxQyxFQUFDLEtBQUssRUFBRSxzQkFBYyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUM7S0FDdkM7Q0FDRixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29uZmlnT3B0aW9uIH0gZnJvbSAnLi4vLi4vY29yZSc7XG5pbXBvcnQgeyBGb3JtbHlXcmFwcGVyQWRkb25zIH0gZnJvbSAnLi93cmFwcGVycy9hZGRvbnMnO1xuaW1wb3J0IHsgVGVtcGxhdGVEZXNjcmlwdGlvbiB9IGZyb20gJy4vcnVuL2Rlc2NyaXB0aW9uJztcbmltcG9ydCB7IFRlbXBsYXRlVmFsaWRhdGlvbiB9IGZyb20gJy4vcnVuL3ZhbGlkYXRpb24nO1xuaW1wb3J0IHsgVGVtcGxhdGVBZGRvbnMgfSBmcm9tICcuL3J1bi9hZGRvbic7XG5pbXBvcnQge1xuICBGb3JtbHlGaWVsZElucHV0LFxuICBGb3JtbHlGaWVsZENoZWNrYm94LFxuICBGb3JtbHlGaWVsZFJhZGlvLFxuICBGb3JtbHlGaWVsZFNlbGVjdCxcbiAgRm9ybWx5RmllbGRUZXh0QXJlYSxcbiAgRm9ybWx5RmllbGRNdWx0aUNoZWNrYm94LFxufSBmcm9tICcuL3R5cGVzL3R5cGVzJztcbmltcG9ydCB7XG4gIEZvcm1seVdyYXBwZXJMYWJlbCxcbiAgRm9ybWx5V3JhcHBlckRlc2NyaXB0aW9uLFxuICBGb3JtbHlXcmFwcGVyVmFsaWRhdGlvbk1lc3NhZ2VzLFxuICBGb3JtbHlXcmFwcGVyRmllbGRzZXQsXG59IGZyb20gJy4vd3JhcHBlcnMvd3JhcHBlcnMnO1xuXG5leHBvcnQgY29uc3QgRklFTERfVFlQRV9DT01QT05FTlRTID0gW1xuICAvLyB0eXBlc1xuICBGb3JtbHlGaWVsZElucHV0LFxuICBGb3JtbHlGaWVsZENoZWNrYm94LFxuICBGb3JtbHlGaWVsZFJhZGlvLFxuICBGb3JtbHlGaWVsZFNlbGVjdCxcbiAgRm9ybWx5RmllbGRUZXh0QXJlYSxcbiAgRm9ybWx5RmllbGRNdWx0aUNoZWNrYm94LFxuXG4gIC8vIHdyYXBwZXJzXG4gIEZvcm1seVdyYXBwZXJMYWJlbCxcbiAgRm9ybWx5V3JhcHBlckRlc2NyaXB0aW9uLFxuICBGb3JtbHlXcmFwcGVyVmFsaWRhdGlvbk1lc3NhZ2VzLFxuICBGb3JtbHlXcmFwcGVyRmllbGRzZXQsXG4gIEZvcm1seVdyYXBwZXJBZGRvbnMsXG5dO1xuXG5leHBvcnQgY29uc3QgQk9PVFNUUkFQX0ZPUk1MWV9DT05GSUc6IENvbmZpZ09wdGlvbiA9IHtcbiAgdHlwZXM6IFtcbiAgICB7XG4gICAgICBuYW1lOiAnaW5wdXQnLFxuICAgICAgY29tcG9uZW50OiBGb3JtbHlGaWVsZElucHV0LFxuICAgICAgd3JhcHBlcnM6IFsnZmllbGRzZXQnLCAnbGFiZWwnXSxcbiAgICB9LFxuICAgIHtcbiAgICAgIG5hbWU6ICdjaGVja2JveCcsXG4gICAgICBjb21wb25lbnQ6IEZvcm1seUZpZWxkQ2hlY2tib3gsXG4gICAgICB3cmFwcGVyczogWydmaWVsZHNldCddLFxuICAgICAgZGVmYXVsdE9wdGlvbnM6IHtcbiAgICAgICAgdGVtcGxhdGVPcHRpb25zOiB7XG4gICAgICAgICAgaW5kZXRlcm1pbmF0ZTogdHJ1ZSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICB7XG4gICAgICBuYW1lOiAncmFkaW8nLFxuICAgICAgY29tcG9uZW50OiBGb3JtbHlGaWVsZFJhZGlvLFxuICAgICAgd3JhcHBlcnM6IFsnZmllbGRzZXQnLCAnbGFiZWwnXSxcbiAgICAgIGRlZmF1bHRPcHRpb25zOiB7XG4gICAgICAgIHRlbXBsYXRlT3B0aW9uczoge1xuICAgICAgICAgIG9wdGlvbnM6IFtdLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICAgIHtcbiAgICAgIG5hbWU6ICdzZWxlY3QnLFxuICAgICAgY29tcG9uZW50OiBGb3JtbHlGaWVsZFNlbGVjdCxcbiAgICAgIHdyYXBwZXJzOiBbJ2ZpZWxkc2V0JywgJ2xhYmVsJ10sXG4gICAgICBkZWZhdWx0T3B0aW9uczoge1xuICAgICAgICB0ZW1wbGF0ZU9wdGlvbnM6IHtcbiAgICAgICAgICBvcHRpb25zOiBbXSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICB7XG4gICAgICBuYW1lOiAndGV4dGFyZWEnLFxuICAgICAgY29tcG9uZW50OiBGb3JtbHlGaWVsZFRleHRBcmVhLFxuICAgICAgd3JhcHBlcnM6IFsnZmllbGRzZXQnLCAnbGFiZWwnXSxcbiAgICAgIGRlZmF1bHRPcHRpb25zOiB7XG4gICAgICAgIHRlbXBsYXRlT3B0aW9uczoge1xuICAgICAgICAgIGNvbHM6IDEsXG4gICAgICAgICAgcm93czogMSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICB7XG4gICAgICBuYW1lOiAnbXVsdGljaGVja2JveCcsXG4gICAgICBjb21wb25lbnQ6IEZvcm1seUZpZWxkTXVsdGlDaGVja2JveCxcbiAgICAgIHdyYXBwZXJzOiBbJ2ZpZWxkc2V0JywgJ2xhYmVsJ10sXG4gICAgICBkZWZhdWx0T3B0aW9uczoge1xuICAgICAgICB0ZW1wbGF0ZU9wdGlvbnM6IHtcbiAgICAgICAgICBvcHRpb25zOiBbXSxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgXSxcbiAgd3JhcHBlcnM6IFtcbiAgICB7bmFtZTogJ2xhYmVsJywgY29tcG9uZW50OiBGb3JtbHlXcmFwcGVyTGFiZWx9LFxuICAgIHtuYW1lOiAnZGVzY3JpcHRpb24nLCBjb21wb25lbnQ6IEZvcm1seVdyYXBwZXJEZXNjcmlwdGlvbn0sXG4gICAge25hbWU6ICd2YWxpZGF0aW9uLW1lc3NhZ2UnLCBjb21wb25lbnQ6IEZvcm1seVdyYXBwZXJWYWxpZGF0aW9uTWVzc2FnZXN9LFxuICAgIHtuYW1lOiAnZmllbGRzZXQnLCBjb21wb25lbnQ6IEZvcm1seVdyYXBwZXJGaWVsZHNldH0sXG4gICAge25hbWU6ICdhZGRvbnMnLCBjb21wb25lbnQ6IEZvcm1seVdyYXBwZXJBZGRvbnN9LFxuICBdLFxuICBtYW5pcHVsYXRvcnM6IFtcbiAgICB7Y2xhc3M6IFRlbXBsYXRlRGVzY3JpcHRpb24sIG1ldGhvZDogJ3J1bid9LFxuICAgIHtjbGFzczogVGVtcGxhdGVWYWxpZGF0aW9uLCBtZXRob2Q6ICdydW4nfSxcbiAgICB7Y2xhc3M6IFRlbXBsYXRlQWRkb25zLCBtZXRob2Q6ICdydW4nfSxcbiAgXSxcbn07XG4iXX0=